import React, {Component} from "react";
import {XAxis, YAxis, Tooltip, CartesianGrid, ResponsiveContainer, Line, LineChart } from 'recharts';

  class Averagerpm extends Component {
      constructor(props) {
        super(props)
        this.state = {

        }
      }
      
      componentWillReceiveProps(nextProps){
        if(nextProps !== this.props){
            this.props = nextProps
        }
      }

      render() {
          var rpm = this.props.value;
          var speed = this.props.value1;
          var TIME =  this.props.TIME;

          if(this.props.rpmgraphdata.length <5){
            if(rpm){
              this.props.rpmgraphdata.push( {RPM: speed, Time: TIME} )
            } 
            
          }
          else{
            if(rpm){
              this.props.rpmgraphdata.shift();
              this.props.rpmgraphdata.push( {RPM: speed, Time: TIME} )
            }
          }

          let data = this.props.rpmgraphdata;
          return (
            <div style={{textAlign:'center', fontSize:'12px', fontWeight:'bold', marginBottom:'50px'}}> 
              <div style={{color:'#ea80fc', marginTop:'5px', fontSize:'15px', fontWeight:'normal'}}>RPM  vs  Time</div>
              <ResponsiveContainer height={200}> 
                <LineChart data={data} margin={{ top: 5, right: 30, left: 0, bottom: 0 }} >   
                    
                    <XAxis stroke='white' dataKey='Time' fontSize='12px' >
                      {/* <Label fill='#ea80fc' value=" 'RPM'  vs  'Time' " offset={-10} position="insideBottom" /> */}
                    </XAxis>

                    <YAxis stroke='white' dataKey='RPM'  fontSize='12px' domain={[0, 'dataMax+20']} />
                    <CartesianGrid strokeDasharray="2 2"/>
                    <Tooltip/>
                    {/* <Legend /> */}
                    <Line type='monotone' strokeWidth={2} dataKey='RPM' activeDot={{r: 5}} stroke='#ff9800'/> 
                </LineChart> 
              </ResponsiveContainer>
            </div>
          );
    }
}
export default Averagerpm;
