var db = require('../sequelize.js')
// Inserts into user_profile for public user
var userProfile = (userInfo,data,callback)=>{
var empty = '';

 try {
   if(data.update == true)
   var userQuery = {
     text : 'INSERT INTO user_profile(user_id, dl_no, mobile_no, city, state, fuel_type, vehicle_name, tank_capacity) values($1, $2, $3, $4, $5, $6, $7, $8)',
     values : [userInfo.id, data.dl_no || empty, data.mobile_no || empty, data.city || empty, data.state || empty, data.fuel_type || empty, data.vehicle_name || empty, data.tank_capacity || empty]
   }
   else {
     var userQuery = {
       text : 'update user_profile set dl_no = $2, city = $4, state = $5, mobile_no = $3, fuel_type = $6, vehicle_name = $7, tank_capacity = $8 where user_id = $1',
       values : [userInfo.id, data.dl_no || empty, data.mobile_no || empty, data.city || empty, data.state || empty, data.fuel_type || empty, data.vehicle_name || empty, data.tank_capacity || empty]
     }
   }
 } catch (e) {
   console.log(e)
   callback('Invalid Parameters',false)
 }
  db.query(userQuery)
  .then(result=>{
    callback(false,'Success')
  })
  .catch(err=>{
    console.log({err})
    callback('Error : Insert failed!',false)
  })
}

module.exports = userProfile;
