var mqtt = require('mqtt');
var db = require('./sequelize')
var sendsftpPath = function(row,formatted){
  console.log(row)
  var SrNo =row.SrNo;
  var ECU = row.ECU;
  var UpdateToVersion = row.UpdateToVersion
  var Username = row.Username
  var client = mqtt.connect('tcp://149.28.63.75:1883');
  client.on('connect', function() {
    var query = {
      text: `select "File_Path" from "sftpMetadata" where ("ECU" = $1 AND "Available_Version" = $2)`,
      values: [ECU, UpdateToVersion]
    }
    db.query(query)
    .then(result => {
      client.publish(`update/${SrNo}`, `${JSON.stringify(result.rows[0])}`)
      console.log(`Sent ${JSON.stringify(result.rows[0])} \ton Device: ${SrNo}`)
    })
    .catch(function(err){
      console.log('error in query')
      console.log(err);
    })
  })
  client.subscribe('update/#')
  client.on('message', function (topic, message) {
    // console.log(message)
    console.log(message.toString())
// console.log(message);
    // console.log(JSON.stringify(message.Status))
    if(JSON.parse(message.toString()).Status == 'Updated'){
      var query= {
        text: `Update "Report" set "Status"= 'Updated' where ("Username"= $1 AND "SrNo"= $2 AND "UpdateToVersion"= $3)`,
        values: [Username,SrNo,UpdateToVersion]
      }
      db.query(query)
      .then(result => {
        console.log("Device updated successfully and completely")
      })
      .catch(function(err){
        console.log('error in query')
        console.log(err);
      })
    }
  })
}

module.exports = {
  sendsftpPath
}
