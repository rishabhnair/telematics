import React, {Component} from 'react'
import {Row, Col} from 'reactstrap'
import Switchbutton from './switchbutton';
import '../../Style/home.css'
import '../../Style/mapview.css'

class MapView extends Component{
    constructor(props){
        super(props);
        this.state={
            check:'',
            value:'',
            switchValue:''
        }
        this.switchValue = this.switchValue.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    switchValue(switchValue){
        this.setState({switchValue: switchValue, check:'', value:''})
    }

    handleChange(e, check) {
        const target = e.target;
        const name  = target.name;
        const value = target.value;
        if(check === 'state' || check==='city' || check==='subcity'){
            this.setState({check: check, value: value})
        }
    }

    render(){
        return(
            <div>
                <Row className='mainrow'>
                    {this.props.checkforadmin === 'admin' ? 
                        <Col xs='12' sm='6' md='2' lg='2' >
                            <select onChange={(e)=>{this.handleChange(e, 'state')}} className='dropdown'>
                                <option>STATE</option>
                                <option  value='haryana'>Haryana</option>
                                <option  value='uttar pradesh'>Uttar Pradesh</option>
                                <option  value='mp'>MP</option>
                            </select>
                                          
                        </Col>
                        :
                        <Col xs='12' sm='6' md='2' lg='2' >
                            <div className='coltextcolor'>U.P </div>   
                        </Col>
                    }

                    {this.props.checkforadmin === 'admin' ?
                        <Col xs ='12' sm='6' md='2' lg='2' >
                            <select onChange={(e)=>{this.handleChange(e, 'city')}} className='dropdown' >
                                <option>CITY</option>
                                <option  value='gurgaon'>Gurgaon</option>
                                <option  value='bsr'>BSR</option>
                                <option  value='banglore'>Banglore</option>
                            </select>
                        </Col>
                        :
                        <Col xs ='12' sm='6' md='2' lg='2'  >
                            <div className='coltextcolor'>Bulandshahr </div>
                        </Col>
                    }

                    {this.props.checkforadmin === 'admin' ? 
                        <Col xs='12' sm='6' md='2' lg='2'  >
                            <select onChange={(e)=>{this.handleChange(e, 'subcity')}} className='dropdown' >
                                <option>SUB-UNIT </option>
                                <option  value='udyog vihar'>Udyog vihar </option>
                                <option  value='dlf phase2'>Dlf phase2</option>
                                <option  value='sikandarpur'>sikandarpur</option>
                            </select>
                        </Col> 
                        :
                        <Col xs='12' sm='6' md='2' lg='2' >
                            <div className='coltextcolor'> I.P College </div>
                        </Col>
                    }

                    <Col xs='12' sm='12' md='3' lg='3'></Col>
                    
                    <Col xs='12' sm='6' md='3' lg='3' className='dropdown'>
                        <Row>
                            <Col xs='6' sm="6" md="6" lg="6" className="homeh" style={{color:'#ff9800'}}>View All Devices</Col>
                            <Col xs='6' sm="6" md="6" lg="6" className="homeh1"> <Switchbutton switchvalue={this.switchValue} /> </Col>
                        </Row>
                    </Col>
                </Row>

                <Row className='maprow'>
                    {this.state.switchValue ? 
                        <div style={{textAlign:'center'}}> Showing all devices </div>
                        :
                        <div style={{textAlign:'center', marginTop:'10%'}}> Showing devices with filter </div>
                    }
                </Row>
            </div>
        )
    }
}
export default MapView;