const db = require('../sequelize');

var getdetails = function(token,callback){
  var query={
    text : 'select * from "sftpMetadata"'
  }
  db.query(query)
  .then(resp=>{
    callback(false,resp.rows)
 }, function(err) {
     console.trace(err.message);
     callback(err.message,false)
 })
 .catch(err=>{
   callback('err',false)
 })
}

module.exports = {
  getdetails
}
