var express = require('express');
var bodyParser=require('body-parser');
var app=express();
app.use(bodyParser.urlencoded({extended:false}))
app.use(bodyParser.json());
app.route('/home')
.get((req,res)=>{
  res.send('new.html')
})
.post((req,res)=>{
  console.log(req.body)
})

app.listen(5000)
