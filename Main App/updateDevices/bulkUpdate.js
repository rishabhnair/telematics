var fs = require('fs')
var db = require('../sequelize.js')
var getCSV = require('get-csv');
var dateTime = require('node-datetime')
var updatedevices = require('./UpdateDevices.js')
var checkrows = require('./checkRows.js')
var bulkUpdate = (filename,Username,callback)=>{
  console.log(filename)
  getCSV(`./filesBulkMetadata/${filename}`, function (err, data) {
    if (err) {
      console.log('invalid csv')
    }
  })
  .catch(function (err) {
    console.log('enter a valid csv')
  }
)
.then(row => {
  if (row) {
    checkrows.checkrows(row, function (wrongRows) {
      if (wrongRows.length != 0) {
        console.log(`error in following rows ${wrongRows}`);
        callback(true,false)
      }
      else {
        console.log(`No error in file, pushing sftp paths to devices`);
        var dt = dateTime.create();
        var formatted = dt.format('Y-m-d H:M:S');

        func1(row,formatted,Username)
        .then(res1=>{
          updatedevices.sendsftpPath(row, formatted, Username);
          callback(false,'Updating Devices')
        })
        .catch(err=>{
          callback(true,false)
          console.log({err})
        })

      }
    });
  }
})
}

  var func1 = async(row,formatted,Username)=>{
    var result = []
        for(var i= 0;i< row.length; i++){
          var query1 = {
            text: `insert into "Report" ("DateTime","Username","SrNo", "ECU","UpdateToVersion","Status") values ($1, $2, $3, $4, $5, $6)`,
            values: [formatted, Username, row[i].SrNo, row[i].ECU, row[i].UpdateToVersion, "Updating"]
          }
          result[i] = await db.query(query1)
          .catch(err=>{
            throw err
          })
          console.log({result})
          if(i==row.length){
            return values
          }
        }
    }

  module.exports = bulkUpdate;
