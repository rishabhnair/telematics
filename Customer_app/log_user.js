var hashpass = require('./password')
var db = require('./sequelize')



var log_user = (req,res,next)=>{
  if(req.cookies.user){
    res.redirect('/dashboard')
  }
  else{
  var cred = req.body;

  console.log('logging in')
  var query = {
    text : 'select * from users where username = $1',
    values : [cred.username],
  }

  db.query(query)
  .then(resp=>{
    console.log(resp.rows[0])
    if(resp.rows[0]!=undefined || cred.username!=undefined){

      if((cred.password)==resp.rows[0].password)
      {
      console.log('login successfull')
      res.cookie('user', cred.username,{ expires: new Date(Date.now() + 18000000), httpOnly: true })
      var query = {
        text : "INSERT into users_log(username,status) values ($1,'active')",
        values : [resp.rows[0].username],
      }
      db.query(query).then(result=>{

      }).catch(error=>{
        res.render('signin.hbs',{error:"wrong username or password"});
      })
      res.redirect('/dashboard')
    }
      else {
      console.log("error")
      res.render('signin.hbs',{error:"wrong username or password"});
      }
    }
    else {
      res.redirect('signin.hbs',{error:"User not found"});

    }
  }).catch(e=>{
    res.render('signin.hbs',{error:"User not found"});
  })
}
}


module.exports = log_user;
