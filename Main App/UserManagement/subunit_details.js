const db = require('../sequelize');

var getdetails = function(username){
  var query={
    text : 'select subunit_name, city, state from subunit_info where subunit_name in (select subunit_name from subunit_db where project_id = (select project_id from users where username = $1)) ',
    values : [username]
  }
  db.query(query)
  .then(reply=>{
    console.log('SUBUNIT DETAILS:')
    console.log(reply)
    callback(false,reply.rows)
 }, function(err) {
     console.trace(err.message);
     callback(err.message,false)
 })
 .catch(err=>{
   callback('err',false)
 })
}

module.exports = {
  getdetails
}
