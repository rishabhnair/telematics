var db = require('../sequelize.js')

var addDevice = (data,callback)=>{
  console.log(data)
  var srno = data.srno;
  var imei = data.imei;
  var buff = srno + imei;
  var device_id = Buffer.from(buff).toString('base64');
  var updated_by = 'admin';
  var query = {
    text : 'INSERT into devices(srno, imei, device_id, updated_by, time) VALUES($1,$2,$3,$4,$5)',
    values : [srno, imei, device_id, updated_by, 'now']
  }
  db.query(query)
  .then(result=>{
    // console.log(result)
    callback(false,result)
  })
  .catch(err=>{
    // console.log(err.message)
    callback(err.message,false)
  })
}

module.exports = addDevice;
