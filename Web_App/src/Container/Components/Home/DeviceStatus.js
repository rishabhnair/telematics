import React, { Component } from 'react';
import {Alert ,Col, Row, Button, Label} from 'reactstrap'
import { Scrollbars } from 'react-custom-scrollbars';
import axios from 'axios'
import DateFormat from 'dateformat' ;
import '../../Style/homemodel.css'

class DeviceStatus extends Component{
    constructor(props){
        super(props);
        this.state = {
            filterDeviceStatus:'',
            selectedvalue:'DateTime',
            deviceReports:''
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
    }

    axiosFunc = () => {
        axios.defaults.headers.common['auth'] = sessionStorage.getItem('DontTouchMe');
        axios.get('http://8.9.36.13/factory/fotareport')
            .then(res => {
                //console.log(res.data)
                if(res.status === 200){
                    let deviceReports = [];
                    for(let i=0; i<res.data.data.length; i++){
                            deviceReports.push({DateTime:res.data.data[i].DateTime ? res.data.data[i].DateTime : 'NA', ECU:res.data.data[i].ECU ? res.data.data[i].ECU : 'NA', SrNo:res.data.data[i].SrNo ? res.data.data[i].SrNo : 'NA', Status:res.data.data[i].Status ? res.data.data[i].Status : 'NA', UpdateToVersion:res.data.data[i].UpdateToVersion ? res.data.data[i].UpdateToVersion : 'NA', Username:res.data.data[i].Username ? res.data.data[i].Username : 'NA' })
                    }
                    this.setState({deviceReports: deviceReports});
                }
            }).catch( er => {
                if(er.response){
                    if(er.response.status === 401)
                        console.log("Unauthorized or Invalid credential")
                    if(er.response.status === 404){
                        console.log("Not Found")
                    }
                }
                else{
                    console.log("Error in response");
                }
            });
    };

    componentDidMount() {
        this.axiosFunc();
        this.interval = setInterval(this.axiosFunc, 5000);
    }

    handleInputChange(e) {
        this.setState({filterDeviceStatus:e.target.value});
    }

    handleChange(e){
        this.setState({selectedvalue:e.target.value})
    }

    componentWillUnmount(){
        clearInterval(this.interval);
    }

    render(){
        return(
            <div>
                <div className='modeldivupdatedevice'> </div>
                                        
                <Row style={{marginTop:'12px'}} >
                    <Col xs='1' sm='1' md='1' lg='1'></Col>

                    <Col xs='12' sm='12' md='5' lg='5'>  
                        <Label className="onandfilter">Filter</Label>
                            <input type="text" name="filterDeviceStatus" id="filterDeviceStatus" value={this.state.filterDeviceStatus} onChange={this.handleInputChange} />                                   
                    </Col>

                    <Col xs='12' sm='12' md='4' lg='4' className='onsmalldevice'>        
                        <Label className="onandfilter"> On </Label>          
                        <select onChange={(e)=>{this.handleChange(e, 'selected')}}  style={{color:'#ff9800 ', width:'60%', height:'100%', background:'#232228', border:'1px solid #ff9800'}} >
                            <option  value='DateTime'>Date</option>
                            <option  value='Username'>Username</option>
                            <option  value='SrNo'>Device ID</option>
                            <option  value='ECU'>ECU</option>
                            <option  value='UpdateToVersion'>Update Version</option>
                            <option  value='Status'>Status</option>
                        </select>
                    </Col>              
                </Row>
                
                <div>
                <Scrollbars
                    onScroll={this.handleScroll}
                    onScrollFrame={this.handleScrollFrame}
                    onScrollStart={this.handleScrollStart}
                    onScrollStop={this.handleScrollStop}
                    onUpdate={this.handleUpdate}
                    renderView={this.renderView}
                    renderTrackHorizontal={this.renderTrackHorizontal}
                    renderTrackVertical={this.renderTrackVertical}
                    renderThumbHorizontal={this.renderThumbHorizontal}
                    autoHide
                    autoHideTimeout={1000}
                    autoHideDuration={200}
                    autoHeight
                    autoHeightMin={0}
                    autoHeightMax={350}
                    thumbMinSize={30}
                    universal={true}
                    renderThumbVertical={props => < div {...props} className="thumb-vertical track-vertical"/>}
                    //renderTrackVertical={props => < div {...props} className="track-vertical"/>}
                    >   
                    <Alert className='checkwidth' fade={false} style={{fontSize:'15px', backgroundColor:'#ff9800', borderColor:'#ff9800', marginTop:"15px", borderRadius:'10px'}}>
                        <Row className="rowdeviceStatus">
                            <Col xs='2' sm='2' md='2' lg='2'><b>Date</b></Col>
                            <Col xs='2' sm='2' md='2' lg='2'><b>Username</b></Col>
                            <Col xs='2' sm='2' md='2' lg='2' ><b>Device ID</b></Col>
                            <Col xs='1' sm='1' md='1' lg='1' ><Row style={{textAlign:'right'}}><b>ECU</b></Row></Col>
                            <Col xs='3' sm='3' md='3' lg='3' style={{textAlign:'center'}} ><b>Update Version</b></Col>
                            <Col xs='2' sm='2' md='2' lg='2'><b>Status</b></Col>
                        </Row>
                    </Alert>
                </Scrollbars>
                </div>

                <div>
                    <Scrollbars
                    onScroll={this.handleScroll}
                    onScrollFrame={this.handleScrollFrame}
                    onScrollStart={this.handleScrollStart}
                    onScrollStop={this.handleScrollStop}
                    onUpdate={this.handleUpdate}
                    renderView={this.renderView}
                    renderTrackHorizontal={this.renderTrackHorizontal}
                    renderTrackVertical={this.renderTrackVertical}
                    renderThumbHorizontal={this.renderThumbHorizontal}
                    autoHide
                    autoHideTimeout={1000}
                    autoHideDuration={200}
                    autoHeight
                    autoHeightMin={0}
                    autoHeightMax={350}
                    thumbMinSize={30}
                    universal={true}
                    renderThumbVertical={props => < div {...props} className="thumb-vertical track-vertical"/>}
                    //renderTrackVertical={props => < div {...props} className="track-vertical"/>}
                    >   

                    {this.state.deviceReports ? 
                        this.state.deviceReports.filter( (item, index) => item[this.state.selectedvalue].toLowerCase().includes(this.state.filterDeviceStatus.toLowerCase()) ).map( (details, i) =>                            
                            <Alert className='checkwidth' key={i} fade={false} style={{fontSize:'13px' ,backgroundColor:'#676A86', borderColor:'#676A86', borderRadius:'10px', color:'white'}}>
                                <Row className="rowdeviceStatusdata">
                                    <Col xs='2' sm='2' md='2' lg='2'> {details.DateTime ? DateFormat(new Date(details.DateTime),"yyyy-mm-dd h:MM:ss") : 'Not Applicable'} </Col>
                                    <Col xs='2' sm='2' md='2' lg='2'> {details.Username ? details.Username : 'Not Applicable'} </Col>
                                    <Col xs='2' sm='2' md='2' lg='2'> {details.SrNo ? details.SrNo : 'Not Applicable'}</Col>
                                    <Col xs='1' sm='1' md='1' lg='1'><Row style={{textAlign:'right'}}> {details.ECU ? details.ECU : 'Not Applicable'} </Row> </Col>
                                    <Col xs='3' sm='3' md='3' lg='3' style={{textAlign:'center'}}> {details.UpdateToVersion ? details.UpdateToVersion : 'Not Applicable'}</Col>
                                    <Col xs='2' sm='2' md='2' lg='2' style={{color: details.Status === 'Updating' ? 'white' : '#dce775' }}>{details.Status ? details.Status : 'Not Applicable'}</Col>
                                </Row>
                            </Alert> 
                        )
                        :
                        'Please wait...'
                    }
                    </Scrollbars>
                </div>                
                
                <div style={{textAlign:'center', marginTop:'10px'}}> 
                    <Button className='hvr-border-fade-orange' onClick={this.props.deviceupdatestatus} style={{marginTop:'5px', color:'#ff9800', fontSize:'15px', borderRadius:'5px', borderColor:'#ff9800', background:'transparent'}} outline >Close</Button>
                </div>
            </div>
        );
    }
}
export default DeviceStatus;