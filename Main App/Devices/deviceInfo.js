var redis = require('redis')
var client = redis.createClient();
var db = require('../sequelize.js')

var fetchDeviceList = (info,callback)=>{
  var query = {}

  client.get(info.token,function(err, reply){
    if(err){
      callback(Error('Unexpected Error!'),false)
    }
    else if(reply=='null'){
      callback(Error('Unexpected Error!'),false)
    }
    else {
      var userInfo = JSON.parse(reply)
      // console.log('USER INFORMATION')
      // console.log(userInfo)
      fetchQuery(userInfo,info.device)
        .then(result=>{
          callback(false,result.rows)
        })
        .catch(err=>{
          callback(Error(err.message),false)
        })
    }
  })
}

async function fetchQuery(userInfo,device){
  if(userInfo.role==2){
    // console.log(userInfo)
    var query = {
       text : 'SELECT subunit_info.city,subunit_info.state,subunit_info.country,devices.project_id,devices.srno,devices.status,devices.subunit_name from devices inner join subunit_info on devices.subunit_name=subunit_info.subunit_name where devices.project_id in (SELECT project_id from subunit_db where company_mail = $1) order by status, time desc',
       // text : 'SELECT * from devices where project_id in (SELECT project_id from subunit_db where company_mail = $1) order by status, time desc',
       values : [userInfo.company_mail]
    }
  }
  else if(device){
    var query = {
      text : 'SELECT * from devices where project_id = $1 AND srno = $2 order by status,time desc',
      values : [userInfo.project_id, device]
    }
  }
  else if(userInfo.project_id == 3){
    var query = {
      // text : 'SELECT * from users inner join user_profile on users.user_id = user_profile.user_id where username = $1',
      text : 'SELECT * from devices where public_user = $1 order by status,time desc',
      values : [userInfo.id]
    }
  }
  else {
    var query = {
      text : 'SELECT subunit_info.city,subunit_info.state,subunit_info.country,devices.project_id,devices.srno,devices.status,devices.subunit_name from devices inner join subunit_info on devices.subunit_name=subunit_info.subunit_name where devices.subunit_name = $1 order by status,time desc',
      // text : 'SELECT * from devices where subunit_name = $1 order by status,time desc',
      values : [userInfo.subunit_name]
    }
  }
  return db.query(await query)
}

module.exports = fetchDeviceList;
