var express = require('express');
var app = express();
const db = require('./sequelize');
const elastic = require('./elasticize');
var bodyParser=require('body-parser');
var tripstart = require('./tripstart');
var tripstop = require('./tripstop');
app.use(bodyParser.urlencoded({extended:false}))
app.use(bodyParser.json());
const redis = require('./redisize');


app.post('/trip',(req,res)=>{
  console.log(req.body);
  if(req.body.status=='start'){
       tripstart.setInRedis(req.body.vin)
   }
  if(req.body.status=='stop'){
       tripstop.getFromElastic(req.body.vin)
  }
})

app.listen(8000, function () {
  console.log('Example app listening on port 8000!');
 });
