import React, { Component } from 'react';
import {Col, Row, Button, Modal, ModalBody, Label, Alert} from 'reactstrap'
import { Scrollbars } from 'react-custom-scrollbars';
import axios from 'axios' ;
import DateFormat from 'dateformat' ;
import '../../Style/deviceliststatus.css'
import '../../Style/animationandtrasition.css'
import '../../Style/homemodel.css'
var fileDownload = require('js-file-download');

class DownloadLogs extends Component{
    constructor(props){
        super(props);
        this.state = {
            filterDeviceStatus:'',
            selectedvalue:'vin',
            confirm:false,
            selecteddevice:'',
            selecteddevicesrno:'',
            deviceList:'',
            getlogNotFount:false
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.updateDeviceList = this.updateDeviceList.bind(this);
        this.togglegetlogNotFound = this.togglegetlogNotFound.bind(this);
    }


    axiosFunc = () => {
        axios.defaults.headers.common['auth'] = sessionStorage.getItem('DontTouchMe');
        axios.get('http://8.9.36.13/factory/downloadlog?srno='+this.props.srno)
            .then(res => {
                if(res.status === 200){
                    //console.log(res.data.data)
                    let deviceReports = [];
                    for(let i=0; i<res.data.data.length; i++){
                            deviceReports.push({vin:res.data.data[i].vin ? res.data.data[i].vin : 'NA', log_start_tmp:res.data.data[i].log_start_tmp ? res.data.data[i].log_start_tmp : 'NA', log_stop_tmp:res.data.data[i].log_stop_tmp ? res.data.data[i].log_stop_tmp : 'NA'})
                    }
                    this.setState({deviceList: deviceReports});
                }
                else
                    console.log("Response code other than 200 ")
            }).catch( er => {
                if(er.response){
                    if(er.response.status === 401)
                        console.log("Unauthorized or Invalid credential")
                    if(er.response.status === 404){
                        console.log("Not Found")
                    }
                }
                else{
                    console.log("Error in response");
                }
            });
    };
    
    componentDidMount() {
        this.axiosFunc();
        this.interval = setInterval(this.axiosFunc, 5000);
    }

    componentWillReceiveProps(nextProps){
        if(nextProps !== this.props){
            this.props = nextProps
        }
    }
    
    handleInputChange(e) {
        this.setState({filterDeviceStatus:e.target.value});
    }

    handleChange(e){
            this.setState({selectedvalue:e.target.value})
    }

    updateDeviceList(e, ind, data){
        let datafordownload = {
            srno: this.props.srno,
            startTime: new Date(data.log_start_tmp),
            stopTime: new Date(data.log_stop_tmp)
        }

        axios.defaults.headers.common['auth'] = sessionStorage.getItem('DontTouchMe');
        axios.post('http://8.9.36.13/factory/downloadlog', datafordownload).then(res => {        
            if(res.status === 200){
                fileDownload(res.data, res.headers['last-modified']+'.asc');
            }
            else     
                console.log("Response code other than 200");
        }).catch( er => {
                if(er.response){
                    if(er.response.status === 401){
                        console.log("Unauthorized or Invalid credential");
                    }
                    if(er.response.status === 404){
                        this.setState({getlogNotFount: true})
                    }
                }
                else{
                    console.log("Error in response");
                }
        });
    }

    togglegetlogNotFound(){
        this.setState({
            getlogNotFount: !this.state.getlogNotFount
        })
    }

    componentWillUnmount(){
        clearInterval(this.interval);
    }


    render(){
        return(
            <div>
                <Modal size='sm' isOpen={this.state.getlogNotFount} centered={true} fade={false} toggle={this.togglegetlogNotFound}>
                    <ModalBody className='updatedevice'>
                        <div style={{textAlign:'center', fontSize:'15px'}}>Log is empty or Not Found</div>

                        <div style={{textAlign:'center', marginTop:'10px'}} >
                            <Button className='hvr-border-fade-orange' style={{borderRadius:'5px', backgroundColor:'transparent', color:'#ff9800', border:'1px solid #ff9800', fontSize:'15px', margin:'5px'}} onClick={this.togglegetlogNotFound}>Close</Button>
                            
                        </div>
                    </ModalBody>
                </Modal>

                <Row style={{marginTop:'12px'}} >

                    <Col xs='1' sm='1' md='1' lg='1'></Col>

                    <Col xs='12' sm='12' md='5' lg='5'>   
                        <Label className="onandfilter"> Filter </Label>         
                            <input type="text" name="filterDeviceStatus" id="filterDeviceStatus" value={this.state.filterDeviceStatus} onChange={this.handleInputChange} />                                    
                    </Col>
                    <Col xs='1' sm='1' md='0' lg='0'></Col>

                    <Col xs='12' sm='12' md='4' lg='4' className='onsmalldevice'>        
                        <Label className="onandfilter"> On </Label>          
                        <select onChange={(e)=>{this.handleChange(e, 'selected')}}  style={{color:'#ff9800 ', width:'60%', height:'100%', background:'#232228', border:'1px solid #ff9800'}} >
                            <option  value='vin'>VIN</option>
                            {/* <option  value='srno'>Serial No.</option> */}
                            <option  value='log_start_tmp'>Start Date</option>
                            <option  value='log_stop_tmp'>End Date</option>
                        </select>
                    </Col>
                </Row>

                <div>
                <Scrollbars
                    onScroll={this.handleScroll}
                    onScrollFrame={this.handleScrollFrame}
                    onScrollStart={this.handleScrollStart}
                    onScrollStop={this.handleScrollStop}
                    onUpdate={this.handleUpdate}
                    renderView={this.renderView}
                    renderTrackHorizontal={this.renderTrackHorizontal}
                    renderTrackVertical={this.renderTrackVertical}
                    renderThumbHorizontal={this.renderThumbHorizontal}
                    autoHide
                    autoHideTimeout={1000}
                    autoHideDuration={200}
                    autoHeight
                    autoHeightMin={0}
                    autoHeightMax={350}
                    thumbMinSize={30}
                    universal={true}
                    renderThumbVertical={props => < div {...props} className="thumb-vertical track-vertical"/>}
                    //renderTrackVertical={props => < div {...props} className="track-vertical"/>}
                    >   
                    <Alert className='checkwidth' fade={false} style={{fontSize:'15px', backgroundColor:'#ff9800', borderColor:'#ff9800', borderRadius:'10px', marginTop:'15px'}}>
                        <Row className="rowdeviceStatus" style={{textAlign:'center'}}>
                            {/* <Col xs='3' sm='3' md='3' lg='3'>Serial No</Col> */}
                            <Col xs='3' sm='3' md='3' lg='3'><b>VIN</b></Col>
                            <Col xs='3' sm='3' md='3' lg='3'><b>Start Date</b></Col>
                            <Col xs='3' sm='3' md='3' lg='3'><b>End Date</b></Col>
                            <Col xs='3' sm='3' md='3' lg='3'><b>Download</b></Col>
                        </Row>
                    </Alert>
                    </Scrollbars>
                </div>
                
                <div>
                    <Scrollbars
                    onScroll={this.handleScroll}
                    onScrollFrame={this.handleScrollFrame}
                    onScrollStart={this.handleScrollStart}
                    onScrollStop={this.handleScrollStop}
                    onUpdate={this.handleUpdate}
                    renderView={this.renderView}
                    renderTrackHorizontal={this.renderTrackHorizontal}
                    renderTrackVertical={this.renderTrackVertical}
                    renderThumbHorizontal={this.renderThumbHorizontal}
                    autoHide
                    autoHideTimeout={1000}
                    autoHideDuration={200}
                    autoHeight
                    autoHeightMin={0}
                    autoHeightMax={350}
                    thumbMinSize={30}
                    universal={true}
                    renderThumbVertical={props => < div {...props} className="thumb-vertical track-vertical"/>}
                    //renderTrackVertical={props => < div {...props} className="track-vertical"/>}
                    >   
                        
                        {this.state.deviceList ? 
                            this.state.deviceList.filter( (item, index) => item[this.state.selectedvalue].toLowerCase().includes(this.state.filterDeviceStatus.toLowerCase() )).map( (details, ind) =>  
                                <Alert className='checkwidth' key={ind} fade={false} style={{fontSize:'13px', backgroundColor:'#676A86', borderColor:'#676A86', borderRadius:'10px', color:'white'}}>  
                                    <Row className="rowdeviceStatusdata" style={{textAlign:'center'}}>
                                        {/* <Col  xs='3' sm='3' md='3' lg='3'> {details.srno ? details.srno : 'Not Applicable'} </Col> */}
                                        <Col  xs='3' sm='3' md='3' lg='3'> {details.vin ? details.vin : 'Not Applicable'} </Col>
                                        <Col  xs='3' sm='3' md='3' lg='3'> {details.log_start_tmp ? DateFormat(new Date(details.log_start_tmp),"yyyy-mm-dd h:MM:ss") : 'Not Applicable'} </Col>
                                        <Col  xs='3' sm='3' md='3' lg='3'> {details.log_stop_tmp ? DateFormat(new Date(details.log_stop_tmp),"yyyy-mm-dd h:MM:ss") : 'Not Applicable' }</Col>
                                        <Col  xs='3' sm='3' md='3' lg='3'> <Button className='hvr-border-fade' onClick={(e) => {this.updateDeviceList(e, ind, details)} } style={{marginTop:'0px' ,color:'white', fontSize:'15px', borderRadius:'5px', borderColor:'transparent', background:'transparent'}}> <i className="fa fa-download" aria-hidden="true"></i></Button> </Col>
                                    </Row>
                                </Alert> 

                            )
                            :
                            'Please wait...'
                        }
                    </Scrollbars>
                </div>                
                
                <div style={{textAlign:'center', marginTop:'10px'}}> 
                    <Button className='hvr-border-fade-orange' onClick={this.props.toggleDownloadlogs} style={{marginTop:'5px', color:'#ff9800', fontSize:'15px', borderRadius:'5px', borderColor:'#ff9800', background:'transparent'}} outline >Close</Button>
                </div>
            </div>
        )
    }
}
export default DownloadLogs;