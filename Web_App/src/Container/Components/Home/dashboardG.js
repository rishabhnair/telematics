import React, {Component } from 'react';
import {Row, Col} from 'reactstrap';
import Speedgauge from './speedometer';
import Rpmgauge from './rpmgauge';
import SpeedTimeAreaChart from './RechartsComponents/SpeedTimeAreaChart'
import Averagerpm from './RechartsComponents/Averagerpm'
import Throttleposition from './RechartsComponents/throttleposition';
import Enginefuelrate from './RechartsComponents/enginefuelrate';
import DateFormat from 'dateformat' ;
import axios from 'axios'
import '../../Style/home.css';
import '../../Style/animationandtrasition.css'

class DashboardG extends Component{
    constructor(props){
        super(props)
        this.state = {
            srno: this.props.srno,
            deviceDetails:'',
            speedtimegraphdata: [ {Speed: 0, Time: 0}, {Speed: 0, Time: 0}, {Speed: 0, Time: 0}, {Speed: 0, Time: 0}, {Speed: 0, Time: 0} ],
            rpmgraphdata: [{RPM:0, Time:0}, {RPM:0, Time:0}, {RPM:0, Time:0}, {RPM:0, Time:0}, {RPM:0, Time:0}],
            throttle: [{'Throttle position':0, Time:0}, {'Throttle position':0, Time:0}, {'Throttle position':0, Time:0}, {'Throttle position':0, Time:0}, {'Throttle position':0, Time:0}],
            enginefuelrate: [{'Engine fuel rate':0, Time:0}, {'Engine fuel rate':0, Time:0}, {'Engine fuel rate':0, Time:0}, {'Engine fuel rate':0, Time:0}, {'Engine fuel rate':0, Time:0}]
        }
    }

    axiosFunc = () => {
        let data = {
            vin : this.state.srno
        }
        axios.defaults.headers.common['auth'] = sessionStorage.getItem('DontTouchMe');
        axios.post('http://8.9.36.13/factory/timeseries', data).then(res => {
            if(res.status === 200){
                this.setState({deviceDetails: res.data.data[0]});
            }
            else
                console.log("Response code other than 200")
        }).catch( er => {
            if(er.response){
                if(er.response.status === 401)
                    console.log("Unauthorized or Invalid credential")
                if(er.response.status === 404){
                    console.log("Not Found")
                }
            }
            else{
                console.log("Error in response");
            }
        });
    };

    componentDidMount(){
        this.axiosFunc();
        this.interval = setInterval(this.axiosFunc, 5000);
    }

    componentWillReceiveProps(nextProps){
        if(nextProps !== this.props){
            this.props = nextProps
            this.setState({srno: nextProps.srno})
        }
    }

    componentWillUnmount(){
      clearInterval(this.interval);
    }

    render(){
        let vin, fueltype, SPEED, RPM, FUEL, EXHAUST, ENGINELOAD, COOLANT, INTAKEAIR, ENGINE, time, TIME;
          
        if(this.state.deviceDetails){
                vin = this.state.deviceDetails.vin;
                fueltype = this.state.deviceDetails.fueltype;
                SPEED = this.state.deviceDetails.p2;
                RPM =this.state.deviceDetails.p3;
                FUEL=this.state.deviceDetails.p5;
                EXHAUST = this.state.deviceDetails.p8;
                ENGINELOAD=this.state.deviceDetails.p9;
                COOLANT=this.state.deviceDetails.p2;
                INTAKEAIR=this.state.deviceDetails.p3;
                ENGINE = this.state.deviceDetails.p5;
                time = DateFormat(this.state.deviceDetails.time,"yyyy-mm-dd h:MM:ss");
                TIME = time.slice(10, 19);
        }
    
        return(
            <div style={{fontSize:'15px'}}>
                <Row className='firstrow'>
                    <Col xs='12' sm="6" md="6" lg="5" className='firstrowfirstcol'>                        
                        <Row style={{textAlign:'left', marginBottom:'10px'}}>
                            <Col xs='12' sm="12" md="12" lg="6">
                                <Row>
                                    <Col xs='4' sm="4" md="4" lg="4" className="homeh">VIN</Col>
                                    <Col xs='8' sm="8" md="8" lg="8" className="homeh1">{vin ? vin : 'Not Applicable'} </Col>
                                </Row>
                            </Col>

                            <Col xs='12' sm="12" md="12" lg="6">
                                <Row>
                                    <Col xs='6' sm="6" md="6" lg="6" className="homeh"> Fuel Type</Col>
                                    <Col xs='6' sm="6" md="6" lg="6" className="homeh1"> {fueltype ? fueltype : 'NA'} </Col>
                                </Row>
                            </Col>
                        </Row>

                        <Row>
                            <Col xs='12' sm="6" md="6" lg="6" className='speedgaugerow'>
                                <Speedgauge speed={SPEED ? SPEED : 0} />
                                <div className='bottomheadingmargin'>Speed</div>
                            </Col>

                            <Col xs='12' sm="6" md="6" lg="6" className='speedgaugecol'>
                                <Rpmgauge value={RPM ? RPM : 0} rpm={'rpm'} />
                                <div className='bottomheadingmargin' style={{marginTop:'20px'}}>RPM </div>  
                            </Col>
                            
                        </Row>
                    </Col>

                    <Col xs='12' sm="6" md="6" lg="7" className='firstrowsecondcol'>
                        
                        <SpeedTimeAreaChart TIME={TIME ? TIME : 0} value={SPEED ? SPEED : 0} rpmgraphdata={this.state.speedtimegraphdata}/>
                    </Col>
                </Row>

                <Row className='secondrow' >
                    <Col xs='12'sm="8" md="8" lg="4" className='firstrowfirstcol'>
                        <div className="topheadingmargin"> Pressure </div>
                        <Row>
                            <Col xs='6'sm="6" md="6" lg="6"> 
                                <Rpmgauge value={FUEL ? FUEL : 0}  fuel={'fuel'}/>   
                                <div className='bottomheadingmargin'>Fuel </div>
                            </Col>

                            <Col xs='6'sm="6" md="6" lg="6"> 
                                <Rpmgauge  value={EXHAUST ? EXHAUST : 0} exhaust={'exhaust'}/>   
                                <div className='bottomheadingmargin'>Exhaust </div>
                            </Col>
                        </Row>
                    </Col>

                    <Col xs='12' sm="4" md="4" lg="2" className='secondrowsecondcol'>
                        <div className="topheadingmargin"> Engine Load </div>
                            <Rpmgauge value={ENGINELOAD ? ENGINELOAD : 0} engineload={'engineload'} />   
                        <div className='bottomheadingmargin'>Calculated Load </div>
                    </Col>

                    <Col xs='12' sm="12" md="12" lg="6" className='firstrowsecondcol'>
                        <div className="topheadingmargin"> Temperature </div>
                        <Row>
                            <Col xs='6' sm="4" md="4" lg="4"> 
                                <Rpmgauge value={COOLANT ? COOLANT : 0}  coolant={'coolant'} />
                                <div className='bottomheadingmargin'>Coolant </div>
                            </Col>

                            <Col xs='6' sm="4" md="4" lg="4"> 
                                <Rpmgauge value={INTAKEAIR ? INTAKEAIR : 0} intakeair={'intakeair'} />
                                <div className='bottomheadingmargin'>Intake Air </div>
                            </Col>

                            <Col xs='12' sm="4" md="4" lg="4"> 
                                <Rpmgauge value={ENGINE ? ENGINE : 0} engine={'engine'} />
                                <div className='bottomheadingmargin'>Engine </div>
                            </Col>
                        </Row>
                    </Col>
                </Row>

                <Row className='thirdrow'>
                    <Col xs='12' sm="12" md="12" lg="4">
                        <Averagerpm TIME={TIME ? TIME : 0} value={RPM ? RPM : 0} value1={SPEED ? SPEED : 0} rpmgraphdata={this.state.rpmgraphdata} averagerpm={'Rpm'} />
                    </Col>

                    <Col xs='12' sm="12" md="12" lg="4">
                        <Enginefuelrate TIME={TIME ? TIME : 0} value={ENGINE ? ENGINE : 0} value1={SPEED ? SPEED : 0} rpmgraphdata={this.state.enginefuelrate} averagerpm={'Engine Fuel Rate'} />
                    </Col>

                    <Col xs='12' sm="12" md="12" lg="4">
                        <Throttleposition TIME={TIME ? TIME : 0} value={EXHAUST ? EXHAUST : 0} value1={SPEED ? SPEED : 0} rpmgraphdata={this.state.throttle} averagerpm={'Throttle Position'}/>
                    </Col>
                </Row>
            </div>
        )
    }
}
export default DashboardG;
