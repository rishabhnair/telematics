import React, {Component} from 'react';
import {Button, Form, FormGroup} from 'reactstrap'
import axios from 'axios';
import '../../Style/login.css'
import '../../Style/header.css'
import '../../Style/homemodel.css'
import '../../Style/animationandtrasition.css'
import '../../Style/forgotpassword.css'
var base64 = require('base-64');
var utf8 = require('utf8');

class ForgotPassword extends Component{
    constructor(props){
        super(props);
        this.state={
            emails:'',
            signupfirst:true,
            signupsecond:false,

            codes2:'',
            passs21:'',
            passs22:'',
            checkconfirmpassword:true,

            specificupdatemetadata:true,
            signup1:true,

            passwordType:'password',

            verificationcode:false,
            verificationcodemsg:'Refer your email',
            verificationcodemsgoncheck:'',
            invalidDetails:'',
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmitSpecificUpdatemetadata = this.handleSubmitSpecificUpdatemetadata.bind(this);
        this.handleSubmitSignup2 = this.handleSubmitSignup2.bind(this);
        this.goback = this.goback.bind(this);
        this.showhidepassword = this.showhidepassword.bind(this);
    }

    showhidepassword(e){
        this.setState({
            passwordType:this.state.passwordType === 'text' ? 'password' : 'text'
        });
    }

    goback(e){
        this.setState({
            signupfirst:true,
            signupsecond:false
        })
    }

    handleChange(e) {
        const target = e.target;
        const name  = target.name;
        const value = target.value;

        if(this.state.emails.length !== 0){
            this.setState({signup1 : false})
        }
        else{
            this.setState({signup1 : true})
        }

        if(name === 'codes2' || name === 'passs21' || name === 'passs22'){
            if(this.state.codes2.length !== 0 && this.state.passs21.length !==0 && this.state.passs22.length !==0){
                this.setState({specificupdatemetadata : false})
            }
            else{
                this.setState({specificupdatemetadata : true})
            }
        }

        this.setState({
            [name]: value
        });
    }

    handleSubmitSpecificUpdatemetadata(event){
        event.preventDefault();
        this.setState({invalidDetails:''})

        if(this.state.emails){
            axios.get('http://8.9.36.13/factory/updatePass?username='+this.state.emails).then(res => {  
                if(res.status === 200){
                    this.setState({signupfirst:false, signupsecond:true});
                    this.setState({verificationcode:true, verificationcodemsgoncheck:''});
                }
                else{
                    console.log("Response status code other than 200");
                }
            }).catch( er => {
                if(er.response){
                    if(er.response.status === 401){
                        this.setState({invalidDetails:'Unregistered email id'})
                    }
                    else if(er.response.status === 404){
                        console.log("Not Found")
                    }
                    else{
                        console.log("Network error")
                    }
                }
                else{
                    console.log("Error in response");
                }
            });
        }
        else{
            this.setState({invalidDetails:'Email is required'})
        }
    }

    handleSubmitSignup2(event){
        event.preventDefault();
        this.setState({checkconfirmpassword:true, verificationcode:false, verificationcodemsgoncheck:'', invalidDetails:''})
        
        if(this.state.passs21 !== this.state.passs22){
            this.setState({checkconfirmpassword :false, verificationcode:true})
        }
        else{
            this.setState({checkconfirmpassword :true})   
            var bytes = utf8.encode(this.state.passs21);
            var encoded = base64.encode(bytes);

            let signupsecond = {
                code : this.state.codes2,
                password : encoded,
            }
            
            if(this.state.codes2 && this.state.passs21 && this.state.passs22){
                axios.post('http://8.9.36.13/factory/updatePass', signupsecond).then(res => {  
                    if(res.status === 200){
                        this.props.toggleForgot();
                    }
                    else{
                        console.log("Response status code other than 200");
                    }
                }).catch( er => {
                    if(er.response){
                        if(er.response.status === 401){
                            this.setState({verificationcode:false, verificationcodemsgoncheck:'Invalid or Expired Code'})
                        }
                        if(er.response.status === 404){
                            console.log("Not Found")
                        }
                    }
                    else{
                        console.log("Error in response");
                    }
                });
            }
            else{
                this.setState({invalidDetails:'Verification code is required'})
            }
        }   
    }

    render(){
        return(
            <div> 
                <div className='l-loginModalDiv'> 

                    {this.state.signupfirst ? 
                        <Form onSubmit={this.handleSubmitSpecificUpdatemetadata} style={{marginTop:'40px', color:'#9e9e9e', fontSize:'15px'}}>
                            <FormGroup>
                                    <input  type="email" name="emails" id="emails" placeholder="Enter Email Id" value={this.state.emails} onChange={this.handleChange}/>
                            </FormGroup>
                            {this.state.invalidDetails ? <span style={{color:'#ff0000'}}>{this.state.invalidDetails}</span> : '' }<br/>
                            <Button type="submit" disabled={this.state.signup1} className='hvr-border-fade-orange' style={{borderRadius:'5px', margin:'15px', color:'#ff9800', border:'1px solid #ff9800',backgroundColor:'transparent', fontSize:'15px', width:'23%'}} outline > Next </Button>
                        </Form>
                        :
                        ''
                    }
        
                    {this.state.signupsecond ? 
                        <Form onSubmit={this.handleSubmitSignup2} style={{marginTop:'40px', color:'#9e9e9e', fontSize:'15px'}}>                                                                                                                            
                            <FormGroup>                                        
                                    <input  type="text" name="codes2" id="codes2" placeholder="Verification Code" value={this.state.codes2} onChange={this.handleChange} />
                                    <br/>{this.state.verificationcode ? <span style={{color:'green'}}>{this.state.verificationcodemsg}</span> : '' }
                                    <br/>{this.state.verificationcodemsgoncheck ? <span style={{color:'#ff0000'}}>{this.state.verificationcodemsgoncheck}</span> : '' }                                          
                            </FormGroup>

                            <FormGroup>
                                    <input  type={this.state.passwordType} name="passs21" id="passs21" placeholder="Password" value={this.state.passs21} minLength={8} onChange={this.handleChange} />
                            </FormGroup>

                            <FormGroup>
                                    <input style={{marginLeft:'31px'}}  type={this.state.passwordType} name="passs22" id="passs22" placeholder="Confirm Password" value={this.state.passs22} minLength={8} onChange={this.handleChange} /> <Button  onClick={this.showhidepassword} style={{marginTop:'0px' ,background:'transparent', border:'transparent', fontSize:'16px', color:'grey'}}><i className="fa fa-eye" aria-hidden="true"></i></Button>
                                    <br/>{this.state.checkconfirmpassword ? '' : <span style={{color:'#ff0000'}}>Those passwords didn't match. Try again.</span>}  
                            </FormGroup>
                            {this.state.invalidDetails ? <span style={{color:'#ff0000'}}>{this.state.invalidDetails}</span> : '' }<br/>
                            <Button onClick={this.goback} className='hvr-border-fade-orange' style={{borderRadius:'5px', margin:'10px', color:'#ff9800', border:'1px solid #ff9800',backgroundColor:'transparent', fontSize:'15px', width:'23%'}} outline > Go Back </Button>
                            <Button type="submit" disabled={this.state.specificupdatemetadata} className='hvr-border-fade-orange' style={{borderRadius:'5px', margin:'10px', color:'#ff9800', border:'1px solid #ff9800',backgroundColor:'transparent', fontSize:'15px', width:'23%'}} outline > Update </Button> 
                        </Form> 
                            :
                            ''
                    }
                </div> 

                <div className='closebuttondiv'> 
                    <Button  onClick={this.props.toggleForgot} className='hvr-border-fade-orange'  style={{borderRadius:'5px', margin:'10px', color:'#ff9800', border:'1px solid #ff9800', backgroundColor:'transparent', fontSize:'15px', width:'23%'}} outline > Close </Button>
                </div>
            </div>
        )
    }
}
export default ForgotPassword;