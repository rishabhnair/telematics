var hexToBinary = require('hex-to-binary');

var dtcDecoder = (msg,callback)=>{
	callback(msg)
	return;
	var test = msg.replace(/[\s0-9a-fA-F]/g,'')
	msg = msg.replace(/\s/g,'')
	//console.log(msg.length)
	if(test.length>0){
		callback("error")
	}
	else{

	var fc = {
		'00' : 'P',
		'01' : 'C',
		'10' : 'B',
		'11' : 'U'
	}
	var sc = {
		'00' : '0',
		'01' : '1',
		'10' : '2',
		'11' : '3'
	}
	var output = []
	var index = 0;
	var x = 6;
	while(x<msg.length){
		var first = hexToBinary(msg.slice(x,x+1))
		var input1 = fc[first.slice(0,2)]
		var input2 = sc[first.slice(2,4)]
		var input3 = msg[x+1]
		var input4 = msg.slice(x+2,x+4)
		output[index] = (input1+input2+input3+input4)
		//console.log(output)
		index = index + 1
		x = x+8
		}

	}
	if(x>=msg.length){
		callback(output)
	}

}

module.exports = dtcDecoder
