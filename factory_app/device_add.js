var fs = require('fs');
var mqtt = require('mqtt')
const pg = require('pg')
var express = require ('express');
const bcrypt = require('./password');
var bodyParser = require('body-parser');


var deviceadd = (req,res,next)=>{

if(req.body.sr_no.trim()=='' || req.body.imei_no.trim()==''){
	res.render('register.hbs',{error : "Invalid input parameters"})
}
else{
var hash = req.body.sr_no + req.body.imei_no;

var config = {
		"domain" : '8.9.36.13:5432',
		"serial_no" : req.body.sr_no,
		"IMEI" : req.body.imei_no,
		"device_hash" : hash,
		"para_pub_topic" : 'param/' +  req.body.sr_no,
		"can_pub_topic" : 'can_data/' +  req.body.sr_no,
		"sub_topic" : 'sub/' +  req.body.sr_no,
		"pub_topic" : 'pub/' +	req.body.sr_no,
		"lastwill_pub" : 'issue/' +	req.body.sr_no + '/lwt',
		"username" : '',
		"password" : ''
	}
	fs.writeFileSync('${hash}.config_file',JSON.stringify(config, null,2));
	const now = new Date();
	var lastwill = config.para_pub_topic + '/lastwill';
	const connectionString = "postgresql://postgres:8hG$PfCjj[Xz!+tz@8.9.36.13:5432/postgres";
	console.log(config);
	const client = new pg.Client(connectionString);
	client.connect();
	const text = {
  	text: 'INSERT into devices (device_id,sr_no,para_pub,default_sub,default_pub,lastwill_pub,can_pub,reg_date,updated_on,imei_no,updated_by,owner) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,1,$11)',
  	values: [config.device_hash,config.serial_no,config.para_pub_topic,config.sub_topic,config.pub_topic,config.lastwill_pub,config.can_pub_topic,now,now,config.IMEI,'a']
	}
	client.query(text)
  	.then(res => {
  		console.log("database updated");
  		next()
	})
		.catch(e => {console.log(e.stack)
			res.render('register.hbs',{error : "Invalid input parameters", device_info:"Null"})
	});
 };
}

 module.exports = deviceadd;
