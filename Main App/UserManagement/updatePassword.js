var redis = require('redis')
var client = redis.createClient()
var db = require('../sequelize.js')
var mailer = require('./nodemailer.js')

var updatePassword = (data,callback)=>{
  if(data.auth){
    var rand = parseInt(Math.random()*100000)
    client.set(rand,JSON.stringify(data))
    var query = {
      text : 'SELECT * from users where username = $1',
      values : [data.user]
    }
    db.query(query)
    .then(res=>{
      if(res.rows.length>0){
        mailer(data.user,rand,function(err,reply){
          if(err){
            console.log(err)
            callback(err,false)
          }
          else {
            console.log(reply)
            callback(false,true)
          }
        })
      }
      else {
          callback(true,false)
      }
    })
  }
  else {
    var code = data.code;
    var pass = data.pass;
    console.log({data})

    client.get(code,function(err,reply){
      var userInfo = JSON.parse(reply)
      console.log({userInfo})
      if(err){
        console.log({err})
        callback(Error('Something went wong'),false)
      }
      else if(reply=='null' || reply == null){
        console.log({err})
        callback(Error('Invalid or Expired Code'),false)
      }
      else {
        console.log({reply})
        var query = {
          text : 'UPDATE users SET password = $1 where username = $2',
          values : [pass,userInfo.user]
        }
        db.query(query)
        .then(result=>{
          callback(false,'success')
        })
        .catch(err=>{
          callback(err,true)
        })
      }
    })
  }
}

module.exports = updatePassword;
