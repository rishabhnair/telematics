import React from 'react';
import { Switch, Route, Redirect, HashRouter} from 'react-router-dom';
import Entry from '../Login/login'
import Home from '../Home/home'

    const checkAuth = () =>{
      const token = sessionStorage.getItem('DontTouchMe');
      return token;
    }

    const AuthRoute1 = ({ component: Component, ...rest }) => (
      <Route {...rest} render={props => (
        !checkAuth() ? (
          <Component {...props}/>
        ) : (
          <Redirect to={{ pathname: '/'}} />
          
        )
      )}/>
    )

    const AuthRoute = ({ component: Component, ...rest }) => (
        <Route {...rest} render={props => (
          checkAuth() ? (
            <Component {...props}/>
          ) : (
            <Redirect to={{ pathname: '/login'}} />  
          )
        )}/>
      )

      export default () => (
        <HashRouter>
            <Switch>
              <AuthRoute1 path="/login" component={Entry}/>
              <AuthRoute exact path = '/' component={Home} />
            </Switch>
        </HashRouter>
      );
