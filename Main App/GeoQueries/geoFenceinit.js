var db = require('../sequelize.js')
var FetchDevices = require('./geoFenceQuery.js')
var notifyFence = require('./notifyFence.js')
//Query For Geofence Every
var geoF = ()=>{
  var query = {
    text : 'SELECT * from subunit_db inner join subunit_info on subunit_db.subunit_name = subunit_info.subunit_name',
    values : []
  }
  db.query(query)
  .then(result=>{
    if(result.rows.length>0){
      result.rows.forEach(function(ele){
        var data = {
            lat : ele.geolat || ele.lat,
            lon: ele.geolon || ele.lon,
            project_id : ele.project_id,
            geodist : ele.geodist
          }
        var vinQuery = {
          text : 'SELECT srno from devices where project_id = $1 AND status = $2',
          values : [ele.project_id,'Connected']
        }
        db.query(vinQuery)
        .then(vins=>{
          data['vins'] = []
          vins.rows.forEach(function(x){
            data['vins'].push(x.srno)
          })
          FetchDevices(data.vins,data.lat,data.lon,data.dist,function(err,reply){
            if(err){
              console.log(err)
            }
            else {
              console.log({reply})
              for(let i=0; i<reply.length; i++){
                notifyFence(reply[i])
              }
            }
          })
        })
        .catch(err=>{
          console.log('something went wrong')
        })
      })
    }
    else {
      console.log('something went wrong')
    }

  })
  .catch(err=>{
    console.log('something went wrong')
  })
}

module.exports = geoF;
