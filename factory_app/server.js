var express = require ('express');
var bodyParser = require('body-parser');
var fs = require('fs');
var mqtt = require('mqtt')
const pg = require('pg')
const bcrypt = require('./bcrypt.js')
const port = process.env.PORT|| 3000;
var app = express();
var hbs = require('hbs');
const ejs = require('ejs');
const device_add = require('./device_add.js');
app.use(express.static(__dirname + '/views'));

app.get('/', (req, res) => {
	res.sendFile(__dirname + '/main.html');
 });

app.set('view engine',ejs);
app.use(bodyParser.urlencoded({ extended: false }));

app.use(bodyParser.json());


app.route('/factory_add')
.post(device_add,(req, res, next)=>{
	//console.log(req.body);
	const connectionString = "postgresql://postgres:8hG$PfCjj[Xz!+tz@8.9.36.13:5432/postgres";
	const client = new pg.Client(connectionString);
	client.connect();
	var selectQuery = {
		text : 'select device_id,para_pub,can_pub,default_sub from devices where sr_no = $1',
		values : [req.body.user_login]
	}
	client.query(selectQuery)
  	.then(result => {

  		console.log(result.rows);
  		res.render('register.ejs',{device_hash: result.rows[0].device_id})
	})
	.catch(e => console.error(e.stack));

	//res.redirect('/register.html')
	//res.send('hello')

 })


app.listen(port, () =>{
	console.log(`server is up on ${port}`);
});
