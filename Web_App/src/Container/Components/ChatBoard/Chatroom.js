import React, { Component } from 'react';
import { Widget, addResponseMessage} from 'react-chat-widget';
import axios from 'axios'
import DateFormat from 'dateformat' ;
import 'react-chat-widget/lib/styles.css';
import '../../Style/chatboard.css'

class App extends Component {
    constructor(props){
        super(props);
        this.state = {
            msg:'',
            resp:'',
            srno:this.props.srno,
            networkerror:''
        }
    }
    
    componentDidMount(){
        if(this.state.msg){
            let msg = {
                msg: this.state.msg,
                srno: this.state.srno
            }
            axios.defaults.headers.common['auth'] = sessionStorage.getItem('DontTouchMe');
            axios.post('http://8.9.36.13/factory/reqresChat', msg, {timeout: 60000}).then(res => {  
                if(res.status === 200){
                    this.setState({resp: res.data})
                    if(res.data !== '')
                        addResponseMessage(res.data+DateFormat(new Date(new Date() ),"yyyy-mm-dd h:MM:ss TT").slice(10,15)+DateFormat(new Date(new Date() ),"yyyy-mm-dd h:MM:ss TT").slice(18,22));
                }
            }).catch( er => {
                if(er.response){
                    if(er.response.status === 401){
                        console.log("Unauthorized or Invalid credential")
                    }
                    if(er.response.status === 404){
                    console.log("Not Found")
                    }
                }
                else{
                    console.log("Error in response");
                }
            });
        }
    }

    componentWillReceiveProps(nextProps){
        if(nextProps !== this.props){
            this.props = nextProps
            this.setState({srno: nextProps.srno})
        }
    }

   async handleNewUserMessage1(newMessage){
       //console.log(newMessage)
        await this.setState({
            msg: newMessage
        })
        await this.componentDidMount()
    }

    render(){
        return (
        <div className="App">
            <Widget
                handleNewUserMessage={this.handleNewUserMessage1.bind(this)}
                senderPlaceHolder = 'Type a message...'
                title="Chat With Device"
                subtitle=''
            />
        </div>
        );
    }
}
export default App;