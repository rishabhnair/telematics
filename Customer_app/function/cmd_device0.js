var db = require('./sequelize')
var mqtt = require('mqtt')

var cmd_device = (device_info)=>{
    var client  = mqtt.connect('tcp://0.0.0.0:1883');
    var topic = 'sub/'+device_info.sr_no;
    client.on('connect', function(){
  		console.log('connected');
  	client.publish(topic,device_info.msg);
  })
  client.end();
}

module.exports = cmd_device;
