var mqtt = require('mqtt');
var db = require('../sequelize.js')
var mqttListen = require('./mqttListen.js')
var sendsftpPath = function(row,formatted,Username){
  // mqttListen()
  console.log({row,formatted,Username})
  console.log(row.length)
  // var SrNo
  // var UpdateToVersion
  var client = mqtt.connect('tcp://149.28.63.75:1883');
  for(var i=0; i<row.length; i++){
    let SrNo =row[i].srno;
    let ECU = row[i].ECU;
    let UpdateToVersion = row[i].UpdateToVersion
    // console.log(`row length ${row.length}`)
    console.log(`row ${i} : ${JSON.stringify(row[i])}`)

    client.on('connect', function () {
      var query = {
        text : `select "File_Path" from "sftpMetadata" where ("ECU" = $1 AND "Available_Version" = $2)`,
        values : [ECU, UpdateToVersion]
      }
      db.query(query)
      .then(result => {
        client.publish(`update/${SrNo}`, `${JSON.stringify(result.rows[0])}`)
        console.log(`Sent ${JSON.stringify(result.rows[0])} \ton Device: ${SrNo}`)
        mqttListen(SrNo)
      })
      .catch(function(err){
        console.log('error in query')
        console.log(err);
      })
  })
  }


}

module.exports = {
  sendsftpPath
}
