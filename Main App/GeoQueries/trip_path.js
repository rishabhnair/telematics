const db = require('../sequelize');

var getdetails = function(vin,start_time,stop_time,callback){
  var query={
    text : 'select start_time,stop_time,start_location,stop_location,path from trip_details where vin = $1 AND start_time = $2 AND stop_time=$3',
    values : [vin,new Date(start_time),new Date(stop_time)]
  }
  db.query(query)
  .then(result=>{
    callback(false,result.rows)
 }, function(err) {
     console.trace(err.message);
     callback(err.message,false)
 })
 .catch(err=>{
   callback('err',false)
 })
}

module.exports = {
  getdetails
}
