var express = require('express');
var app = express();
const db = require('./sequelize');
const elastic = require('./elasticize');
var bodyParser=require('body-parser');
var tripstart = require('./tripstart');
var queryElastic = require('./queryElastic');
app.use(bodyParser.urlencoded({extended:false}))
app.use(bodyParser.json());
const redis = require('./redisize');


app.post('/timeseries',(req,res)=>{
  console.log(req.body);
       queryElastic.getFromElastic(req.body.vin,13,function(err,reply){
         if(err){
           res.status(401).json({error : err})
         }
         else {
           res.status(200).json({data : reply})
         }
       })
})

app.listen(8000, function () {
  console.log('Example app listening on port 8000!');
 });
