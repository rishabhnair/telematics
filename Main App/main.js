var express = require('express');
var bodyParser=require('body-parser');

var userValues = require('./userValues.js')

var login = require('./UserManagement/login.js')
var signup = require('./UserManagement/signup.js')
var auth = require('./UserManagement/auth.js')
var checkUser = require('./UserManagement/checkUser.js')
var updatePassword = require('./UserManagement/updatePassword.js')
var fetchUserProfile = require('./UserManagement/fetchUserProfile.js')
var pubUserDevice = require('./UserManagement/pubUserDevice.js')
var userProfile = require('./UserManagement/user_profile.js')

var fetchDeviceList = require('./Devices/deviceInfo.js')
var bulkRegisterDevices = require('./Devices/bulkRegisterDevices.js')
var registerDevice = require('./Devices/registerDevice.js')
var addDevice = require('./Devices/addDevice.js')
var deleteDevice = require('./Devices/deleteDevice.js')

var reqres = require('./ChatReqRes/reqres.js')


var startLog = require('./PlatformService/startLog.js')
var stopLog = require('./PlatformService/stopLog.js')
var cmd_device = require('./PlatformService/cmd_device.js')
var getDevices = require('./PlatformService/getDevice.js')
var getVin = require('./PlatformService/getVin.js')
var batch_file = require('./PlatformService/batch_file.js')
var fetchCanLog = require('./PlatformService/fetchCanLog.js')

const getCSV = require('get-csv');

const elastic = require('./GeoQueries/elasticize.js');
var queryElastic = require('./GeoQueries/queryElastic.js');
var getFromElastic = require('./GeoQueries/getLocation.js')
var tripstart = require('./GeoQueries/tripstart');
var tripstop = require('./GeoQueries/tripstop');
var tripdetails = require('./GeoQueries/trip_details.js')
var trippath = require('./GeoQueries/trip_path.js')
var alldevices = require('./GeoQueries/alldevices.js')
var device = require('./GeoQueries/device.js')
var geoF = require('./GeoQueries/geoFenceinit.js')
var scdetails = require('./GeoQueries/nearby.js')

var mgrsignup = require('./fft/mgrsignup.js')
var fftsignup = require('./fft//fftsignup.js')

var report = require('./updateDevices/report.js')
var bulkMetadata = require('./updateDevices/bulkMetadata.js')
var singleMetadata = require('./updateDevices/singleMetadata.js')
var singleDevice = require('./updateDevices/singDevice.js')
var bulkUpdate = require('./updateDevices/bulkUpdate.js')
var getmetadata = require('./updateDevices/getmetadata.js')

var getAlerts = require('./alerts/getAlerts.js')
var geoConfig = require('./geoConfig.js')
// var alertsFunc = require('./alerts/getAlerts.js')

var app=express();
var fs = require('fs')
app.use(bodyParser.urlencoded({extended:false}))
app.use(bodyParser.json());
var cookieParser = require('cookie-parser');
app.use(cookieParser());
var upload = require('express-fileupload');
app.use(upload());


app.all("*", (req, res, next)=>{
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With, auth');
  res.header('Access-Control-Allow-Credentials', true);
  // console.log(req.headers);
  // console.log(req.body);
  // console.log('----gateway----');
  if(req.method === 'OPTIONS') {
        res.sendStatus(200);
     } else {
       next();
     }
});

app.route('/login')
.post(login,(req,res)=>{
  res.status(200).send('logged in')
})

app.route('/signup')
.post(signup,(req,res)=>{
  res.status(200).send('signed up')
})
.get(auth,(req,res)=>{

})

app.route('/auth')
.post(auth,(req,res)=>{

})

app.route('/addDevice')
.post((req,res)=>{
  console.log('entered')
  var info = {
    srno : req.body.srno,
    imei : req.body.imei || req.body.srno
  }
  addDevice(info,function(err, reply){
    if(err){
      console.log(err)
      res.status(401).json({ error : err })
    }
    else {
      console.log(reply)
      res.status(200).json({success : reply })
    }
  })
})

app.route('/registerDevice')
.post(checkUser,(req,res)=>{
  console.log(req.files)
  if(req.files){
    var files = req.files;
    console.log(files)
    for(key in files){
      var file = files[key]
      bulkRegisterDevices(file,req.headers.auth,function(err, reply){
        if(err){
          res.status(400).json(err)
        }
        else {
          res.status(200).json(reply)
        }
      })
      break;
    }
  }
  else {
    var info = {
      srno : req.body.srno,
      imei : req.body.imei || req.body.srno,
      token : req.headers.auth
    }
    userValues(req.headers.auth,function(err,userInfo){
      if(err){
        res.status(401).json({error : 'Unauthorized access'})
      }
      else if(userInfo.project_id == 3){
        pubUserDevice(userInfo.id,info.srno,function(err,reply){
          if(err){
            res.status(401).json({error : 'Unauthorized access'})
          }
          else {
            res.status(200).json({success : 'User Device Registered'})
          }
        })
      }
      else {
        registerDevice(info)
        .then(result=>{
          res.status(200).json({success : 'Device Registered'})
        })
        .catch(err=>{
          console.log({err})
          res.status(401).json(err)
        })
      }
    })
  }
})

app.route('/deviceinfo*')
.get(checkUser,(req,res)=>{
  var empty = ''
  var info = {
    token : req.headers.auth,
    device : req.query.device || empty,
  }
  // console.log(info)
  fetchDeviceList(info,function(err, reply){
    if(err){
      res.status(401).json({error: err.message})
    }
    else {
      res.status(200).json({devices : reply})
    }
  })
})

app.route('/updatePass*')
.get((req,res)=>{
  console.log(req.query)
  var data = {
    auth : true,
    user : req.query.username
  }
  updatePassword(data,function(err,reply){
    if(err){
      res.status(401).json({error: err.message})
    }
    else {
      res.status(200).json({success: 'Acivation mail has been sent'})
    }
  })
})
.post((req,res)=>{
  var data = {
    auth : false,
    code : req.body.code,
    pass : req.body.password
  }
  console.log({data})
  updatePassword(data,function(err,reply){
    if(err){
      res.status(401).json({error: err.message})
    }
    else {
      res.status(200).json({success: 'Acivation mail has been sent'})
    }
  })
})

app.route('/deleteDevice')
.post(checkUser,(req,res)=>{
  var info = {
    token : req.headers.auth,
    srno : req.body.srno
  }
  console.log(info)
  deleteDevice(info,function(err, reply){
    if(err){
      res.status(401).json({error : err})
    }
    else {
      res.status(200).json({ success : reply})
    }
  })
})


app.get('/fotareport',(req,res)=>{
  console.log(`Token: ${req.headers.auth}`)
  userValues(req.headers.auth,function(err,reply){
    if(err){
      res.status(401).json({error : err})
    }
    else{
      report.reportDetails(reply.srno,function(err,reply){
        if(err){
          res.status(401).json({error : err})
        }
        else {
          res.status(200).json({data : reply})
        }
      })
    }
  })
})

app.get('/locationall',(req,res)=>{
  userValues(req.headers.auth,function(err,reply){
    if(err){
      res.status(401).json({error : err})
    }
    else{
      alldevices.getlocation(reply.srno,function(err,reply){
        if(err){
          res.status(401).json({error : err})
        }
        else {
          res.status(200).json({data : reply})
        }
      })
    }
  })
})

app.post('/location',(req,res)=>{
  console.log(req.body);
  device.getlocation(req.body.srno,function(err,reply){
    if(err){
      res.status(401).json({error : err})
    }
    else {
      res.status(200).json({data : reply})
    }
  })
})


app.route('/reqresChat')
.post((req,res)=>{
  if(req.body.srno){
    reqres(req.body,function(err, reply){
      if(err){
        res.status(401).json({error : err})
      }
      else {
        res.status(200).json(reply)
      }
    })
  }
  else{
    res.status(401).json({error : 'Invalid Parameters'})
  }
})

app.route('/userProfile')
.post(checkUser,(req,res)=>{
  // console.log(req.body)
  userValues(req.headers.auth,function(err,userInfo){
    if(err){
      res.status(401).json({ error : 'Unauthorized User' })
    }
    else {
      userProfile(userInfo,req.body,function(err,reply){
        if(err){
          res.status(401).json({ error : err })
        }
        else {
          res.status(200).json({ success : reply })
        }
      })
    }
  })
})
.get(checkUser,(req,res)=>{
  // console.log(req.headers.auth)
  token = req.headers.auth;
  fetchUserProfile(token,function(err, reply){
    if(err){
      res.status(401).json({error : err})
    }
    else {
      res.status(200).json(reply)
    }
  })
})



app.route('/command')
.get((req,res)=>{

})
.post((req,res)=>{
  console.log(req.body)
  if(req.body.msg=='start'){
    startLog(req.body,function(){
      console.log('callbacks here')
      cmd_device(req.body)
      res.status(200).send('success')
    })
  }
  else if(req.body.msg=='stop'){
    stopLog(req.body,function(){
      console.log('callback here bro')
      cmd_device(req.body)
      res.status(200).send('success')
    })
  }
  else {
    res.status(401).send('Failed')
  }
})

app.route('/fftsignup')
.post((req,res)=>{
  if(req.body.subunit_name && req.body.project_id && req.body.mgr){
    mgrsignup(req.body,function(err,reply){
      if(err){
          res.status(401).json({error : 'Error has occured'})
      }
      else {
          res.status(200).json({success : 'Successfull'})
      }
    })
  }
  else {
    if(req.body.mgr==false){
      console.log('NO mgr')
      fftsignup(req.body,function(err,reply){
        if(err){
            res.status(401).json({error : 'Error has occured'})
        }
        else {
            res.status(200).json({success : 'Successfull'})
        }
      })
    }
    else {
      res.status(401).json({error : 'Invalid Details'})
    }
  }
})

app.route('/getLocation')
.post(checkUser,(req,res)=>{
  console.log('starting Location Service')
  var interval = setInterval(function () {
    getFromElastic(function(err,reply){
      if(err){
        console.log({err})
      }
      else {
        console.log('done')
      }
    })
    if(req.body.stop)
    {
      clearInterval(interval)
    }

  }, 5000);
  res.send('Request recieved').end()

})

app.route('/geoconfig')
.post(checkUser,(req,res)=>{
  console.log('wow',req.body)
  userValues(req.headers.auth,function(err,userInfo){
    if(err){
      res.status(401).json({ error : 'amor bhoro phott gayo' })
    }
    else {
      console.log({userInfo})
      geoConfig(req.body,userInfo.project_id,function(err,reply){
        if(err){
          res.status(401).json({ error : 'amor bhoro' })
        }
        else {
          res.status(200).json({ success : 'Configuration Updated' })
        }
      })

    }
  })
})
app.route('/fftlogin')
.post((req,res)=>{
  var token = req.body.token;
  var project_id = req.body.project_id;
  var reporting_manager = req.body.subunit_name;
  var query = {
    text : 'SELECT user_id,full_name,project_id,back_office_id from user_master where full_name=$1 AND role_id=$2',
    values : [reporting_manager,2]
  }
  db.query(query)
  .then(resp=>{
    if(resp.rows.length>0){
      var data = {
        token : token,
        project_id : resp.rows[0].project_id,
        user_id : resp.rows[0].user_id,
        full_name : resp.rows[0].full_name
      }
      client.set(token,JSON.stringify(data),function(err,reply){
        if(err){
          console.log('Something Wrong with Redis')
          res.status(401).send('Unexpected error')
        }
        else {
          console.log('Key Value Stored in Hashtable')
          res.status(200).send('succesfull')
        }
      })
    }
    else {
      console.log('Manager not found')
      res.status(401).send('Unexpected error')
    }
  })
  .catch(err=>{
    console.log('Something Went Wrong')
    res.status(401).send('Unexpected error')
  })
})


app.route('/checkToken')
.post((req,res)=>{
  // console.log(req.body.user_id)
  if(req.body.token){
    client.get(code,function(err,reply){
      console.log(reply)
      if(err){
        console.log({err})
        res.status(401).json({error : 'Something went wong'})
      }
      else if(reply=='null'){
          console.log({err})
          res.status(401).json({error : 'Invalid or Expired Code'})
        }
      else {
        console.log('Correct Token')
          res.status(200).json(JSON.parse(reply))
      }
    });
  }
  else {
    console.log('Incorrect Request')
    res.status(401).send('Unexpected error')
  }

})




app.route('/timeseries')
.post(checkUser,(req,res)=>{
  // console.log('hello-->')
  // console.log(req.body);
       queryElastic.getFromElastic(req.body.vin,1,function(err,reply){
         if(err){
           res.status(401).json({error : err})
         }
         else {
           res.status(200).json({data : reply})
         }
       })
})

app.post('/trip',(req,res)=>{
  // console.log(req.body);
  if(req.body.status=='start'){
       tripstart.setInRedis(req.body.vin)
       res.status(200).json({success : 'Success'})
   }
  if(req.body.status=='stop'){
       tripstop.getFromElastic(req.body.vin,function(err,reply){
         if(err){
           res.status(401).json({error : err})
         }
         else {
           res.status(200).json({data : reply})
         }
       })
  }
})

app.post('/tripdetails',(req,res)=>{
  // console.log(req.body);
  tripdetails.getdetails(req.body.vin,function(err,reply){
    if(err){
      res.status(401).json({error : err})
    }
    else {
      res.status(200).json({data : reply})
    }
  })
})

app.post('/nearestsc',(req,res)=>{
  // console.log(req.body);
  scdetails.getFromGoogle(req.body.lat,req.body.lon,function(err,reply){
    if(err){
      res.status(401).json({error : err})
    }
    else {
      res.status(200).json({data : reply})
    }
  })
})

app.post('/trippath',(req,res)=>{
  // console.log(req.body);
  trippath.getdetails(req.body.vin,req.body.start_time,req.body.stop_time,function(err,reply){
    if(err){
      res.status(401).json({error : err})
    }
    else {
      res.status(200).json({data : reply})
    }
  })
})

app.route('/downloadlog*')
.get((req,res)=>{
  // console.log(req.query.srno)
  fetchCanLog(req.query.srno,function(err, reply){
    if(err){
      res.status(401).json({ error : 'error' })
    }
    else {
      res.status(200).json({ data : reply })
    }
  })
})
.post((req, res) => {
    // console.log('enter post')
    // console.log(req.body)
    batch_file(req.body, function (err, fname) {
      if(err){
        res.status(401).json({ error : 'error' })
      } else {
        console.log('hurray')
        var file = __dirname + '/files/'+fname;
        // res.status(200).json({content : file, filename: fname})
        res.download(file,fname);
      }

    });
  })


  app.route('/alerts*')
  .get(checkUser,(req,res)=>{
    var token = req.headers.auth;
    getAlerts.fetchAlerts(token,function(err,reply){
      if(err){
        res.status(401).json({ error : 'error' })
      }
      else{
        try {
          res.status(200).json({ data : reply })
        } catch (e) {
          // console.log('headers')
        }
      }
    })
  })


app.route('/getmetadata')
.get(checkUser,(req,res)=>{
  // console.log('sdf?ghjklkjhgjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj')
  var token = req.headers.auth;
  getmetadata.getdetails(token,function(err,reply){
    if(err){
      re.status(401).json({ error : 'error'})
    }
    else {
      res.status(200).json({ success : reply})
    }
  })
})


app.route('/test*')
.get((req,res)=>{
  // console.log(req.headers)
  // console.log(req.body)
})
.post((req,res)=>{
  var file = __dirname + '/test';
  res.download(file);
})

app.post('/Bulkmetadata', function (req, res) {
  // console.log("file here ");
  // console.log(req);
  // console.log(req.files);
  if (req.files) {
    var file = req.files.files;
    var filename = file.name;
    file.mv("./filesBulkMetadata/" + filename, function (err) {
      if (err) {
        console.log("File Upload Failed", filename, err);
        res.send("Error Occured!")
      }
      else {
        bulkMetadata(filename,function(err,reply){
          if(err){
            res.status(401).json({ error : 'Shit Failed '})
          }
          else {
            res.status(200).json({ success : 'Successfully Uploaded'})
          }
        })
      }
  })
  }
  else {
    res.status(401).json({ error : 'Invalid Request'})
  }
})

app.post('/BulkDevice', function (req, res) {
  if (req.files) {
    var file = req.files.files,
    filename = file.name;
    file.mv("./filesBulkMetadata/" + filename, function (err) {
      if (err) {
        console.log("File Upload Failed", filename, err);
        res.send("Error Occured!")
      }
      else {
        userValues(req.headers.auth,function(err,reply){
          if(err){
            res.status(401).json({ error : 'Unauthorized access'})
          }
          else {
            bulkUpdate(filename,reply.username,function(err,reply){
              if(err){
                res.status(401).json({ error : 'Bulk Update'})
              }
              else {
                res.status(200).json({ success: reply })
              }
            })
          }
        })
      }
})
}
})
//---------------------------------------------------------------------------------------SingleMetadata


app.post('/SingleMetadata', function (req, res) {
  console.log(JSON.stringify(req.body));
  var json = req.body;
  singleMetadata(req.body,function(err, reply){
    if(err){
      res.status(401).json({ error : 'Error Occured'})
    }
    else {
      res.status(200).json({ success : 'Successfull'})
    }
  })
})
  //res.status(200).json('error do')

// ---------------------------------------------------------------------------------------BulkDevice

////---------------------------------------------------------------------------------------SingleDevice

app.route('/SingleDevice')
.post(checkUser,(req, res)=>{
  userValues(req.headers.auth,function(err,reply){
    if(err){
      res.status(401).json({ error : 'Unauthorized access'})
    }
    else {
      singleDevice(req.body,reply.username,function(err,reply){
        if(err){
          res.status(401).json({error : err})
        }
        else {
          res.status(200).json({ success : reply})
        }
      })
    }
  })
})

var subunitInfo = require('./UserManagement/subunitInfo.js')
app.route('/subunitinfo*')
.get((req,res)=>{
  console.log(req.query.email)
  subunitInfo(req.query.email,function(err,reply){
    if(err){
      res.status(401).json({error : 'No list available for this comapany email'})
    }
    else {
      res.status(200).json(reply)
    }
  })
})


// var uploader = multer({ dest: './files'});

// File input field name is simply 'file'
var multer  = require('multer');
var uploader = multer({ dest: 'uploads' }); //setting the default folder for multer

//other imports and code will go here

app.post('/upload',uploader.single('fileData'), (req, res,next) => {
  console.log(req.file);//this will be automatically set by multer
  console.log(req.body);
  //below code will read the data from the upload folder. Multer     will automatically upload the file in that folder with an  autogenerated name
  fs.readFile(req.file.path,(err, contents)=> {
   if (err) {
   console.log('Error: ', err);
  }else{
   console.log('File contents ',contents);
  }
 });
});


app.post('*',(checkUser,req,res)=>{
  // console.log('req.body')
  res.redirect('/login');
})
app.get('*',(checkUser,req,res)=>{
  // console.log('req.body')
  res.redirect('/login');
})




app.listen(3002,function(){
  console.log('app started')
  console.log('Starting Pipeline Services....')
  getDevices();
  getVin();
  console.log('Pipelines are up.')
  getAlerts.alertjson();
  console.log('Setting Up GEO FENCE FORCE FIELD BOOM!!!!!!!!!')
  setInterval(function () {
     geoF()
  }, 60000);
  var interval = setInterval(function () {
    getFromElastic(function(err,reply){
      if(err){
        console.log({err})
      }
      else {
        console.log('done')
      }
    })
    if(false)
    {
      clearInterval(interval)
    }

  }, 1000*5);
})
