var redis = require('redis')
var client = redis.createClient();
var db = require('../sequelize.js')
var gen = require('../tokenGen/gen.js')
var userLogs = require('./logger.js')
var fftlogin = require('./fftlogin.js')

var login = (req,res,next)=>{
  // console.log(req.body)
  var user = req.body.username
  var pass = req.body.password
  var query = {
    text : 'SELECT * from users INNER JOIN subunit_info ON users.subunit_name= subunit_info.subunit_name where (users.username = $1 AND users.password = $2)',
    values : [user,pass]
  }
  db.query(query)
  .then(result=>{
    // console.log(result.rowCount)
    if(result.rowCount==0)
    {
      throw Error('Invalid Credentials');
    }
    else {
      var userInfo = {
        username : result.rows[0].username,
        password : result.rows[0].password,
        project_id : result.rows[0].project_id,
        subunit_name : result.rows[0].subunit_name,
        role : result.rows[0].role,
        id : result.rows[0].user_id,
        city : result.rows[0].city,
        state : result.rows[0].state,
        company_mail : result.rows[0].company_mail,
        login_time : Date.now(),
        valid_time : Date.now() + 1000*60*30
      }
      // console.log('HARDIK HERE')
      // console.log(userInfo)
      gen.createTok(user,function(data){
        if(userInfo.project_id==3){
          var defaultQuery = {
            text : 'select srno,lat,lon from devices where  project_id = $1 and public_user = $2 order by time desc limit 1',
            values : [userInfo.project_id,userInfo.id]
          }
        }
        else if(userInfo.project_id==9){
          var defaultQuery = {
            text : 'select srno,lat,lon from devices where status = $1 order by time desc limit 1',
            values : ['Connected']
          }
        }
        else {
          var defaultQuery = {
            text : 'select srno,lat,lon from devices where project_id = $1 and status = $2 order by time desc limit 1',
            values : [userInfo.project_id,'Connected']
          }
        }
        db.query(defaultQuery)
        .then(srno=>{
          var empty = '';
          if(srno.rows.length>0){

            var login = {
              token : data,
              srno : srno.rows[0].srno || empty,
              lat : srno.rows[0].lat || empty,
              lon : srno.rows[0].lon || empty,
              role : userInfo.role || empty,
              subunit_name : userInfo.subunit_name || empty,
              city : userInfo.city || empty,
              state : userInfo.state || empty,
              company_mail : userInfo.company_mail || empty
            }

          }
          else {
            var login = {
              token : data,
              srno :  empty,
              lat :  empty,
              lon :  empty,
              city : userInfo.city || empty,
              state : userInfo.state || empty,
              company_mail : userInfo.company_mail || empty,
              role : userInfo.role,
              subunit_name : userInfo.subunit_name
            }

          }
          client.set(data,JSON.stringify(userInfo),function(err,resp){
            if(err){
              res.status(401).json({Error :'DB issue'})
            }
            else {
              // console.log(userInfo)
              userLogs(userInfo)
              fftlogin(userInfo,login.token)

              // client.expire(data,60*30)
              res.status(200).json(login)
            }
          })
        })
        .catch(err=>{
          console.log(err)
          res.status(401).json({Error :'DB issue'})
        })
      })
    }
  })
  .catch(err=>{
    console.log(err)
    res.status(401).json({Error : err.message})
  })
}

module.exports = login;
