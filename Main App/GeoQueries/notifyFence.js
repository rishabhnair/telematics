var mqtt = require('mqtt')

var notifyFence = (srnoList)=>{
  var client  = mqtt.connect('tcp://149.28.63.75:1883');
    var topic = 'alerts/'+srnoList;
    var msg = `Vehicle with Serial No: ${srnoList} has left the assigned GeoFence`
    client.publish(topic,msg,function(){
      client.end()
    })
}

module.exports = notifyFence;
