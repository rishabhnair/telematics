
var express = require('express');
var bodyParser=require('body-parser');
var fs = require('fs');
var path = require('path');
var multer  = require('multer');
var userValues = require('./userValues.js')
var upload = multer({ dest: 'uploads/' }); //setting the default folder for multer
var app=express();
var fs = require('fs')
app.use(bodyParser.urlencoded({extended:false}))
app.use(bodyParser.json());
//other imports and code will go here

const handleError = (err, res) => {
  res
    .status(500)
    .contentType("text/plain")
    .end("Oops! Something went wrong!");
};

app.post('/upload',upload.single('fileData'),(req, res) => {
  userValues(req.headers.auth,function(err,reply){
    if(err){
      res.status(402).send('error')
    }
    else {
      console.log(req.file)
      const tempPath = req.file.path;
      const targetPath = path.join(__dirname, `./uploads/${reply.id}.png`);
      const vehiclePath = path.join(__dirname, `./uploads/v${reply.id}.png`);
      console.log(req.file.originalname)

      if ((req.file.originalname) == 'owner') {
        console.log('true')
        fs.rename(tempPath, targetPath, err => {
          console.log('double true')
          if (err) return handleError(err, res);

          res
          .status(200)
          .contentType("text/plain")
          .end("File uploaded!");
        });
      }
      else if ((req.file.originalname) == 'vehicle') {
        console.log('true')
        fs.rename(tempPath, vehiclePath, err => {
          console.log('double true')
          if (err) return handleError(err, res);

          res
          .status(200)
          .contentType("text/plain")
          .end("File uploaded!");
        });

      } else {
        fs.unlink(tempPath, err => {
          if (err) return handleError(err, res);

          res
          .status(403)
          .contentType("text/plain")
          .end("Only .png files are allowed!");
        });
      }

    }
  })
  }
);

app.get('/upload', function(req, res) {
  userValues(req.headers.auth,function(err,reply){
    if(err){
      res.status(402).send('error')
    }
    else {
      fs.readFile(`./uploads/${reply.user_id}.png`, function(err, data) {
        if (err) throw err; // Fail if the file can't be read.
        else {
          console.log(data)
          res.writeHead(200, {'Content-Type': 'image/jpeg'});
          res.end(data); // Send the file data to the browser.
        }
      });
    }
  })
});
app.listen('3003',function(){
  console.log('wrfrw')
})
