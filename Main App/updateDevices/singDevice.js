var db = require('../sequelize.js')
var dateTime = require('node-datetime')
var updatedevices = require('./UpdateDevices.js')
var singleDevice = (json,Username,callback)=>{
  var query = {
    text: `select "ECU", "Available_Version" from "sftpMetadata" where("ECU"= $1 AND "Available_Version"= $2)`,
    values: [json.ECU, json.UpdateToVersion]
  }
  db.query(query)
    .then(result => {
      console.log("json", json);
      console.log("result", result);
      console.log("result rows", result.rows)
      if (result.rows.length > 0) {

        console.log('Values are correct, Updating device');
        var dt = dateTime.create();
        var formatted = dt.format('Y-m-d H:M:S');
        var query1 = {
          text: `insert into "Report" ("DateTime","Username","SrNo", "ECU","UpdateToVersion","Status") values ($1, $2, $3, $4, $5, $6)`,
          values: [formatted, Username, json.srno, json.ECU, json.UpdateToVersion, "Updating"]
        }
        db.query(query1)
          .then(result => {
            var row =[]
            row.push(json)
            updatedevices.sendsftpPath(row, formatted, Username);
            callback(false,'Updating Devices')
          })
          .catch(function (err) {
            // console.log('error in query')
            console.log(err);
            callback('Error in Querying',false)
          })
      }
      else {
        console.log('invalid values');
        callback('Invalid Values',false)
      }
    })
    .catch(function (err) {
      // console.log('error in query')
      console.log(err);
      callback('Invalid Values',false)
    })
}

module.exports = singleDevice;
