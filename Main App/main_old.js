var express = require('express');
var bodyParser=require('body-parser');
var login = require('./UserManagement/login.js')
var signup = require('./UserManagement/signup.js')
var auth = require('./UserManagement/auth.js')
var checkUser = require('./UserManagement/checkUser.js')
var updatePassword = require('./UserManagement/updatePassword.js')
var fetchDeviceList = require('./Devices/deviceInfo.js')
var bulkRegisterDevices = require('./Devices/bulkRegisterDevices.js')
var registerDevice = require('./Devices/registerDevice.js')
var addDevice = require('./Devices/addDevice.js')
var deleteDevice = require('./Devices/deleteDevice.js')
var fetchUserProfile = require('./UserManagement/fetchUserProfile.js')
var reqres = require('./ChatReqRes/reqres.js')
var startLog = require('./PlatformService/startLog.js')
var stopLog = require('./PlatformService/stopLog.js')
var cmd_device = require('./PlatformService/cmd_device.js')
var getDevices = require('./PlatformService/getDevice.js')
var getVin = require('./PlatformService/getVin.js')
const getCSV = require('get-csv');
const elastic = require('./GeoQueries/elasticize.js');
var queryElastic = require('./GeoQueries/queryElastic.js');


var app=express();
var fs = require('fs')
app.use(bodyParser.urlencoded({extended:false}))
app.use(bodyParser.json());
var cookieParser = require('cookie-parser');
app.use(cookieParser());
var upload = require('express-fileupload');
app.use(upload());

app.all("*", (req, res, next)=>{
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With, auth');
  res.header('Access-Control-Allow-Credentials', true);
  console.log(req.headers);
  console.log(req.body);
  console.log('----gateway----');
  if(req.method === 'OPTIONS') {
        res.sendStatus(200);
     } else {
       next();
     }
});

app.route('/login')
.post(login,(req,res)=>{
  res.status(200).send('logged in')
})

app.route('/signup')
.post(signup,(req,res)=>{
  res.status(200).send('signed up')
})
.get(auth,(req,res)=>{

})

app.route('/auth')
.post(auth,(req,res)=>{

})

app.route('/addDevice')
.post((req,res)=>{
  console.log('entered')
  var info = {
    // token : req.headers.auth,
    srno : req.body.srno,
    imei : req.body.imei || req.body.srno
  }
  addDevice(info,function(err, reply){
    if(err){
      console.log(err)
      res.status(401).json({ error : err })
    }
    else {
      console.log(reply)
      res.status(200).json({success : reply })
    }
  })
})

app.route('/registerDevice')
.post(checkUser,(req,res)=>{
  console.log(req.files)
  if(req.files){
    var files = req.files;
    console.log(files)
    for(key in files){
      var file = files[key]
      bulkRegisterDevices(file,req.headers.auth,function(err, reply){
        if(err){
          res.status(400).json(err)
        }
        else {
          res.status(200).json(reply)
        }
      })
      break;
    }
  }
  else {
    var info = {
      srno : req.body.srno,
      imei : req.body.imei || req.body.srno,
      token : req.headers.auth
    }
    registerDevice(info)
    .then(result=>{
      res.status(200).json({success : 'Device Registered'})
    })
    .catch(err=>{
      console.log({err})
      res.status(401).json(err)
    })
  }
})

app.route('/deviceinfo*')
.get(checkUser,(req,res)=>{
  var empty = ''
  var info = {
    token : req.headers.auth,
    device : req.query.device || empty,
  }
  console.log(info)
  fetchDeviceList(info,function(err, reply){
    if(err){
      res.status(401).json({error: err.message})
    }
    else {
      res.status(200).json({devices : reply})
    }
  })
})

app.route('/updatePass*')
.get((req,res)=>{
  console.log(req.query)
  var data = {
    auth : true,
    user : req.query.username
  }
  updatePassword(data,function(err,reply){
    if(err){
      res.status(500).json({error: err.message})
    }
    else {
      res.status(200).json({success: 'Acivation mail has been sent'})
    }
  })
})
.post((req,res)=>{
  var data = {
    auth : false,
    code : req.body.code,
    pass : req.body.password
  }

  updatePassword(data,function(err,reply){
    if(err){
      res.status(500).json({error: err.message})
    }
    else {
      res.status(200).json({success: 'Acivation mail has been sent'})
    }
  })
})

app.route('/deleteDevice')
.post(checkUser,(req,res)=>{
  var info = {
    token : req.headers.auth,
    srno : req.body.srno
  }
  console.log(info)
  deleteDevice(info,function(err, reply){
    if(err){
      res.status(401).json({error : err})
    }
    else {
      res.status(200).json({ success : reply})
    }
  })
})

app.route('/reqresChat')
.post((req,res)=>{
  reqres(req.body,function(err, reply){
    if(err){
      res.status(401).json({error : err})
    }
    else {
      res.status(200).json(reply)
    }
  })
})

app.route('userProfile')
.post(checkUser,(req,res)=>{
  fetchUserProfile(req.body,function(err, reply){
    if(err){
      res.status(401).json({error : err})
    }
    else {
      res.status(200).json(reply)
    }
  })
})

app.route('/command')
.get((req,res)=>{

})
.post((req,res)=>{
  console.log(req.body)
  if(req.body.msg=='start'){
    start_log(req.body,function(){
      console.log('callbacks here')
      cmd_device(req.body)
    })
  }
  if(req.body.msg=='stop'){
    stop_log(req.body,function(){
      console.log('callback here bro')
      cmd_device(req.body)
    })
  }
  if(req.body.vin_no){
    console.log("vehicle number")
    //store_vehicle(req.body)
  }
})

app.route('/timeseries')
.post(checkUser,(req,res)=>{
  console.log('hello-->')
  console.log(req.body);
       queryElastic.getFromElastic(req.body.vin,1,function(err,reply){
         if(err){
           res.status(401).json({error : err})
         }
         else {
           res.status(200).json({data : reply})
         }
       })
})

app.route('/test*')
.get((req,res)=>{
  console.log(req.headers)
  console.log(req.body)
})
.post((req,res)=>{
  console.log(req.headers)
  console.log(req.body)
})

app.post('/Bulkmetadata', function (req, res) {
  console.log("file here ");
  console.log(req.files);
  if (req.files) {
    console.log("req.files.file1", req.files.files.name)
    var file = req.files.files,
      filename = file.name;
    console.log("{{{{{{{{{{{{{{{{{{{{{{{{{{", req.files);
    console.log("####" + filename);
    file.mv("./" + filename, function (err) {
      if (err) {
        console.log("File Upload Failed", filename, err);
        res.send("Error Occured!")
      }
      else {
        getCSV(`./${filename}`, function (err, data) {
          if (err) {
            console.log('invalid csv')
          }
        })
          .catch(function (err) {
            console.log('enter a valid csv')
          }
          )
          .then(row => {
            if (row) {
              console.log(row);
              console.log(typeof row);
              addRecords(row);
              res.send('uploaded successfully');
              //db.end();
              console.log("file uploaded success");

            }
          })
        var addRecords = function (row, res) {
          console.log(row);
          console.log(row[0]);
          for (let i = 0; i < row.length; i++) {
            //element = JSON.stringify(row[i]).split(',');
            var query = {
              text: 'insert into "sftpMetadata" ("ECU","Available_Version","File_Path") values ($1, $2, $3)on CONFLICT on CONSTRAINT pkey DO UPDATE  set "File_Path" = $3',
              //text : 'delete from "sftpMetadata" ',
              //text: 'insert into "Report" ("DateTime", "Username", "SrNo", "ECU", "UpdateToVersion", "Status") VALUES ("2018-09-26 15:40:38", "admin","TELINK098765432", "TCU", "Version6", "Updating")'
              //values: []
              values: [row[i].ECU, row[i].Available_Version, row[i].File_Path],
            }
            db.query(query)
              .then(result => {
                console.log(result);
                console.log("Baby ,", i);
                if (i == row.length - 2) {
                  //res.send('file uploaded successfully');
                }
              })
              .catch(function (err) {
                console.log('error in query')
                console.log(err);
              })
          }
          console.log("file uploaded success");

          //db.end();
        }
      }
    }
    )
  }
})

//---------------------------------------------------------------------------------------SingleMetadata


app.post('/SingleMetadata', function (req, res) {
  console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")
  console.log(req);
  console.log("req body ------------------")
  console.log(JSON.stringify(req.body));
  //console.log("*********", JSON.parse(req.body.file1));
  //res.setHeader('Content-Type', 'application/json');
  // var json = {
  //   "ECU": "DCU",
  //   "Available_Version": "Version4",
  //   "File_Path": "sftp://108.61.89.215"
  // }
  var json = req.body;
  var query = {
    text: 'insert into "sftpMetadata" ("ECU","Available_Version","File_Path") values ($1, $2, $3)on CONFLICT on CONSTRAINT pkey DO UPDATE  set "File_Path" = $3',
    values: [json.ECU, json.Available_Version, json.File_Path]
  }
  db.query(query)
    .then(result => {
      console.log(result);
      console.log("data inserted Successfully");
      res.send("uploaded successfully");
      //db.end()
    }
    )
    .catch(function (err) {
      console.log('error in query');
      res.send("error in query")
      console.log(err)
    })
  //res.status(200).json('error do')
})

//---------------------------------------------------------------------------------------BulkDevice

app.post('/BulkDevice', function (req, res) {
  console.log("file here ");
  // console.log(req.files);
  const object1 = req.files;
  console.log("KEYS Now", Object.keys(object1));
  let keyhere = Object.keys(object1);
  // console.log("first key", keyhere[0]);
  var zz = keyhere[0];
  let Username = zz;
  console.log("Username ,", Username);
  if (req.files) {
    // console.log("zz", zz);
    console.log("first", req.files[zz]);
    //console.log("req.files.file1", req.files.file1)
    var file = req.files[zz],
      filename = file.name;
    file.mv("./pub/" + filename, function (err) {
      if (err) {
        console.log("File Upload Failed", filename, err);
        res.send("Error Occured!")``
      }
      else {
        getCSV(`./pub/${filename}`, function (err, data) {
          if (err) {
            console.log('invalid csv')
          }
        })
          .catch(function (err) {
            console.log('enter a valid csv')
          }
          )
          .then(row => {
            if (row) {
              checkrows.checkrows(row, function (wrongRows) {
                if (wrongRows.length != 0) {
                  console.log(`error in following rows ${wrongRows}`);
                  res.send(`error in following rows ${wrongRows}`);
                }
                else {
                  console.log(`No error in file, pushing sftp paths to devices`);
                  var dt = dateTime.create();
                  var formatted = dt.format('Y-m-d H:M:S');
                  // func(row,formatted,function(err, reply){
                  //   if(err){
                  //
                  //   }
                  //   else {
                  //     // updatedevices.sendsftpPath(row, formatted, Username);
                  //     res.send("updating devices");
                  //
                  //   }
                  // })

                  func1(row,formatted)
                  .then(res1=>{
                    updatedevices.sendsftpPath(row, formatted, Username);

                    res.send("updating devices");
                  })
                  .catch(err=>{
                    console.log({err})
                  })

                }
              });
            }
          })
        //-------------------
      }
    }
    )
  }
  var func1 = async(row,formatted)=>{
    var result = []
        for(var i= 0;i< row.length; i++){
          var query1 = {
            text: `insert into "Report" ("DateTime","Username","SrNo", "ECU","UpdateToVersion","Status") values ($1, $2, $3, $4, $5, $6)`,
            values: [formatted, Username, row[i].SrNo, row[i].ECU, row[i].UpdateToVersion, "Updating"]
          }
          result[i] = await db.query(query1)
          .catch(err=>{
            throw err
          })
          console.log({result})
          if(i==row.length){
            return values
          }
        }
    }

  var func = async (data,format,callback)=>{
    var row = data
    var dog;
    var formatted = format
    console.log('ROws Length',row.length)
    for(var i= 0;i< row.length; i++){
      var query1 = {
        text: `insert into "Report" ("DateTime","Username","SrNo", "ECU","UpdateToVersion","Status") values ($1, $2, $3, $4, $5, $6)`,
        values: [formatted, Username, row[i].SrNo, row[i].ECU, row[i].UpdateToVersion, "Updating"]
      }
      db.query(query1)
        .then(result => {
          if(i==row.length){
            console.log({i},'Callback Being Called')
            dog="set"
            callback(false, result)
          }
        })
        .catch(function (err) {
          console.log('error in query')
          console.log(err);
          callback(err,false)
        })
    }
    var biscuit = await dog
    console.log("-----------------------------",biscuit)
  }
})

////---------------------------------------------------------------------------------------SingleDevice
var singleDevice = require('./updateDevices/singDevice.js')

app.route('/SingleDevice')
.post(checkUser,(req, res)=>{
  singleDevice(req.body,function(err,reply){
    if(err){
      res.status(200).json({ success : reply})
    }
    else {
      res.status(401).json({error : err})
    }
  })
})

app.post('*',(checkUser,req,res)=>{
  console.log('req.body')
  res.redirect('/login');
})
app.get('*',(checkUser,req,res)=>{
  console.log('req.body')
  res.redirect('/login');
})


app.listen(3002,function(){
  console.log('app started')
  console.log('Starting Pipeline Services....')
  getDevices();
  getVin();
  console.log('Pipelines are up.')
})
