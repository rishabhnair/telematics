var db = require('../sequelize.js')
var nodemailer = require('./nodemailer.js')
var redis = require('redis')
var client = redis.createClient();
var gen = require('../tokenGen/gen.js')
var mailer = require('./nodemailer.js')

var signup = (req,res,next) =>{
  console.log(req.body)
  var name = req.body.name || req.body.username
  var user = req.body.username
  // var pass = Buffer.from(req.body.password).toString('base64')
  // var subunit = req.body.subunit
  var email = ((req.body.username).split('@'))[1]
  var checkQuery = {
    text : 'select * from users where username = $1',
    values : [req.body.username]
  }
  db.query(checkQuery)
  .then(ok=>{
    if(ok.rows.length>0){
      res.status(401).json({error: "user already  bhai"})

    }
    else {
      var query = {
        text : 'SELECT * FROM subunit_db INNER JOIN subunit_info on subunit_db.subunit_name = subunit_info.subunit_name where subunit_db.company_mail = $1',
        values : [email]
      }
      db.query(query)
      .then(result=>{
        if(result.rowCount==0){
          var subunit = 'public';
          var project_id = 3;
          var userInfo = {
            name : name,
            user : user,
            subunit : subunit,
            project_id : project_id
          }
          var rand = parseInt(Math.random()*10000)
          client.set(rand,JSON.stringify(userInfo))
          mailer(user,rand,function(err,reply){
            if(err){
              console.log({err})
              res.status(200).json({error: 'Email couldn\'t be sent'})
            }
            else {
              console.log(reply)
              res.status(200).json({success: 'acivation mail has been sent', public : true})
            }
          })
        }
        else {
          var subunit = result.rows[0].subunit_name;
          var project_id = result.rows[0].project_id;
          var userInfo = {
            name : name,
            user : user,
            subunit : subunit,
            project_id : project_id
          }
          var rand = parseInt(Math.random()*100000)
          client.set(rand,JSON.stringify(userInfo))
          mailer(user,rand,function(err,reply){
            console.log({err})
            if(err){
              res.status(200).json({error: 'Email couldn\'t be sent'})
            }
            else {
              console.log(reply)
              res.status(200).json({success: 'Acivation mail has been sent', public : false})
            }
          })
        }
      })
      .catch(err=>{
        console.log({err})
        res.status(500).json({error: err.message})
      })

    }

  })
  .catch(err=>{
    console.log({err})
    res.status(401).json({error: err})
  })

}

module.exports = signup;
