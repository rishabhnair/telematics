
var db = require('./sequelize.js');

var checkrows = function(row,callback){
  console.log('inside checkrows function');
  var invalidRows=[];
  for (var i=0; i<row.length; i++){

    let k = i;
    var query={
      text : 'select "ECU", "Available_Version" from "sftpMetadata" where ("ECU" = $1)',
      values : [row[k].ECU]
    }
    db.query(query)
    .then( result => {
      var flag= 0;
      for(var j= 0; j<result.rows.length; j++){
          if(row[k].UpdateToVersion==result.rows[j].Available_Version){
          flag=1;
          console.log(`Row ${k} in ecuConfig.csv is correct`)
          // console.log(row[k])
          }
        }
        if(flag==0){
          console.log(`Row ${k} in ecuConfig.csv is incorrect`)
          invalidRows.push(k);
          console.log(`invalidRows[] right now: ${invalidRows}`)
        }
      }
    )
    .then(res =>{
      if(k==row.length-1)
      {
        callback(invalidRows)
      }
    })
    .catch(function(err){
      console.log('error in query')
      console.log(err);
    })

  }
  //db.end();
}

module.exports = {
  checkrows
}
