import React, {PureComponent } from 'react';
import {Col, Row, Button, Modal, ModalBody, Form, FormGroup} from 'reactstrap'
import Dropzone from 'react-dropzone'
import DeviceStatus from './Home/DeviceStatus'
import DeviceListStatus from './Home/deviceliststatus'
import Notification from './Home/notification'
import logo from '../images/ThinkEmbedded_logo-1.png';
import axios from 'axios';
import { Line } from 'rc-progress';
import *as constant from './constant'
import '../Style/header.css'
import '../Style/homemodel.css'
import '../Style/animationandtrasition.css'
var base64 = require('base-64');
var utf8 = require('utf8');

class Header extends PureComponent{
    constructor(props) {
        super(props);
        this.state = {
            modal: false,
            nestedModalUpdateDevice: false,
            nestedModalUpdateMetadata: false,
            nestedModalDeviceStatus: false,
            closeAll: false,
            addDevice:false,
            listDevice:false,
            notificationState:false,

            UpdateDeviceColor:'#ff9800',
            UpdateMetadataColor:'#ff9800',
            DeviceStatusColor:'#ff9800',
            AddDeviceColor:'#ff9800',
            DeviceListColor:'#ff9800',
            
            specificupdate:true,
            vin:'',
            ecu:'',
            version:'',

            ecumetadata:'',
            versionmetadata:'',
            filepath:'',
            servermetadata:'',
            specificupdatemetadata:true,

            specificaddition:true,
            imei:'',
            serialno:'',

            //bulk update /registration
            files: [],
            percent: 0,
            filename:'',
            filestatus:'',
            removetext:false,

            //setting error handling after submit
            successfully:'', 
            alreadyregistered:'',
            invaliddetails:'',
            allfields:'',

            //total device notification
            notificationlist:'',
            currentnotification: 0,

            ecuList:[],
            versionList:[],

            //emails: sessionStorage.getItem('kuchhbhi'),
            emails: base64.decode( utf8.decode( sessionStorage.getItem('kuchhbhi') ) ),
            cityList:'',
            stateList:'',
            subunitList:''
        };   
        this.handleChange = this.handleChange.bind(this);
        this.handleEcuList = this.handleEcuList.bind(this);

        this.toggle = this.toggle.bind(this);
        this.toggleNestedUpdateDevice = this.toggleNestedUpdateDevice.bind(this);
        this.toggleNestedUpdateMetadata = this.toggleNestedUpdateMetadata.bind(this);
        this.toggleNestedModalDeviceStatus = this.toggleNestedModalDeviceStatus.bind(this);
        this.toggleAddDevice = this.toggleAddDevice.bind(this);
        this.toggleDeviceList = this.toggleDeviceList.bind(this);
        this.toggleNotification = this.toggleNotification.bind(this);
        this.toggleNotification1 = this.toggleNotification1.bind(this);
        this.toggleAll = this.toggleAll.bind(this);

        this.handleSubmitSpecificUpdate = this.handleSubmitSpecificUpdate.bind(this);
        this.handleSubmitSpecificUpdatemetadata = this.handleSubmitSpecificUpdatemetadata.bind(this);
        this.handleSubmitAddDevice = this.handleSubmitAddDevice.bind(this);

        this.onDropAddger = this.onDropAddger.bind(this);
        this.onDropDevice = this.onDropDevice.bind(this);
        this.onDropMetadata = this.onDropMetadata.bind(this);
    }


    axiosFuncEcu = () => {
        axios.defaults.headers.common['auth'] = sessionStorage.getItem('DontTouchMe');
        axios.get('http://8.9.36.13/factory/getmetadata')
            .then(res => {
                if(res.status === 200){
                    let ecuList = [];
                    for(let i =0; i<res.data.success.length; i++){
                        if(!ecuList.includes(res.data.success[i].ECU))
                            ecuList.push(res.data.success[i].ECU);
                    }

                    this.setState({ecuList: ecuList, versionList: res.data.success});
                }
            }).catch( er => {
                if(er.response){
                    if(er.response.status === 401)
                        console.log("Unauthorized or Invalid credential")
                    if(er.response.status === 404){
                        console.log("Not Found")
                    }
                }
                else{
                    console.log("Error in response");
                }
            });
    };

    axiosFunc = () => {
        axios.defaults.headers.common['auth'] = sessionStorage.getItem('DontTouchMe');
        axios.get('http://8.9.36.13/factory/alerts')
            .then(res => {
                if(res.status === 200){
                    let deviceReports = [];
                    for(let i=0; i<res.data.data.length; i++){
                            deviceReports.push({srno:res.data.data[i].srno ? res.data.data[i].srno : 'NA', alertCode:res.data.data[i].alertCode ? res.data.data[i].alertCode : 'NA', time:res.data.data[i].time ? res.data.data[i].time : 'NA'})
                    }
                    this.setState({notificationlist: deviceReports, currentnotification: (res.data.data.length - Number(sessionStorage.getItem('noti')))  });
                }
            }).catch( er => {
                if(er.response){
                    if(er.response.status === 401)
                        console.log("Unauthorized or Invalid credential")
                    if(er.response.status === 404){
                        console.log("Not Found")
                    }
                }
                else{
                    console.log("Error in response");
                }
            });
    };

    componentDidMount() {
        
            if(this.state.emails){
                axios.defaults.headers.common['auth'] = sessionStorage.getItem('DontTouchMe');
                axios.get('http://8.9.36.13/factory/subunitinfo?email='+this.state.emails)
                    .then(res => {
                        if(res.status === 200){
                            //console.log('res in header', res);
                            let cityList = [];
                            let stateList = [];
                            let subunitList = [];

                            for(let i=0; i<res.data.length; i++){
                                cityList.push({city:res.data[i].city ? res.data[i].city : 'NA', pid:res.data[i].project_id ? res.data[i].project_id  : 'NA'})
                                stateList.push( {state:res.data[i].state ? res.data[i].state : 'NA', pid:res.data[i].project_id ? res.data[i].project_id  : 'NA'} )
                                subunitList.push({subunit:res.data[i].subunit_name ? res.data[i].subunit_name : 'NA', pid:res.data[i].project_id ? res.data[i].project_id  : 'NA'})
                            }
                            this.setState({cityList: cityList, stateList: stateList, subunitList: subunitList})
                        }
                    }).catch( er => {
                        if(er.response){
                            if(er.response.status === 401)
                                console.log("Unauthorized or Invalid credential")
                            if(er.response.status === 404){
                                console.log("Not Found")
                            }
                        }
                        else{
                            console.log("Error in response");
                        }
                    });
                }
        
        this.axiosFuncEcu();
        this.axiosFunc();
        this.interval = setInterval(this.axiosFunc, 5000);
    }


    componentWillReceiveProps(nextProps){
        if(nextProps !== this.props){
            this.props = nextProps;
        }
    }

    handleChange(e, check) {
        const target = e.target;
        const name  = target.name;
        const value = target.value;

        if(check === 'state' || check==='city' || check==='subcity'){
            const data = {value, check }
            this.props.onselectStateCitySubcity(data);
        }
         
        if(this.state.vin.length !== 0){
            this.setState({specificupdate : false})
        }
        else{
            this.setState({specificupdate : true})
        }

        if(this.state.ecumetadata.length !== 0 && this.state.versionmetadata.length !== 0 && this.state.filepath.length !== 0){
            this.setState({specificupdatemetadata : false})
        }
        else{
            this.setState({specificupdatemetadata : true})
        }

        if(this.state.serialno.length !==0){
            this.setState({specificaddition : false})
        }
        else{
            this.setState({specificaddition: true})
        }

        this.setState({
            [name]: value
        });
    }

    handleEcuList(e, selectedValue){
        if(selectedValue === 'ecuUpdateDevice')
            this.setState({ecu: e.target.value})
        
        if(selectedValue === 'version')
            this.setState({version: e.target.value})


        if(selectedValue === 'ecumetadata')
            this.setState({ecumetadata: e.target.value})

    }

    toggle() {
        this.setState({
          modal: !this.state.modal
        });
    }

    toggleNestedUpdateDevice() {
    this.setState({
        nestedModalUpdateDevice: !this.state.nestedModalUpdateDevice,
        UpdateDeviceColor : this.state.UpdateDeviceColor === 'white' ? '#ff9800' : 'white',
        closeAll: false,
        successfully:'',
        invaliddetails:'',
        allfields:'',
        percent:0,
        filename:'',
        filestatus:'',
        removetext: false,
        ecu:'',
        version:''
    });
    }

    toggleNestedUpdateMetadata() {
    this.setState({
        nestedModalUpdateMetadata: !this.state.nestedModalUpdateMetadata,
        UpdateMetadataColor : this.state.UpdateMetadataColor === 'white' ? '#ff9800' : 'white',
        closeAll: false,
        successfully:'',
        invaliddetails:'',
        allfields:'',
        percent:0,
        filename:'',
        filestatus:'',
        removetext: false,
        ecumetadata:''
    });
    }

    toggleNestedModalDeviceStatus(){
    this.setState({
        nestedModalDeviceStatus: !this.state.nestedModalDeviceStatus,
        DeviceStatusColor : this.state.DeviceStatusColor === 'white' ? '#ff9800' : 'white',
        closeAll: false
        });
    }

    toggleAddDevice(){
        this.setState({
            addDevice: !this.state.addDevice,
            AddDeviceColor : this.state.AddDeviceColor === 'white' ? '#ff9800' : 'white',
            closeAll: false,
            successfully:'',
            alreadyregistered:'',
            invaliddetails:'',
            allfields:'',
            percent:0,
            filename:'',
            filestatus:'',
            removetext: false
        });
    }

    toggleDeviceList(){
        this.setState({
            listDevice: !this.state.listDevice,
            DeviceListColor : this.state.DeviceListColor  === 'white' ? '#ff9800' : 'white',
            closeAll: false
        });
    }

    toggleNotification1(){
        this.setState({
            notificationState: !this.state.notificationState,
        })
    }

    toggleNotification(){
        this.setState({
            notificationState: !this.state.notificationState,
        })
        if(this.state.currentnotification !== 0)
            sessionStorage.setItem('noti', this.state.currentnotification);
    }

    toggleAll() {
        this.setState({
            nestedModalUpdateDevice: !this.state.nestedModalUpdateDevice,
            closeAll: true
        });
    }


    onDropAddger(files) {
        if(files){
            this.setState({removetext:true})
        }
        this.setState({ percent: 0, filename:files[0].name, filestatus:''});
        let data = new FormData();

        data.append('files', files[0]);
        //const url = '';
        const url = 'http://8.9.36.13/factory/registerDevice';

        const config = {
          headers: { 'content-type': 'multipart/form-data'},
          onUploadProgress: progressEvent => {
            var percent = Math.round(progressEvent.loaded * 100 / progressEvent.total);
            if (percent >= 100) {
              this.setState({ percent: 98 });
            } else {
              this.setState({ percent });
            }
          }
        };
    
        const that = this;

        axios.defaults.headers.common['auth'] = sessionStorage.getItem('DontTouchMe');
        axios.post(url, data, config)
          .then(function(response) {
              //console.log(response.data);
              if(response.status === 200){
                    that.setState({filestatus:'Successfully registered !', percent: 100})
              }
          })
          .catch(function(error) {
                that.setState({ percent: 0, filestatus:'Error in update, try again !'});
        });
    }

    onDropDevice(files) {
        if(files){
            this.setState({removetext:true})
        }

        this.setState({ percent: 0, filename:files[0].name, filestatus:''});
        let data = new FormData();
        data.append('files', files[0]);
        const url = 'http://8.9.36.13/factory/BulkDevice';

        const config = {
          headers: { 'content-type': 'multipart/form-data'},
          onUploadProgress: progressEvent => {
            var percent = Math.round(progressEvent.loaded * 100 / progressEvent.total);
            if (percent >= 100) {
              this.setState({ percent: 98 });
            } else {
              this.setState({ percent });
            }
          }
        };
    
        const that = this;

        axios.defaults.headers.common['auth'] = sessionStorage.getItem('DontTouchMe');
        axios.post(url, data, config)
          .then(function(response) {
              //console.log(response);
              if(response.status === 200){
                    that.setState({filestatus:'Successfully updated !', percent: 100})
              }
          })
          .catch(function(error) {
                that.setState({ percent: 0, filestatus:'Error in update, try again !'});
        });
    }

    onDropMetadata(files) {
        if(files){
            this.setState({removetext:true})
        }

        this.setState({ percent: 0, filename:files[0].name, filestatus:''});

        let data = new FormData();
        data.append('files', files[0]);
        const url = 'http://8.9.36.13/factory/Bulkmetadata';

        const config = {
          headers: { 'content-type': 'multipart/form-data'},
          onUploadProgress: progressEvent => {
            var percent = Math.round(progressEvent.loaded * 100 / progressEvent.total);
            if (percent >= 100) {
              this.setState({ percent: 98 });
            } else {
              this.setState({ percent });
            }
          }
        };
    
        const that = this;

        axios.defaults.headers.common['auth'] = sessionStorage.getItem('DontTouchMe');
        axios.post(url, data, config)
          .then(function(response) {
              if(response.status === 200){
                    that.setState({filestatus:'Successfully updated !', percent: 100})
              }
          })
          .catch(function(error) {
                that.setState({ percent: 0, filestatus:'Error in update, try again !'});
        });
    }


    handleSubmitSpecificUpdate(event){
        event.preventDefault();
        let specificupdate = {
            srno : this.state.vin,
            ECU : this.state.ecu,
            UpdateToVersion : this.state.version
        }

        this.setState({
            successfully:'',
            invaliddetails:'',
            allfields:''
        })
        if(this.state.vin && this.state.ecu && this.state.version){
            axios.defaults.headers.common['auth'] = sessionStorage.getItem('DontTouchMe');
            axios.post('http://8.9.36.13/factory/SingleDevice', specificupdate).then(res => {        
                if(res.status === 200){
                    this.setState({successfully:'s'})
                }
                else
                    console.log("Response code other than 200");
            }).catch( er => {
                    if(er.response){
                        if(er.response.status === 401){
                            console.log("Unauthorized or Invalid credential");
                            this.setState({invaliddetails:'Error in update, try again !'})
                        }
                        if(er.response.status === 404){
                            console.log("Not Found")
                        }
                    }
                    else{
                        console.log("Error in response");
                    }
            });
        }
        else{
            this.setState({allfields: 'All fields are required'})
        }
    }

    handleSubmitSpecificUpdatemetadata(event){
        event.preventDefault();

        let specificupdate = {
            ECU : this.state.ecumetadata,
            Available_Version : this.state.versionmetadata,
            File_Path : this.state.filepath,
            //Server: this.state.servermetadata
        }

        this.setState({
            successfully:'',
            invaliddetails:'',
            allfields:''
        })

        if(this.state.ecumetadata && this.state.versionmetadata && this.state.filepath){
            axios.defaults.headers.common['auth'] = sessionStorage.getItem('DontTouchMe');
            axios.post('http://8.9.36.13/factory/SingleMetadata', specificupdate).then(res => {        
                if(res.status === 200){
                    this.setState({successfully:'s'})
                }
                else
                    console.log("Response code other than 200");
            }).catch( er => {
                //console.log(er.response)
                    if(er.response){
                        if(er.response.status === 401){
                            this.setState({invaliddetails:'Error in update, try again !'})
                            console.log("Unauthorized or Invalid credential");
                        }
                        if(er.response.status === 404){
                            console.log("Not Found")
                        }
                    }
                    else{
                        console.log("Error in response");
                    }
            });
        }
        else{
            this.setState({allfields: 'All fields are required'})
        }
    }

    handleSubmitAddDevice(event){
        event.preventDefault();
        let addition = {
            //imei: this.state.imei,
            srno: this.state.serialno
        }
        this.setState({
            successfully:'',
            alreadyregistered:'',
            invaliddetails:'',
            allfields:''
        })
        if(this.state.serialno){
            axios.defaults.headers.common['auth'] = sessionStorage.getItem('DontTouchMe');
            axios.post('http://8.9.36.13/factory/registerDevice', addition).then(res => {  
                if(res.status === 200){
                    console.log("Successfully added");
                    this.setState({successfully:'s'})
                }
                else{
                    console.log("Response code other than 200");
                }
            }).catch( er => {
                if(er.response){
                    if(er.response.status === 401){
                        if(er.response.data.error === 'Device already Registered !'){
                            this.setState({alreadyregistered:'a'})
                        }
                        else{
                            this.setState({invaliddetails:'Invalid Device ID !'})
                        }
                    }
                    if(er.response.status === 404){
                        console.log("Not Found")
                    }
                }
                else{
                    console.log("Error in response");
                }
            });
        }
        else{
            this.setState({allfields: 'Device ID is required'})
        }
    }

    componentWillUnmount(){
       clearInterval(this.interval)
    }

    render(){
            let dropzonefileforaddDevice =  
            <Row>
                <Dropzone id='addreg'  className='dropezone' onDrop={(files) => this.onDropAddger(files)}>                                                                                                       
                    {this.state.removetext !== true ?
                        <div style={{textAlign:'center', marginTop:'10%'}}>
                            <i className="fa fa-upload" aria-hidden="true"></i><br/>
                            Click or drag file here
                        </div>
                        :
                        <div style={{textAlign:'center', marginTop:'10%'}}>
                            <Line percent={this.state.percent} style={{width:'60%'}} strokeWidth='5' strokeColor='green' strokeLinecap='square' trailColor='transparent'/>
                            {this.state.percent ? this.state.percent+'%' : ''}<br/>
                            {this.state.filename ? 'File Name : '+this.state.filename : ''}<br/>
                        </div>
                    }
                        <div style={{marginTop:'60px'}}>      
                            {this.state.filestatus === 'Successfully registered !' ? <div style={{color:'green'}}>{this.state.filestatus}</div> : <div style={{color:'#ff0000'}}>{this.state.filestatus}</div>}                                                         
                        </div>
                    
                </Dropzone>
            </Row>

            let dropzonefileforupdateDevice =
            <Row>
                <Dropzone  className='dropezone' onDrop={(files) => this.onDropDevice(files)}>                                                                                                       
                    {this.state.removetext !== true ? 
                        <div style={{textAlign:'center', marginTop:'10%'}}>
                            <i className="fa fa-upload" aria-hidden="true"></i><br/>
                            Click or drag file here
                        </div>
                        :
                        <div style={{textAlign:'center', marginTop:'10%'}}>
                            <Line percent={this.state.percent} style={{width:'60%'}} strokeWidth='5' strokeColor='green' strokeLinecap='square' trailColor='transparent'/>
                            {this.state.percent ? this.state.percent+'%' : ''}<br/>
                            {this.state.filename ? 'File Name : '+this.state.filename : ''}<br/>
                        </div>
                    }
                    <div style={{marginTop:'60px'}}>
                        {this.state.filestatus === 'Successfully updated !' ? <div style={{color:'green'}}>{this.state.filestatus}</div> : <div style={{color:'#ff0000'}}>{this.state.filestatus}</div>}                                                         
                    </div>
                </Dropzone>
            </Row>

            let dropzonefileforupdateMetaData = 
            <Row>
                <Dropzone  className='dropezone' onDrop={(files) => this.onDropMetadata(files)}>                                                                                                       
                {this.state.removetext !== true ? 
                    <div style={{textAlign:'center', marginTop:'10%'}}>
                        <i className="fa fa-upload" aria-hidden="true"></i><br/>
                        Click or drag file here
                    </div>
                    :
                    <div style={{textAlign:'center', marginTop:'10%'}}>
                        <Line percent={this.state.percent} style={{width:'60%'}} strokeWidth='5' strokeColor='green' strokeLinecap='square' trailColor='transparent'/>
                        {this.state.percent ? this.state.percent+'%' : ''}<br/>
                        {this.state.filename ? 'File Name : '+this.state.filename : ''}<br/>
                    </div>             
                }
                    <div style={{marginTop:'60px'}}>
                        {this.state.filestatus === 'Successfully updated !' ? <div style={{color:'green'}}>{this.state.filestatus}</div> : <div style={{color:'#ff0000'}}>{this.state.filestatus}</div>}                                                         
                    </div>
                </Dropzone>
            </Row>

            return(
                <Row className='container-fluid maincontainer'>
                    <Col xs='12' sm="12" md='2' lg='2'>
                        <a href='/' target='_top'><img className="imgstyle hvr-bounce-in" src={logo} alt="Header Logo" height='35px'/></a>
                    </Col>

                    <Col xs='12' sm="12" md='10' lg='10' style={{fontSize:'15px'}}>
                        <Row>
                            <Col xs='0' sm='0' md='5' lg='5'> <br/></Col>

                            {this.props.checkforadmin === '2' ? 
                                <Col xs='12' sm='4' md='2' lg='2' className='borderright'>
                                    <select onChange={(e)=>{this.handleChange(e, 'state')}} className='dropdown'>
                                        <option hidden>STATE</option>
                                        {this.state.stateList ? 
                                                this.state.stateList.map((t,i) =>
                                                    //console.log(t);
                                                <option key={i} value={t.pid}>{t.state} </option>
                                                    )
                                                :
                                                ''
                                        }  
                                    </select>
                                                
                                </Col>
                                :
                                <Col xs='12' sm='4' md='2' lg='2' className='borderright'>
                                    <div className='coltextcolor'>{base64.decode( utf8.decode( sessionStorage.getItem('$$$1') ) ) ? base64.decode( utf8.decode( sessionStorage.getItem('$$$1') ) ) : 'NA'}</div>   
                                </Col>
                            }

                            {this.props.checkforadmin === '2' ?
                                <Col xs ='12' sm='4' md='2' lg='2' className='borderright'>
                                    <select onChange={(e)=>{this.handleChange(e, 'city')}} className='dropdown' >
                                        <option hidden>CITY</option>
                                        {this.state.cityList ? 
                                                this.state.cityList.map((t,i) =>
                                                    //console.log(t);
                                                <option key={i} value={t.pid}>{t.city} </option>
                                                    )
                                                :
                                                ''
                                        }  
                                    </select>
                                </Col>
                                :
                                <Col xs ='12' sm='4' md='2' lg='2' className='borderright'>
                                    <div className='coltextcolor'>{base64.decode( utf8.decode( sessionStorage.getItem('$$$2') ) ) ? base64.decode( utf8.decode( sessionStorage.getItem('$$$2') ) ) : 'NA'} </div>
                                </Col>
                            }

                            {this.props.checkforadmin === '2' ? 
                                <Col xs='12' sm='4' md='2' lg='2' className='borderright'>
                                    <select onChange={(e)=>{this.handleChange(e, 'subcity')}} className='dropdown' >
                                        <option hidden>Subunit </option>
                                            {this.state.subunitList ? 
                                                this.state.subunitList.map((t,i) =>
                                                    //console.log(t);
                                                <option key={i} value={t.pid}>{t.subunit} </option>
                                                    )
                                                :
                                                ''
                                            }   
                                    </select>
                                </Col> 
                                :
                                <Col xs='12' sm='4' md='2' lg='2' className='borderright'>
                                    <div className='coltextcolor'>{base64.decode( utf8.decode( sessionStorage.getItem('$$$'))) ? base64.decode( utf8.decode( sessionStorage.getItem('$$$'))) : 'NA' } </div>
                                </Col>
                            }
                            

                            <Col xs='12' sm='12' md='1' lg='1'>
                                <Row style={{textAlign:'right'}}> 
                                    <Col xs='8' sm='9' md='0' lg='0'> </Col>

                                    {/* Device notification Component */}
                                    
                                    <Button className='hvr-wobble-bottom' onClick={this.toggleNotification} style={{color:'#ff9800', fontSize:'15px', borderColor:'#232228', backgroundColor:'transparent',  marginTop: '5px'}}><i className="fa fa-bell"> { this.state.currentnotification === 0 ? '' : <div><span className='fa fa-comment'></span> <span className='num'>{this.state.currentnotification > 99 ? 99+'+' : this.state.currentnotification}</span></div> }</i></Button>
                                    
                                    {/* fa-fa-bar menu */}
                                    <div className = "btn-group" style={{marginLeft:'20px', marginTop:'10px'}}>  
                                        <Button className='hvr-border-fade-orange' data-toggle = "dropdown" style={{background:'transparent', border:'1px solid #ff9800', marginTop:'1px', textAlign:'right'}}> <i className='fa fa-bars' style={{color:'#ff9800', fontSize:'15px'}}></i> </Button>
                                        <ul className = "dropdown-menu" role = "menu" style={{backgroundColor:'#232228', left:'-400%'}}>
                                            <Button className='hvr-border-fade-orange'  onClick={this.toggle} style={{color:'#ff9800', fontSize:'15px', borderColor:'#ff9800', backgroundColor:'transparent', margin:'10px'}} outline >Device Management</Button>
                                            <br/>
                                            <a href={constant.logoutLink} ><Button onClick={(e) => {this.props.signout(e)} } className='hvr-border-fade-orange' style={{color:'#ff9800', borderColor:'#ff9800', fontSize:'15px', background:'transparent', marginTop:'5px', marginLeft:'10px'}} outline> <i className="fa fa-sign-out"></i>Sign out</Button></a>
                                        </ul>
                                    </div>
                                    
                                </Row>
                                
                                <Modal size='lg' isOpen={this.state.notificationState} fade={false} centered={false} toggle={this.toggleNotification1} onClosed={this.state.closeAll ? this.toggle : undefined}>
                                {/* this.state.notificationState */}
                                    <ModalBody className='updatedevice'>
                                        <Notification notificationlist={this.state.notificationlist} notificationonclose={this.toggleNotification1} />                                          
                                    </ModalBody>
                                </Modal>

                                <Modal size='sm' isOpen={this.state.modal} centered={true} fade={false} toggle={this.toggle}>
                                {/* this.state.modal */}
                                    <ModalBody className="devicemanagement" style={{textAlign:'center'}}>
                                        {/* <div className="modeldiv"> <b>DEVICE MANAGEMENT</b> </div> */}
                                        <div> 
                                            <Button block className='hvr-border-fade-orange' onClick={this.toggleDeviceList} style={{color:this.state.DeviceListColor, fontSize:'15px', borderColor:'#ff9800', background:'transparent', marginTop:'10px'}} outline>Device Status</Button>
                                            <Button block className='hvr-border-fade-orange' onClick={this.toggleAddDevice} style={{color:this.state.AddDeviceColor, fontSize:'15px', borderColor:'#ff9800' ,background:'transparent', marginTop:'10px'}} outline>Register Device</Button>                         
                                            <Button block className='hvr-border-fade-orange' onClick={this.toggleNestedUpdateDevice} style={{color:this.state.UpdateDeviceColor, fontSize:'15px', borderColor:'#ff9800', background:'transparent', marginTop:'10px'}} outline>Device Firmware Update</Button>
                                            <Button block className='hvr-border-fade-orange' onClick={this.toggleNestedUpdateMetadata} style={{color:this.state.UpdateMetadataColor, fontSize:'15px', borderColor:'#ff9800', background:'transparent', marginTop:'10px'}} outline>Add Device Firmware Version</Button>
                                            <Button block className='hvr-border-fade-orange' onClick={this.toggleNestedModalDeviceStatus} style={{color:this.state.DeviceStatusColor, fontSize:'15px', borderColor:'#ff9800', background:'transparent', marginTop:'10px'}} outline>Device Firmware Update Status</Button>      
                                        </div>
                                    
                                        <div className='closebuttondiv'> 
                                            <br/>
                                            <Button className='hvr-border-fade-orange' onClick={this.toggle} style={{marginTop:'20px', color:'#ff9800', margin:'auto', fontSize:'15px', borderRadius:'5px', borderColor:'#ff9800', background:'transparent'}} outline >Close</Button>
                                        </div>

                                        <Modal size='lg' isOpen={this.state.nestedModalUpdateDevice} fade={false} toggle={this.toggleNestedUpdateDevice} onClosed={this.state.closeAll ? this.toggle : undefined}>
                                        {/* this.state.nestedModalUpdateDevice */}
                                            <ModalBody className='updatedevice'>
                                                {/* <div className='modeldivupdatedevice'>
                                                    {/* <b>UPDATE DEVICES</b>  */}
                                                {/* </div> */} 

                                                <Row style={{marginTop:'30px'}}>   

                                                    <Col xs='12' sm='6' md='6' lg='6' style={{borderRight:'1px solid #616161', textAlign:'center', color:'white'}}>    
                                                        <div className='bulkandspecifi'><b>Bulk Device Firmware Update</b> </div>                                                                                            
                                                            {dropzonefileforupdateDevice}
                                                        <Row style={{marginTop:'110px', marginBottom:'20px'}}>
                                                            <a href='' style={{fontSize:'15px', color:'white', margin:'auto'}} ><u>Download bulk device firmware update template</u></a>                                 
                                                        </Row>
                                                    </Col>

                                                    <Col xs='12' sm='6' md='6' lg='6' style={{textAlign:'center', color:'white'}}>                           
                                                        <div className='bulkandspecifi'> <b>Specific Device Firmware Update</b> </div>                                                     
                                                        
                                                        <Form onSubmit={this.handleSubmitSpecificUpdate} style={{marginTop:'40px', color:'#9e9e9e', fontSize:'15px'}}>                                           
                                                            <FormGroup>                                                                                                          
                                                                    <input type="text" name="vin" id="vin" placeholder="VIN / Device ID" value={this.state.vin} onChange={this.handleChange} />                                                              
                                                            </FormGroup>

                                                            <FormGroup>                                                           
                                                                    <select onChange={(e)=>{this.handleEcuList(e, 'ecuUpdateDevice')}} className='ecudropdown'>
                                                                        <option hidden>Firmware</option>
                                                                        {this.state.ecuList ? 
                                                                            this.state.ecuList.map((t,i) =>
                                                                                //console.log(t);
                                                                            <option key={i} value={t}>{t} </option>
                                                                                )
                                                                            :
                                                                            ''
                                                                        }
                                                                    </select> 

                                                            </FormGroup>

                                                            <FormGroup>                           
                                                                <select onChange={(e)=>{this.handleEcuList(e, 'version')}} className='ecudropdown'>
                                                                        <option hidden>Version</option>                                                          
                                                                        {this.state.versionList ? 
                                                                        this.state.versionList.filter( (item, index) => item['ECU'].toLowerCase()===this.state.ecu.toLowerCase() ).map( (data, i)  => 
                                                                            //console.log("", data, 'index', i) 
                                                                            <option key={i} value={data.Available_Version}>{data.Available_Version} </option>                                     
                                                                        )
                                                                        :
                                                                        ''
                                                                        }                                                       
                                                                </select> 

                                                            </FormGroup>
                                                            {this.state.allfields ? <span style={{color:'#ff0000'}}>{this.state.allfields}</span> : '' }
                                                            {this.state.successfully ==='s' ? <span style={{color:'green'}}>Successfully Updated ! </span> : '' }
                                                            {this.state.invaliddetails ? <span style={{color:'#ff0000'}}>{this.state.invaliddetails}</span> : '' }
                                                            <br/>

                                                            <Button className="hvr-border-fade-orange" type="submit" disabled={this.state.specificupdate} style={{marginTop:'20px', color:'#ff9800', margin:'20px', fontSize:'15px', borderRadius:'5px', borderColor:'#ff9800', backgroundColor:'transparent'}}> Update </Button>
                                                        </Form>
                                                    </Col>
                                                </Row>

                                                <div style={{borderTop:"1px solid #616161", textAlign:'center'}}> 
                                                    <Button className='hvr-border-fade-orange' onClick={this.toggleNestedUpdateDevice} style={{marginTop:'15px', color:'#ff9800', fontSize:'15px', borderRadius:'5px', borderColor:'#ff9800', background:'transparent'}} outline >Close</Button>
                                                </div>
                                            </ModalBody>
                                        </Modal>

                                        <Modal size='lg' isOpen={this.state.nestedModalUpdateMetadata} fade={false} toggle={this.toggleNestedUpdateMetadata} onClosed={this.state.closeAll ? this.toggle : undefined}>
                                        {/* this.state.nestedModalUpdateMetadata */}
                                            <ModalBody className='updatedevice'>
                                                <div className='modeldivupdatedevice'>
                                                    {/* <b>UPDATE METADATA</b>  */}
                                                </div>

                                                <Row style={{marginTop:'30px'}}> 
                                                    <Col xs='12' sm='6' md='6' lg='6' style={{borderRight:'1px solid #616161', textAlign:'center', color:'white'}}>       
                                                        <div className='bulkandspecifi'><b>Bulk Add Device Firmware Version</b> </div>                                                    
                                                            {dropzonefileforupdateMetaData}
                                                        <Row style={{marginTop:'110px', marginBottom:'20px'}}>
                                                            <a href='' style={{fontSize:'15px', color:'white', margin:'auto'}} ><u>Download bulk add device firmware version template</u></a>                                 
                                                        </Row>
                                                    </Col>

                                                    <Col xs='12' sm='6' md='6' lg='6' style={{textAlign:'center', color:'white'}}>                                                     
                                                        <div className='bulkandspecifi'> <b>Specific Add Device Firmware Version</b> </div>                                                      
                                                        <Form onSubmit={this.handleSubmitSpecificUpdatemetadata} style={{marginTop:'40px', color:'#9e9e9e', fontSize:'15px'}}>
                                                            <FormGroup>
                                                                    <select onChange={(e)=>{this.handleEcuList(e, 'ecumetadata')}} className='ecudropdown'>
                                                                        <option hidden>Firmware</option>
                                                                        {this.state.ecuList ? 
                                                                            this.state.ecuList.map((t,i) =>
                                                                                //console.log(t);
                                                                            <option key={i} value={t}>{t} </option>
                                                                                )
                                                                            :
                                                                            ''
                                                                        }
                                                                    </select>                                                                   
                                                            </FormGroup>

                                                            <FormGroup>
                                                                    <input type="text" name="versionmetadata" id="versionmetadata" placeholder="Version" value={this.state.versionmetadata} onChange={this.handleChange}/>                                                           
                                                            </FormGroup>

                                                            <FormGroup>
                                                                    <input type="text" name="filepath" id="filepath" placeholder="File Path e.g sftp://mytext.com/text" value={this.state.filepath} onChange={this.handleChange}/>
                                                            </FormGroup>
                                                            {this.state.allfields ? <span style={{color:'#ff0000'}}>{this.state.allfields}<br/></span> : '' }
                                                            {this.state.successfully ==='s' ? <span style={{color:'green'}}>Successfully Updated ! <br/></span> : '' }
                                                            {this.state.invaliddetails ? <span style={{color:'#ff0000'}}>{this.state.invaliddetails} <br/> </span> : '' }
                                                            <br/>
                                                            
                                                            <Button className="hvr-border-fade-orange" type="submit" style={{marginTop:'20px', color:'#ff9800', margin:'auto', fontSize:'15px', borderRadius:'5px', borderColor:'#ff9800', backgroundColor:'transparent'}}> Update </Button>
                                                        </Form>
                                                    </Col>
                                                </Row>

                                                <div style={{borderTop:"1px solid #616161", textAlign:'center'}}> 
                                                    <Button className='hvr-border-fade-orange' onClick={this.toggleNestedUpdateMetadata} style={{marginTop:'15px', color:'#ff9800', fontSize:'15px', borderRadius:'5px', borderColor:'#ff9800', background:'transparent'}} outline >Close</Button>
                                                </div>
                                            </ModalBody>
                                        </Modal>

                                        <Modal size='lg' isOpen={this.state.addDevice} centered={true} fade={false} toggle={this.toggleAddDevice} onClosed={this.state.closeAll ? this.toggle : undefined}>
                                        {/* this.state.addDevice */}
                                            <ModalBody className='updatedevice'>
                                                <div className='modeldivupdatedevice'>
                                                    {/* <b>ADD DEVICE</b>  */}
                                                </div>

                                                <Row style={{marginTop:'30px'}}> 
                                                    <Col xs='12' sm='6' md='6' lg='6' style={{borderRight:'1px solid #616161', textAlign:'center', color:'white'}}>                                                       
                                                        <div className='bulkandspecifi'><b>Bulk Device Registration</b> </div>                                                      
                                                        {dropzonefileforaddDevice}
                                                        <Row style={{marginTop:'110px', marginBottom:'20px'}}>
                                                            <a href='' style={{fontSize:'15px', color:'white', margin:'auto'}} ><u>Download bulk device registration template</u></a>
                                                        </Row>
                                                    </Col>

                                                    <Col xs='12' sm='6' md='6' lg='6' style={{textAlign:'center', color:'white', fontSize:'15px'}}>                                                      
                                                        <div className='bulkandspecifi'> <b>Specific Device Registration</b> </div>                                                      
                                                        <Form onSubmit={this.handleSubmitAddDevice} style={{marginTop:'40px', color:'#9e9e9e', fontSize:'15px'}}>
                                            
                                                            {/* <FormGroup row style={{marginTop:'30px', marginBottom:'15px'}}>
                                                                <Label sm='3'>IMEI </Label>
                                                                <Col sm={7}>
                                                                    <Input style={{fontSize:'15px', color:'#ff9800', height:'30px', borderColor:'transparent' ,borderBottom:'2px solid #ff9800', backgroundColor:'transparent'}} type="text" name="imei" id="imei" placeholder="IMEI" value={this.state.imei} onChange={this.handleChange} />
                                                                </Col>
                                                            </FormGroup> */}

                                                            <FormGroup style={{marginTop:'15px', marginBottom:'5px'}}>
                                                                    <input  type="text" name="serialno" id="serialno" placeholder="Device ID" value={this.state.serialno} onChange={this.handleChange}/>
                                                            </FormGroup>
                                                            {this.state.allfields ? <span style={{color:'#ff0000'}}><br/>{this.state.allfields}<br/></span> : '' }
                                                            {this.state.invaliddetails ? <span style={{color:'red'}}><br/> Invalid Device ID ! <br/></span> : '' }
                                                            {this.state.successfully ==='s' ? <span style={{color:'green'}}><br/> Successfully registered ! <br/></span> : '' }
                                                            {this.state.alreadyregistered ==='a' ? <span style={{color:'red'}}><br/> Device already registered ! <br/></span> : '' }
                                                            <br/>
                                                            
                                                            <Button disabled={this.state.specificaddition} className="hvr-border-fade-orange" type="submit" style={{marginTop:'20px', color:'#ff9800', margin:'auto', fontSize:'15px', borderRadius:'5px', borderColor:'#ff9800', backgroundColor:'transparent'}}> Submit </Button>
                                                        </Form>
                                                    </Col>
                                                </Row>

                                                <div style={{borderTop:"1px solid #616161", textAlign:'center'}}> 
                                                    <Button className='hvr-border-fade-orange' onClick={this.toggleAddDevice} style={{marginTop:'15px', color:'#ff9800', fontSize:'15px', borderRadius:'5px', borderColor:'#ff9800', background:'transparent'}} outline >Close</Button>
                                                </div>
                                            </ModalBody>
                                        </Modal>

                                        {/*  Device list status with update/updating device */}
                                        <Modal size='lg' isOpen={this.state.nestedModalDeviceStatus} fade={false} toggle={this.toggleNestedModalDeviceStatus} onClosed={this.state.closeAll ? this.toggle : undefined}>
                                        {/* this.state.nestedModalDeviceStatus */}
                                            <ModalBody className='updatedevice'>
                                                <DeviceStatus deviceupdatestatus={this.toggleNestedModalDeviceStatus} />                                              
                                            </ModalBody>
                                        </Modal>

                                        {/*  Device list status with delte device */}
                                        <Modal size='lg' isOpen={this.state.listDevice} fade={false} toggle={this.toggleDeviceList} onClosed={this.state.closeAll ? this.toggle : undefined}>                                        
                                        {/* this.state.listDevice */}
                                            <ModalBody className='updatedevice'>
                                                <DeviceListStatus toggledevicelist={this.toggleDeviceList} />                                              
                                            </ModalBody>
                                        </Modal>
                                    </ModalBody>
                                </Modal>
                            </Col>
                            <Col xs='1' sm='1' md='1' lg='1'> </Col>
                        </Row>
                    </Col>
                </Row>
            )
        }
}
export default Header;
