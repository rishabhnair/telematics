var db = require('./sequelize.js')
var client = require('./fftDB.js')
var fftsignup = (data,callback)=>{
  console.log({data})
  var query = {
    text : 'SELECT * from subunit_db inner join subunit_info on subunit_db.subunit_name = subunit_info.subunit_name where subunit_db.project_id = $1',
    values : [data.project_id]
  }
  db.query(query)
  .then(result=>{
    console.log(result.rows)
    console.log(result.rows[0])
    if(result.rows.length>0 && result.rows[0].fft==true && data.vin){
      var empty = ''
      var user_name = data.srno;
      var full_name = data.srno;
      var vehicle_no = data.vin || empty
      var is_active = true;
      var role_id = 3;
      var project_id = 2;
      var back_office_id = data.project_id;
      var mgrQuery = {
        text : 'SELECT user_id from user_master where back_office_id=$1 and role_id=$2',
        values : [back_office_id,2]
      }

      client.query(mgrQuery)
      .then(mgrID=>{
        if(mgrID.rows.length>0){
          var reporting_manager_id = mgrID.rows[0].user_id
          var newQuery = {
            text : 'INSERT into user_master(user_name,full_name,vehicle_no,is_active,role_id,project_id,back_office_id,reporting_manager_id,modified_on) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9)',
            values : [user_name,full_name,vehicle_no,is_active,role_id,project_id,back_office_id,reporting_manager_id,'now']
          }
          client.query(newQuery)
          .then(res=>{
            callback(false,true)
          })
        }
        else {
          console.log('manager not found')
          callback(true,false)
        }
      })
      .catch(err=>{
        // console.log(err)
        console.log('mgrID error')
        callback(true,false)
      })
    }
    else {
      console.log('Most probably no fft access')
      callback(true,false)
    }
  })
  .catch(err=>{
    console.log({err})
    console.log('Unexpecte Errror')
    callback(true,false)
  })
}

// fftsignup(data)
module.exports = fftsignup;
