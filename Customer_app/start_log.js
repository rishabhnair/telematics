var mqtt = require('mqtt')
var db = require('./sequelize')
var start_log = (sr_no) = {
  var client  = mqtt.connect('tcp://0.0.0.0:1883');
  var query = {
    text : 'select * from log_table where device_id = $1 order by start_tmp desc limit 1',
    value = [sr_no],
  }
  db.query(query)
  .then(result => {
    var query = {
      text : 'insert into can_log(vin,device_id,log_start_tmp) values($1,$2,$3)',
      values : [result.rows[0].vin,result.rows[0].device_id,now],
    }//conencted check
    db.query(query)
    .then(result1 =>{
      const query2 =
            {
              text: 'update devices set status = $1 where sr_no = $2',
              values: ['active',result1.rows[0].device_id],
            }
            db.query(query2)
            .then(result=>{
              var topic = 'param/'+sr_no;
              return 'success';
            }).catch(e=> console.log(e));
    }).catch(e=> console.log(e));
  }).catch(e=> console.log(e));
}

module.exports = start_log;
