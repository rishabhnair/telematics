var express = require('express');
var bodyParser = require('body-parser');
var expressValidator = require('express-validator');
const hbs = require('hbs');
var customer_add = require('./customer_add');
//var User = require('../models/User');
var log_data = require('./log_data');
//var databaseQueries=require('./databaseQueries.js')
var checkuser = require('./checkuser')
var signin = require('./log_user')
//var device_add = require('./device_add')
const db = require('./sequelize')
var signup = require('./signup')
var cmd_device = require('./function/cmd_device')
var batch_file = require('./function/batch_file')
var start_log = require('./function/start_log')
var stop_log = require('./function/stop_log')
var fs = require('fs')
var vin = require('./vin')
var get_vin = require('./function/get_vin')
var reqres = require('./function/reqres')
//var sequelize= new Sequelize('postgres://postgres@localhost:5433/truring');
var app = express();
hbs.registerHelper('ifEquals', function (arg1, arg2, options) {
  return (arg1 == arg2) ? options.fn(this) : options.inverse(this);
});
app.use(expressValidator());
app.use(bodyParser.urlencoded({ extended: true }))
app.use(express.static(__dirname + '/views'));
app.use(bodyParser.json());
var cookieParser = require('cookie-parser');
app.use(cookieParser());

app.set('view engine', hbs);

//pushmeta
const http = require('http');
var dateTime = require('node-datetime');
const getCSV = require('get-csv');
var checkrows = require('./checkRows');
var updatedevices = require('./UpdateDevices');
var upload = require('express-fileupload');
app.use(upload());


app.use(function (req, res, next) {

  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  //res.setHeader('Content-Type', 'application/json');

  next();

})


//push

app.route('/logout')
  .get((req, res) => {
    res.clearCookie('user')
    res.redirect('/login')
  })


app.route('/login')
  .get(checkuser, (req, res) => {
    console.log('hello')
    res.render('signin.hbs')
  })
  .post(signin, (req, res) => {
    console.log(req.body)
  })


app.route('/dashboard')
  .get(checkuser, (req, res) => {
    res.redirect('/listdevices');
  });

app.route('/register')
  .get(checkuser, (req, res) => {
    res.render('register.hbs')
  })

app.route('/c_add')
  .post(customer_add, (req, res, next) => {
    //console.log(req.body);
    var selectQuery = {
      text: 'select sr_no,status from devices where sr_no = $1',
      values: [req.body.sr_no]
    }
    db.query(selectQuery)
      .then(result => {

        console.log(result.rows);
        res.render('listdevices.hbs', { device_info: result.rows });

      })
      .catch(e => console.error(e.stack));



  })

app.route('/listdevices')
  .get(checkuser, (req, res) => {
    var query = {
      text: 'select * from devices where status is not null'
    }
    db.query(query)
      .then(result => {
        console.log(result.rows)
        res.render('listdevices.hbs', { device_info: result.rows })
      })
      .catch(e => {
        console.error(e)
      });
  });

app.route('/logdata*')
  .get(checkuser, (req, res) => {
    var query = {
      text: 'select * from devices left join log_table on devices.did = log_table.device_id where sr_no= $1 order by start_tmp desc;',
      values: [req.query.sr_no],
    }
    db.query(query)
      .then(result => {
        console.log(result.rows[0])
        res.render('logdata.hbs', { device_info: result.rows[0] })
      }).catch(e => console.error(e.stack))
  })
  .post((req, res) => {
    res.render('logdata.hbs', { device_info: req.body })
  })


//Route for create users page
app.route('/signup')
  .get((req, res) => {
    console.log("hit")
    res.render('signup.hbs')  //prepare registration page
  })
  .post(signup, (req, res) => {

  });


//Alerts page
app.route('/error')
  .get((req, res) => {
    console.log("alerts")
    res.render('error.hbs')
  })

//UpdateDevices page
app.route('/updateDevices')
  .get((req, res) => {
    console.log("updateDevice", req.cookies.user);
    res.render('updatedevices.hbs', { username_info: req.cookies.user })
  })

//metadata page
app.route('/updateMetadata')
  .get((req, res) => {
    console.log("Metadata")
    res.render('updatemetadata.hbs')
  })

//historyLogs Page

app.route('/historyLogs')
  .get(checkuser, (req, res) => {
    var query = {
      text: 'select * from "Report" where "Status" is not null'
    }
    db.query(query)
      .then(result => {
        console.log("******************************");
        console.log(result.rows)
        console.log("******************************");
        res.render('historylogs.hbs', { history_info: result.rows })
      })
      .catch(e => {
        console.error(e)
      });
  });

//
// app.route('/historyLogs')
// .get((req,res)=>{
//   console.log("Logs")
//   res.render('historylogs.hbs')
// })


//Route for view user
app.route('/deviceinfo*')
  .get(checkuser, (req, res) => {
    console.log(req.query)
    var query = {
      text: 'select * from devices where sr_no = $1',
      values: [req.query.device_id],
    }
    db.query(query)
      .then(result => {
        console.log(result.rows[0]);
        res.render('register1.hbs', { device_info: result.rows[0] })
      });
  })

app.route('/command')
  .get((req, res) => {

  })
  .post(log_data, (req, res) => {
    console.log(req.body)
    if (req.body.msg == 'start') {
      start_log(req.body, function () {
        console.log('callbacks here')
        cmd_device(req.body)
      })
    }
    if (req.body.msg == 'stop') {
      stop_log(req.body, function () {
        console.log('callback here bro')
        cmd_device(req.body)
      })
    }
    if (req.body.vin_no) {
      console.log("vehicle number")
      //store_vehicle(req.body)
    }
  })

app.route('/cmd')
  .get((req, res, next) => {
    console.log('qefwef');
    res.status(200).json({ success: 'login successful' }).end();
  })
  .post(reqres, (req, res, next) => {
    console.log('asf');
    res.status(200).json({ success: 'login successful' });

  });


//Route for users users page
app.route('/downloadlog')
  .get((req, res) => {
  })
  .post((req, res) => {
    var query = {
      text: 'select * from can_log where device_id = $1',
      values: [sr_no],
    }    //get user's data
    db.query(query)
      .then(result => {
        if (result.rows) {

        }
        else {
          res.send("no download logs")
        }
      })
  })

app.route('/historiclog*')
  .get((req, res) => {
    console.log(req.query.error)
    var query = {
      text: 'select * from devices inner join can_log on devices.device_id=can_log.device_id  where vin = $1 order by log_start_tmp desc',
      //text: 'select * from candata where srno = $1 order by log_start_tmp desc',
      values: [req.query.vin],
    }
    db.query(query)
      .then(result => {
        console.log(result.rows)
        res.render('data.hbs', { devices: result.rows, error: req.query.error })
      })
  })
  .post((req, res) => {
    console.log('enter post')
    batch_file(req.body, function (fname) {
      console.log(fname)
      console.log('\n repsonse \n ')
      //res.send(fname).end();
      res.status(200).json({ success: fname });
      //   res.set({
      //     'Content-Type': 'application/octet-stream',
      //     'Content-Disposition': 'attachment;filename=view.asc'
      //   })
      //   //res.set('Content-type', 'image/png')
      //   var filestream = fs.createReadStream('./views/css/style.css')
      // //  filestream.pipe(res)
      //   res.download('./views/css/style.css')

    });
  })

app.route('/home*')
  .get((req, res) => {
    // console.log("hello")
    // console.log(req.body.file)
    var file = req.query.file;
    res.set({
      'Content-Type': 'application/octet-stream',
      'Content-Disposition': 'attachment;filename=' + file
    })
    //res.set('Content-type', 'image/png')
    var filestream = fs.createReadStream('./files/' + file)
    filestream.on('error', function () {
      console.log('error');
      //console.log(error);
      var v = file.split('_');
      var err = 'file not found';
      res.redirect('http://8.9.36.13:3000/historiclog?vin=' + v[0] + '&error=' + err)
    })
    filestream.pipe(res)

  });

// app.post('*', (req, res) => {
//   console.log(req.body)
//   res.redirect('/login');
// })
// app.get('*', (req, res) => {
//   console.log(req.body)
//   res.redirect('/login');
// })


//Route for delete users

//pushmeta_start

app.post('/Bulkmetadata', function (req, res) {
  console.log("file here ");
  console.log(req.files);
  if (req.files) {
    console.log("req.files.file1", req.files.file1)
    var file = req.files.file1,
      filename = file.name;
    console.log("{{{{{{{{{{{{{{{{{{{{{{{{{{", req.files);
    console.log("####" + filename);
    file.mv("./pub/" + filename, function (err) {
      if (err) {
        console.log("File Upload Failed", filename, err);
        res.send("Error Occured!")
      }
      else {
        getCSV(`./pub/${filename}`, function (err, data) {
          if (err) {
            console.log('invalid csv')
          }
        })
          .catch(function (err) {
            console.log('enter a valid csv')
          }
          )
          .then(row => {
            if (row) {
              console.log(row);
              console.log(typeof row);
              addRecords(row);
              res.send('uploaded successfully');
              //db.end();
              console.log("file uploaded success");

            }
          })
        var addRecords = function (row, res) {
          console.log(row);
          console.log(row[0]);
          for (let i = 0; i < row.length; i++) {
            //element = JSON.stringify(row[i]).split(',');
            var query = {
              text: 'insert into "sftpMetadata" ("ECU","Available_Version","File_Path") values ($1, $2, $3)on CONFLICT on CONSTRAINT pkey DO UPDATE  set "File_Path" = $3',
              //text : 'delete from "sftpMetadata" ',
              //text: 'insert into "Report" ("DateTime", "Username", "SrNo", "ECU", "UpdateToVersion", "Status") VALUES ("2018-09-26 15:40:38", "admin","TELINK098765432", "TCU", "Version6", "Updating")'
              //values: []
              values: [row[i].ECU, row[i].Available_Version, row[i].File_Path],
            }
            db.query(query)
              .then(result => {
                console.log(result);
                console.log("Baby ,", i);
                if (i == row.length - 2) {
                  //res.send('file uploaded successfully');
                }
              })
              .catch(function (err) {
                console.log('error in query')
                console.log(err);
              })
          }
          console.log("file uploaded success");

          //db.end();
        }
      }
    }
    )
  }
})

//---------------------------------------------------------------------------------------SingleMetadata


app.post('/SingleMetadata', function (req, res) {
  console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")
  console.log(req);
  console.log("req body ------------------")
  console.log(JSON.stringify(req.body));
  //console.log("*********", JSON.parse(req.body.file1));
  //res.setHeader('Content-Type', 'application/json');
  // var json = {
  //   "ECU": "DCU",
  //   "Available_Version": "Version4",
  //   "File_Path": "sftp://108.61.89.215"
  // }
  var json = req.body;
  var query = {
    text: 'insert into "sftpMetadata" ("ECU","Available_Version","File_Path") values ($1, $2, $3)on CONFLICT on CONSTRAINT pkey DO UPDATE  set "File_Path" = $3',
    values: [json.ECU, json.Available_Version, json.File_Path]
  }
  db.query(query)
    .then(result => {
      console.log(result);
      console.log("data inserted Successfully");
      res.send("uploaded successfully");
      //db.end()
    }
    )
    .catch(function (err) {
      console.log('error in query');
      res.send("error in query")
      console.log(err)
    })
  //res.status(200).json('error do')
})

//---------------------------------------------------------------------------------------BulkDevice

app.post('/BulkDevice', function (req, res) {
  console.log("file here ");
  console.log(req.files);
  const object1 = req.files;
  console.log("KEYS Now", Object.keys(object1));
  let keyhere = Object.keys(object1);
  console.log("first key", keyhere[0]);
  var zz = keyhere[0];
  let Username = zz;
  console.log("Username ,", Username);
  if (req.files) {
    console.log("zz", zz);
    console.log("first", req.files[zz]);
    //console.log("req.files.file1", req.files.file1)
    var file = req.files[zz],
      filename = file.name;
    console.log("{{{{{{{{{{{{{{{{{{{{{{{{{{---- ", req.files);
    console.log("####" + filename);
    file.mv("./pub/" + filename, function (err) {
      if (err) {
        console.log("File Upload Failed", filename, err);
        res.send("Error Occured!")
      }
      else {
        getCSV(`./pub/${filename}`, function (err, data) {
          if (err) {
            console.log('invalid csv')
          }
        })
          .catch(function (err) {
            console.log('enter a valid csv')
          }
          )
          .then(row => {
            if (row) {
              checkrows.checkrows(row, function (wrongRows) {
                if (wrongRows.length != 0) {
                  console.log(`error in following rows ${wrongRows}`);
                  res.send(`error in following rows ${wrongRows}`);
                }
                else {
                  console.log(`No error in file, pushing sftp paths to devices`);
                  var dt = dateTime.create();
                  var formatted = dt.format('Y-m-d H:M:S');
                  // func(row,formatted,function(err, reply){
                  //   if(err){
                  //
                  //   }
                  //   else {
                  //     // updatedevices.sendsftpPath(row, formatted, Username);
                  //     res.send("updating devices");
                  //
                  //   }
                  // })

                  func1(row,formatted)
                  .then(res1=>{
                    updatedevices.sendsftpPath(row, formatted, Username);
                    res.send("updating devices");
                  })
                  .catch(err=>{
                    console.log({err})
                  })

                }
                // sendsftpPathsToDevices()
              });
            }
          })
        //-------------------
      }
    }
    )
  }
  var func1 = async(row,formatted)=>{
    var result = []
        for(var i= 0;i< row.length; i++){
          var query1 = {
            text: `insert into "Report" ("DateTime","Username","SrNo", "ECU","UpdateToVersion","Status") values ($1, $2, $3, $4, $5, $6)`,
            values: [formatted, Username, row[i].SrNo, row[i].ECU, row[i].UpdateToVersion, "Updating"]
          }
          result[i] = await db.query(query1)
          .catch(err=>{
            throw err
          })
          console.log({result})
          if(i==row.length){
            return values
          }
        }
    }

  var func = async (data,format,callback)=>{
    var row = data
    var dog;
    var formatted = format
    console.log('ROws Length',row.length)
    for(var i= 0;i< row.length; i++){
      var query1 = {
        text: `insert into "Report" ("DateTime","Username","SrNo", "ECU","UpdateToVersion","Status") values ($1, $2, $3, $4, $5, $6)`,
        values: [formatted, Username, row[i].SrNo, row[i].ECU, row[i].UpdateToVersion, "Updating"]
      }
      db.query(query1)
        .then(result => {
          if(i==row.length){
            console.log({i},'Callback Being Called')
            dog="set"
            callback(false, result)
          }
        })
        .catch(function (err) {
          console.log('error in query')
          console.log(err);
          callback(err,false)
        })
    }
    var biscuit = await dog
    console.log("-----------------------------",biscuit)
  }
})

////---------------------------------------------------------------------------------------SingleDevice


app.post('/SingleDevice', function (req, res) {
  console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")
  console.log("req body ------------------")
  console.log(JSON.stringify(req.body));

  var json = req.body;

  var query = {
    text: `select "ECU", "Available_Version" from "sftpMetadata" where("ECU"= $1 AND "Available_Version"= $2)`,
    values: [json.ECU, json.UpdateToVersion]
  }
  db.query(query)
    .then(result => {
      console.log("json", json);
      console.log("result", result);
      console.log("result rows", result.rows)
      if (result.rows.length != 0) {

        console.log('Values are correct, Updating device');
        var dt = dateTime.create();
        var formatted = dt.format('Y-m-d H:M:S');
        var query1 = {
          text: `insert into "Report" ("DateTime","Username","SrNo", "ECU","UpdateToVersion","Status") values ($1, $2, $3, $4, $5, $6)`,
          values: [formatted, json.Username, json.SrNo, json.ECU, json.UpdateToVersion, "Updating"]
        }
        db.query(query1)
          .then(result => {
            var row =[]
            row.push(json)
            updatedevices.sendsftpPath(row, formatted);
            res.send("updating devices").end();
          })
          .catch(function (err) {
            console.log('error in query')
            console.log(err);
          })
      }
      else {
        console.log('invalid values');
        res.send("invalid values");
      }
    })
    .catch(function (err) {
      console.log('error in query')
      console.log(err);
    })


})

//pushmeta_end

//alerts_start
var alertData = {
  mess: []
};
app.get('/alerts', (req, res) => {

  var late = JSON.stringify(alertData);
  //console.log("late", late);
if(late.mess!=null)
{
  console.log("late", late);
}
  res.send(late);

  alertData.mess = [];
  new_alerts = [];

});

app.post('/alerts*', (req, res) => {

  console.log("rr", req.body.alert_msg);
  var z = { "serialNumber1": req.body.sr_no, "alertCode1": req.body.alert_msg };
  alertData.mess.push(z);
  console.log(alertData);
  console.log(JSON.stringify(alertData));

  res.end();
});
//alerts_end

app.listen(3000, function () {
  console.log('app started')
  get_vin();
  vin();
})

//http.Server(app).listen(3000);
