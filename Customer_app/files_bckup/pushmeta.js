const express = require('express');

var bodyparser = require('body-parser');

var app = express();

const http = require('http');
var db = require('./sequelize');
const getCSV = require('get-csv');

var checkrows = require('./checkRows');

var updatedevices = require('./UpdateDevices');

app.use(bodyparser.json());

var upload = require('express-fileupload');

var cookieParser = require('cookie-parser');
app.use(cookieParser());

app.use(function (req, res, next) {

  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  //res.setHeader('Content-Type', 'application/json');

  next();

})

app.use(bodyparser.urlencoded({ extended: true }));

http.Server(app).listen(5003);
app.use(upload());
console.log("Server Started at port 5003");



// app.get('/',function(req,res){
//
// res.sendFile(__dirname+'/index.html');
//
// })


//---------------------------------------------------------------------------------------Bulkmetadata

app.post('/Bulkmetadata', function (req, res) {
  console.log("file here ");
  console.log(req.files);
  if (req.files) {
    console.log("req.files.file1", req.files.file1)
    var file = req.files.file1,
      filename = file.name;
      console.log("{{{{{{{{{{{{{{{{{{{{{{{{{{",req.files);
    console.log("####" + filename);
    file.mv("./pub/" + filename, function (err) {
      if (err) {
        console.log("File Upload Failed", filename, err);
        res.send("Error Occured!")
      }
      else {
        getCSV(`./pub/${filename}`, function (err, data) {
          if (err) {
            console.log('invalid csv')
          }
        })
          .catch(function (err) {
            console.log('enter a valid csv')
          }
          )
          .then(row => {
            if (row) {
              console.log(row);
              console.log(typeof row);
              addRecords(row);
              //db.end();
              console.log("file uploaded success");
              res.send('file uploaded successfully');
            }
          })
        var addRecords = function (row, res) {
          console.log('inside our function');
          console.log(row);
          console.log('printing row0')
          console.log(row[0]);
          for (var i = 0; i < row.length - 1; i++) {
            //element = JSON.stringify(row[i]).split(',');
            var query = {
              //text: 'insert into "sftpMetadata" ("ECU","Available_Version","File_Path") values ($1,$2,$3)',
              //text : 'delete from "sftpMetadata" ', 
              //text: 'insert into "Report" ("DateTime", "Username", "SrNo", "ECU", "UpdateToVersion", "Status") VALUES ("2018-09-26 15:40:38", "admin","TELINK098765432", "TCU", "Version6", "Updating")'
              //values: []
              //values: [row[i].ECU, row[i].Available_Version, row[i].File_Path],
            }
            db.query(query)
              .then(result => console.log(result))
              .catch(function (err) {
                console.log('error in query')
                console.log(err);
              })
          }
          //db.end();
        }
      }
    }
    )
  }
})


//---------------------------------------------------------------------------------------SingleMetadata


app.post('/SingleMetadata', function (req, res) {
  console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")
  console.log(req);
  console.log("req body ------------------")
  console.log(JSON.stringify(req.body));
  //console.log("*********", JSON.parse(req.body.file1));

  //res.setHeader('Content-Type', 'application/json');
  // var json = {
  //   "ECU": "DCU",
  //   "Available_Version": "Version4",
  //   "File_Path": "sftp://108.61.89.215"
  // }
  var json = req.body;
  var query = {
    text: 'insert into "sftpMetadata" ("ECU","Available_Version","File_Path") values ($1,$2,$3)',
    values: [json.ECU, json.Available_Version, json.File_Path]
  }
  db.query(query)
    .then(result => {
      console.log(result);
      //db.end()
    }
    )
    .catch(function (err) {
      console.log('error in query');
      res.send("error in query")
      console.log(err)
    })
  console.log("Uploaded Successfully");
  res.send("Uploaded Successfully");
  res.status(200).json('error do')
})

//---------------------------------------------------------------------------------------BulkDevice

app.post('/BulkDevice', function (req, res) {
  console.log("file here ");
  console.log(req.files);
  const object1 = req.files;
  console.log("KEYS Now",Object.keys(object1));
  let keyhere = Object.keys(object1);
  console.log("first key",keyhere[0]);
  var zz = keyhere[0];

  if (req.files) {
    console.log("first", req.files[zz]);
    //console.log("req.files.file1", req.files.file1)
    var file = req.files[zz],
      filename = file.name;
      console.log("{{{{{{{{{{{{{{{{{{{{{{{{{{---- ",req.files);
    console.log("####" + filename);
    file.mv("./pub/" + filename, function (err) {
      if (err) {
        console.log("File Upload Failed", filename, err);
        res.send("Error Occured!")
      }
      else {
        getCSV(`./pub/${filename}`, function (err, data) {
            if(err){
              console.log('invalid csv')
            }
        })
        .catch(function(err){
          console.log('enter a valid csv')
         }
        )
        .then(row => {
          if(row){
            checkrows.checkrows(row,function(wrongRows){
              if (wrongRows.length != 0){
                console.log(`error in following rows ${wrongRows}`);
                res.send(`error in following rows ${wrongRows}`);
              }
              else
              {
                console.log(`No error in file, pushing sftp paths to devices`);
                updatedevices.sendsftpPath(row)
                res.send("No error in file, pushing sftp paths to devices");
             }
              // sendsftpPathsToDevices()
            });
          }
        })

      }
    }
    )
  }
})

////---------------------------------------------------------------------------------------SingleDevice


app.post('/SingleDevice', function (req, res) {
  console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")
  console.log(req.cookies);
  console.log("req body ------------------")
  console.log(JSON.stringify(req.body));
  
  var json = req.body;

  var query = {
    text: `select "ECU", "Available_Version" from "sftpMetadata" where("ECU"= $1 AND "Available_Version"= $2)`,
    values: [json.ECU,json.UpdateToVersion]
  }
  db.query(query)
  .then(result => {
    console.log("json",json);
    console.log("result",result);
    console.log("result rows",result.rows)
    if(result.rows.length!=0){
      console.log('Values are correct, Updating device')
      updatedevices.sendsftpPath(json);
      console.log("userrrrrrrrrrrrrrrrrrrr", req.cookies.user);
      res.send("Uploaded Successfully").end();
      //res.status(200).json({'msg':'Uploaded Successfully'});
    
    }
    else{
      console.log('invalid values');
      res.send("invalid values");
    }
  })
  .catch(function(err){
    console.log('error in query')
    console.log(err);
  })
  


  
})

