import React, {Component} from 'react'
import {Button} from 'reactstrap'
import { Scrollbars } from 'react-custom-scrollbars';
import axios from 'axios';
import '../../Style/devicelist.css'
import '../../Style/homemodel.css'
import '../../Style/animationandtrasition.css'

class DeviceList extends Component{
    constructor(props){
        super(props);
        this.state={
            buttontextcolor:'white',
            vincolor: '#ea80fc',
            backgroundColor:'#FF7A01',
            borderColor:'1px solid #616161',
            active : 1,
            d: '',
            deviceListData:''
        }
        this.onSelectDevice = this.onSelectDevice.bind(this);
        this.getInitialState = this.getInitialState.bind(this);
        this.myColorbackground = this.myColorbackground.bind(this);
        this.myColortext = this.myColortext.bind(this);
        this.myColorvin = this.myColorvin.bind(this);
    }

    axiosFuncForDeviceList = () => {
        axios.defaults.headers.common['auth'] = sessionStorage.getItem('DontTouchMe');
        axios.get('http://8.9.36.13/factory/deviceinfo').then(res => {
            if(res.status === 200){
                this.setState({deviceListData: res.data.devices});
            }
            else
                console.log("Response code other than 200")
        }).catch( er => {
            if(er.response){
                if(er.response.status === 401)
                    console.log("Unauthorized or Invalid credential")
                if(er.response.status === 404){
                    console.log("Not Found")
                }
            }
            else{
                console.log("Error in response");
            }
        });
    };

    componentDidMount(){
        this.axiosFuncForDeviceList();
        this.interval = setInterval(this.axiosFuncForDeviceList, 5000);
    }

    componentWillReceiveProps(nextProps){
        if(nextProps !== this.props){
            this.props = nextProps
        }
    }

    getInitialState(){
        return { active: 1,
        }
    }
  
    myColorbackground(position) {
        if (this.state.active === position) {
            return "#ff9800";
        }
        return "#232228";
    }

    myColortext(position) {
        if (this.state.active === position) {
            return "black";
        }
        return "white";
    }

    myColorvin(position) {
        if (this.state.active === position) {
            return "black";
        }
        return "#ea80fc";
    }

    onSelectDevice(e, device, index){
        this.props.ondeviceselect(index, device);
        if(this.state.active === index) {
            this.setState({active : index})
        } 
        else {
            this.setState({active : index})
        }
    }

    componentWillUnmount(){
        clearInterval(this.interval);
    }


    render(){
        let filterkey = 'srno'
        let filterValue = this.props.filtervalue;

        if(this.props.checkforCityStateSubunit){
            filterkey = 'project_id'
            filterValue = this.props.filteronCityStateSununit;
        }

        return(
            <div style={{height:'90vh'}}>
                <Scrollbars
                    onScroll={this.handleScroll}
                    onScrollFrame={this.handleScrollFrame}
                    onScrollStart={this.handleScrollStart}
                    onScrollStop={this.handleScrollStop}
                    onUpdate={this.handleUpdate}
                    renderView={this.renderView}
                    renderTrackHorizontal={this.renderTrackHorizontal}
                    renderTrackVertical={this.renderTrackVertical}
                    renderThumbHorizontal={this.renderThumbHorizontal}
                    autoHide
                    autoHideTimeout={1000}
                    autoHideDuration={200}
                    thumbMinSize={20}
                    universal={true}
                    renderThumbVertical={props => < div className="thumb-vertical track-vertical"/>}
                    //renderTrackVertical={props => < div {...props} className="track-vertical"/>} 
                >  
                {this.state.deviceListData ?                  
                        this.state.deviceListData.filter( (item, index) => item[filterkey].toLowerCase().includes(filterValue.toLowerCase())).map((device, index) => 
                        <div key={index} style={{textAlign:'center'}}>
                            <Button className='hvr-border-fade-orange' disabled={device.status === 'Connected' || device.status==='Registered' ? false : true} onClick={(e) =>  {this.onSelectDevice(e, device, index+1)} } style={{borderRadius:'8px',  background:this.state.active === 0 ? '#ff9800' : this.myColorbackground(index+1), color:this.myColortext(index+1), margin:'2px', fontSize:'13px', border:this.state.borderColor, width:'90%', textAlign:'center'}} key={index}><span className='fa fa-circle' style={{fontSize:'10px' ,color:device.status === 'Connected' ? '#00CC00' : device.status === 'New' ? '#fff94c' : device.status === 'Registered' ? 'white' : '#ff0000'}}></span> DEVICE {index+1} <br/> <div style={{color:this.myColorvin(index+1)}}> {device.srno} </div> </Button>
                        </div>

                        )
                        :
                        'Please wait...'
                }
            </Scrollbars>
        </div>
        );
    }
}
export default DeviceList;



