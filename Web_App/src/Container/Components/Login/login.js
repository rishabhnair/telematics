import React, {Component} from 'react';
import {Button, Modal, ModalBody, Form, FormGroup} from 'reactstrap'
import axios from 'axios';
import ForgotPassword from '../ForgotPassword/forgotpassword'
import '../../Style/login.css'
import '../../Style/header.css'
import '../../Style/homemodel.css'
import '../../Style/animationandtrasition.css'
import '../../Style/forgotpassword.css'
import landing from '../../images/landing.png'
var base64 = require('base-64');
var utf8 = require('utf8');

class Entry extends Component{
    constructor(props){
        super(props);
        this.state={
            modal: true,
            nestedModalUpdateDevice: false,
            nestedModalUpdateMetadata: false,
            closeAll: false,

            specificupdate:true,
            emaill:'',
            passwordl:'',

            emails:'',
            usernames:'',
            signupfirst:true,
            signupsecond:false,

            countrys2:'',
            states2:'',
            citys2:'',
            subunits2:'',
            subunitlist:'',
            codes2:'',
            passs21:'',
            passs22:'',
            checkconfirmpassword:true,

            specificupdatemetadata:true,
            signup1:true,

            passwordType:'password',

            verificationcode:false,
            verificationcodemsg:'Refer your email',
            verificationcodemsgoncheck:'',

            invalidDetails:'',

            forgotpassword:false
        }

        this.handleChange = this.handleChange.bind(this);
        this.onselectSubunit = this.onselectSubunit.bind(this);
        this.toggle = this.toggle.bind(this);

        this.toggleNestedUpdateMetadata = this.toggleNestedUpdateMetadata.bind(this);
       
        this.toggleAll = this.toggleAll.bind(this);

        this.toggleForgot = this.toggleForgot.bind(this);

        this.handleSubmitSpecificUpdate = this.handleSubmitSpecificUpdate.bind(this);
        this.handleSubmitSpecificUpdatemetadata = this.handleSubmitSpecificUpdatemetadata.bind(this);

        this.showhidepassword = this.showhidepassword.bind(this);
        this.goback = this.goback.bind(this);
        this.handleSubmitSignup2 = this.handleSubmitSignup2.bind(this);
        this.signupClose = this.signupClose.bind(this);
    }

    axiosFunc = () => {
        if(this.state.emails){
            axios.get('http://8.9.36.13/factory/subunitinfo?email='+this.state.emails)
                .then(res => {
                    if(res.status === 200){
                        this.setState({subunitlist:res.data})
                    }
                }).catch( er => {
                    if(er.response){
                        if(er.response.status === 401)
                            console.log("Unauthorized or Invalid credential")
                        if(er.response.status === 404){
                            console.log("Not Found")
                        }
                    }
                    else{
                        console.log("Error in response");
                    }
                });
            }
    };

    componentDidMount() {
        this.axiosFunc();
        //this.interval = setInterval(this.axiosFunc, 2000);
    }

    toggle() {
        this.setState({
            // modal: !this.state.modal,
            emails:'',
            usernames:'',
            countrys2:'',
            states2:'',
            citys2:'',
            subunits2:'',
            codes2:'',
            passs21:'',
            passs22:'',
            emaill:'',
            passwordl:'',
            checkconfirmpassword:true,
            invalidDetails:''
        });
    }

    toggleForgot(){
        this.setState({
            forgotpassword: !this.state.forgotpassword,
            invalidDetails:''
        })
    }

    toggleNestedUpdateMetadata() {
    this.setState({
        nestedModalUpdateMetadata: !this.state.nestedModalUpdateMetadata,
        modal: !this.state.modal,
        signupfirst:true,
        signupsecond:false,
        closeAll: false,
        passwordType: 'password',
        emails:'',
        usernames:'',
        countrys2:'',
        states2:'',
        citys2:'',
        subunits2:'',
        codes2:'',
        passs21:'',
        passs22:'',
        emaill:'',
        passwordl:'',
        checkconfirmpassword:true,
        invalidDetails:''
    });
    }

    toggleAll() {
    this.setState({
        nestedModalUpdateDevice: !this.state.nestedModalUpdateDevice,
        closeAll: true,
    });
    }

    showhidepassword(e){
        this.setState({
            passwordType:this.state.passwordType === 'text' ? 'password' : 'text'
        });
    }

    goback(e){
        this.setState({
            signupfirst:true,
            signupsecond:false
        })
    }

    handleChange(e, check) {
        const target = e.target;
        const name  = target.name;
        const value = target.value;

        if(this.state.emaill.length !== 0 && this.state.passwordl.length !== 0){
            this.setState({specificupdate : false})
        }
        else{
            this.setState({specificupdate : true})
        }

        if(this.state.emails.length !== 0 && this.state.usernames.length !== 0){
            this.setState({signup1 : false})
        }
        else{
            this.setState({signup1 : true})
        }

        if( name === 'countrys2' || name === 'states2' || name === 'subunits2' || name === 'citys2' || name === 'codes2' || name === 'passs21' || name === 'passs22'){
            
            if(this.state.countrys2.length !== 0 && this.state.states2.length !== 0 && this.state.citys2.length !== 0 && this.state.subunits2.length !== 0 && this.state.codes2.length !== 0 && this.state.passs21.length !==0 && this.state.passs22.length !==0){
                this.setState({specificupdatemetadata : false})
            }
            else{
                this.setState({specificupdatemetadata : true})
            }
        }
        this.setState({
            [name]: value
        });
    }

    onselectSubunit(e){
        this.setState({subunits2: e.target.value})
    }

    signupClose(e){
        this.setState({
            signupsecond:false,
            nestedModalUpdateMetadata: false,
            emails:'',
            usernames:'',
            countrys2:'',
            states2:'',
            citys2:'',
            subunits2:'',
            codes2:'',
            passs21:'',
            passs22:'',
            emaill:'',
            passwordl:'',
            checkconfirmpassword:true
        })
    }


    handleSubmitSpecificUpdate(event){
        event.preventDefault();
        this.setState({invalidDetails:''})

        var bytes = utf8.encode(this.state.passwordl);
        var encoded = base64.encode(bytes);

        let login = {
            username : this.state.emaill,
            password : encoded
        }

        if(this.state.emaill && this.state.passwordl){
            axios.post('http://8.9.36.13/factory/login', login).then(res => {  
                    if(res.status === 200){
                        sessionStorage.setItem("noti", 0);
                        sessionStorage.setItem('$$$1', base64.encode( utf8.encode(res.data.state) ));
                        sessionStorage.setItem('$$$2', base64.encode( utf8.encode(res.data.city) ));
                        sessionStorage.setItem('$$$', base64.encode( utf8.encode(res.data.subunit_name) ));
                        sessionStorage.setItem('kuchhbhi', base64.encode( utf8.encode(this.state.emaill) ));
                        sessionStorage.setItem('role', res.data.role)
                        sessionStorage.setItem('DontTouchMe', res.data.token);
                        sessionStorage.setItem('srno', base64.encode( utf8.encode( res.data.srno ) ));
                        
                        this.props.history.push({
                            pathname: '/',
                            //search: '/',
                            //state: { srno: res.data.srno }
                        })
                    }
                    else{
                        console.log("Response status code other than 200");
                    }
            }).catch( er => { 
                if(er.response){
                    if(er.response.status === 401){
                        this.setState({invalidDetails:'Email or Password is incorrect'})
                    }
                    if(er.response.status === 404){
                        console.log("Not Found")
                    }
                }
                else{
                    console.log("Error in response");
                }
            });
        }
        else{
            this.setState({invalidDetails:'Email or Password is incorrect'})
        }
    }

    handleSubmitSpecificUpdatemetadata(event){
        event.preventDefault();
        this.setState({invalidDetails:''})
        let signupfirst = {
            name : this.state.usernames,
            username : this.state.emails,
        }
        if(this.state.usernames && this.state.emails){
            axios.post('http://8.9.36.13/factory/signup', signupfirst).then(res => {  
                if(res.data.public === false){
                    if(res.status === 200){
                        this.componentDidMount();
                        this.setState({signupfirst:false, signupsecond:true});
                        this.setState({verificationcode:true, verificationcodemsgoncheck:''});
                    }
                    else{
                        console.log("Response status code other than 200");
                    }
                }
                else{
                    this.setState({invalidDetails:'Unregistered domain name'})
                }
            }).catch( er => {
                if(er.response){
                    if(er.response.status === 401){
                        this.setState({invalidDetails:'User already registered'})
                    }
                    if(er.response.status === 404){
                        console.log("Not Found")
                    }
                }
                else{
                    console.log("Error in response");
                }
            });
        }
        else{
            this.setState({invalidDetails:'Username or Email is incorrect'})
        }
    }

    handleSubmitSignup2(event){
        this.setState({checkconfirmpassword:true, verificationcode:false, verificationcodemsgoncheck:'', allfields:''})
        event.preventDefault();
        if(this.state.passs22.length !==0 && this.state.passs21.length !== 0){
            if(this.state.passs21 !== this.state.passs22){
                this.setState({checkconfirmpassword :false, verificationcode:true})
            }
            else{
                if(this.state.countrys2 && this.state.citys2 && this.state.states2 && this.state.subunits2 && this.state.codes2){
                        this.setState({checkconfirmpassword :true})   
                        var bytes = utf8.encode(this.state.passs21);
                        var encoded = base64.encode(bytes);

                        let signupsecond = {
                            country : this.state.countrys2,
                            city : this.state.citys2,
                            state : this.state.states2,
                            subunit : this.state.subunits2,
                            code : this.state.codes2,
                            password : encoded,
                        }
                    
                        axios.post('http://8.9.36.13/factory/auth', signupsecond).then(res => {  
                            if(res.status === 200){
                                this.setState({modal:true, signupsecond:false, toggleNestedUpdateMetadata: false});
                            }
                            else{
                                console.log("Response status code other than 200");
                            }
                        }).catch( er => {
                            console.log(er.response)
                            if(er.response){
                                if(er.response.status === 401){
                                    this.setState({verificationcode:false, verificationcodemsgoncheck:'Incorrect or Expired Code'})
                                }
                                if(er.response.status === 404){
                                    console.log("Not Found")
                                }
                            }
                            else{
                                console.log("Error in response");
                            }
                        });
                }
                else{
                    this.setState({
                        allfields: 'All fields are required'
                    })
                }
            }
        }
        else{
            this.setState({checkconfirmpassword :false})
        }
    }

    componentWillUnmount(){
        //clearInterval(this.interval)
    }

    render(){
        return(
            <div> 
                <img src={landing} alt="Header Logo" style={{width:'100%', height:'100%'}}/>
                
                <Modal size='md' isOpen={this.state.forgotpassword} fade={false} toggle={this.toggleForgot}>                                        
                    <ModalBody className='updatedevice'>
                        <ForgotPassword toggleForgot={this.toggleForgot} />                                             
                    </ModalBody>
                </Modal>
                
                <Modal isOpen={this.state.modal} fade={false} toggle={this.toggle}>
                    <ModalBody className='updatedevice'>
                        <div className='modeldivupdatedevice'> <h2><b>Sign in</b></h2> </div>
                        
                        <div className='l-loginModalDiv'> 
                            <Form onSubmit={this.handleSubmitSpecificUpdate}>                               
                                <FormGroup>
                                        <input  type="email" name="emaill" id="emaill" placeholder="Email id" value={this.state.emaill} onChange={this.handleChange}  />
                                </FormGroup>

                                <FormGroup>
                                        <input style={{marginLeft:'31px'}} type={this.state.passwordType} name="passwordl" id="passwordl" placeholder="Password" minLength={8} value={this.state.passwordl}  onChange={this.handleChange}/><Button  onClick={this.showhidepassword} style={{background:'transparent', border:'transparent', fontSize:'16px', color:'grey', marginTop:'0px'}}><i className="fa fa-eye" aria-hidden="true"></i></Button>
                                        <div style={{textAlign:'center'}}><button onClick={this.toggleForgot} className='button1'>Forgot password?</button></div>
                                </FormGroup>
                                {this.state.invalidDetails ? <span style={{color:'#ff0000'}}>{this.state.invalidDetails} ! </span> : '' }
                                <br/>

                                <Button type="submit" disabled={this.state.specificupdate} className='hvr-border-fade-orange' style={{borderRadius:'5px', margin:'10px', color:'#ff9800', border:'1px solid #ff9800',backgroundColor:'transparent', fontSize:'15px', width:'23%'}} outline > Sign in </Button>    
                            </Form>       
                        </div>

                        <div className='closebuttondiv'> 
                            <br/>
                            Don't have an account ?
                            <button className='button1' onClick={this.toggleNestedUpdateMetadata} style={{fontSize:'14px', color:'white'}}>Sign up Now</button> 
                            <br/>  <i className="fa fa-hand-o-right" aria-hidden="true"></i>
                            <a href='http://tmdev.truringsoftek.co.in/home/' target='_self'><button className='button1' style={{fontSize:'14px', color:'white'}}>Home</button></a>
                        </div>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.nestedModalUpdateMetadata} fade={false} toggle={this.toggleNestedUpdateMetadata} onClosed={this.state.closeAll ? this.toggle : undefined}>
                    <ModalBody className='updatedevice'>
                        <div className='modeldivupdatedevice'> <h2><b>Sign up</b></h2> </div>
                            <div className='l-loginModalDiv'> 

                                {this.state.signupfirst ? 
                                    <Form onSubmit={this.handleSubmitSpecificUpdatemetadata} style={{marginTop:'40px', color:'#9e9e9e', fontSize:'15px'}}>
                                        <FormGroup>
                                                <input  type="text" name="usernames" id="usernames" placeholder="Username" value={this.state.usernames} onChange={this.handleChange} />
                                        </FormGroup>

                                        <FormGroup>
                                                <input  type="email" name="emails" id="emails" placeholder="Email Id" value={this.state.emails} onChange={this.handleChange}/>
                                        </FormGroup>
                                        
                                        {this.state.invalidDetails ? <span style={{color:'#ff0000'}}>{this.state.invalidDetails}</span> : '' }<br/>

                                        <Button type="submit" disabled={this.state.signup1} className='hvr-border-fade-orange' style={{borderRadius:'5px', margin:'15px', color:'#ff9800', border:'1px solid #ff9800',backgroundColor:'transparent', fontSize:'15px', width:'23%'}} outline > Next </Button>
                                    </Form>
                                    :
                                    ''
                                }
                    
                                {this.state.signupsecond ? 
                                    <Form onSubmit={this.handleSubmitSignup2} style={{marginTop:'40px', color:'#9e9e9e', fontSize:'15px'}}>
                                        <FormGroup>
                                                <input  type="text" name="countrys2" id="countrys2" placeholder="Country" value={this.state.countrys2} onChange={this.handleChange} />
                                        </FormGroup>

                                        <FormGroup>
                                                <input  type="text" name="states2" id="states2" placeholder="State" value={this.state.states2} onChange={this.handleChange} />
                                        </FormGroup>

                                        <FormGroup>
                                                <input  type="text" name="citys2" id="citys2" placeholder="City" value={this.state.citys2} onChange={this.handleChange} />
                                        </FormGroup>

                                        <FormGroup>
                                            <select onChange={(e)=>{this.onselectSubunit(e)}} className='subunitdropdown'>
                                                    <option hidden>Subunit</option>                                                          
                                                    {this.state.subunitlist ? 
                                                        this.state.subunitlist.map((t,i) =>
                                                            //console.log(t);
                                                        <option key={i} value={t.subunit_name}>{t.subunit_name} </option>
                                                            )
                                                        :
                                                        ''
                                                    }                                                      
                                            </select> 
                                        </FormGroup>

                                        <FormGroup>
                                                <input  type="text" name="codes2" id="codes2" placeholder="Enter Verification Code" value={this.state.codes2} onChange={this.handleChange} />
                                                <br/>{this.state.verificationcode ? <span style={{color:'green'}}>{this.state.verificationcodemsg}</span> : '' }
                                                <br/>{this.state.verificationcodemsgoncheck ? <span style={{color:'#ff0000'}}>{this.state.verificationcodemsgoncheck}</span> : '' }
                                        </FormGroup>

                                        <FormGroup>
                                                <input  type={this.state.passwordType} name="passs21" id="passs21" placeholder="Password" value={this.state.passs21} minLength={8} onChange={this.handleChange} />
                                        </FormGroup>

                                        <FormGroup>
                                                <input style={{marginLeft:'31px'}}  type={this.state.passwordType} name="passs22" id="passs22" placeholder="Confirm Password" value={this.state.passs22} minLength={8} onChange={this.handleChange} /> <Button  onClick={this.showhidepassword} style={{marginTop:'0px' ,background:'transparent', border:'transparent', fontSize:'16px', color:'grey'}}><i className="fa fa-eye" aria-hidden="true"></i></Button>
                                                <br/>{this.state.checkconfirmpassword ? '' : <span style={{color:'#ff0000'}}>Those passwords didn't match. Try again.</span>}
                                        </FormGroup>
                                        {this.state.allfields ? <span style={{color:'#ff0000'}}>{this.state.allfields}<br/></span> : ''}
                                        <Button onClick={this.goback} className='hvr-border-fade-orange' style={{borderRadius:'5px', margin:'10px', color:'#ff9800', border:'1px solid #ff9800',backgroundColor:'transparent', fontSize:'15px', width:'23%'}} outline > Go Back </Button>
                                        <Button type="submit" disabled={this.state.specificupdatemetadata} className='hvr-border-fade-orange' style={{borderRadius:'5px', margin:'10px', color:'#ff9800', border:'1px solid #ff9800',backgroundColor:'transparent', fontSize:'15px', width:'23%'}} outline > Signup </Button> 
                                    </Form> 
                                        :
                                        ''
                                }
                            </div> 

                            <div className='closebuttondiv'> 
                                {this.state.signupfirst ? '' :
                                    <div><br/> Already have an account ?  <button className='button1' onClick={this.toggleNestedUpdateMetadata} style={{color:'white', fontSize:'14px'}}> Sign in Now </button>
                                    <br/> </div> 
                                }
                                {this.state.signupfirst ? 
                                   <div><br/> Already have an account ?
                                        <button className='button1' onClick={this.toggleNestedUpdateMetadata} style={{fontSize:'14px', color:'white'}}>Sign in Now</button>
                                    </div>
                                    : '' }
                            </div>
                        </ModalBody>
                    </Modal>
                </div>
                
            )
        }
    }
export default Entry;