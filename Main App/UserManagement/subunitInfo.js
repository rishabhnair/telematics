var db  = require('../sequelize.js')

var subunitInfo = (email,callback)=>{
  var cpemail = ((email).split('@'))[1]
  console.log({cpemail})
  var query = {
    text : 'SELECT * from subunit_db inner join subunit_info on subunit_db.subunit_name = subunit_info.subunit_name where company_mail= $1',
    values : [cpemail]
  }
  db.query(query)
  .then(res=>{
    console.log(res.rows)
    if(res.rows.length>0){
      console.log(res.rows)
      callback(false,res.rows)
    }
    else {
      callback(true,false)
    }
  })
  .catch(err=>{
    console.log({err})
    callback(true,false)
  })
}

module.exports = subunitInfo;
