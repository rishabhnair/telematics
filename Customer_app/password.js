  var bcrypt = require('bcryptjs');
  const salt = bcrypt.genSaltSync();

  var checkpass = (pass,qpass)=>{
    return bcrypt.compareSync(pass, qpass);
  };

  var createpass = (pass)=>{
    return bcrypt.hashSync(pass, salt);
  } ;


  module.exports = {checkpass,createpass};
