const db = require('../sequelize');
const elastic = require('./elasticize');
const redis = require('./redisize');
var putToDB = require('./putToDB')

var getFromElastic = function(vin){
  var stop= new Date (Date.now());
  redis.hget('tripy',vin,function(error,result){
     if(error){
       console.log('redis error:')
       console.log(error);
     }
       else {
       var start = new Date (JSON.parse(result).start_time);
       console.log(start)
       console.log(stop)
       startElasticFormat = start.toISOString().split('.')[0].replace(/[-:]/g, '')+'Z';
       stopElasticFormat = stop.toISOString().split('.')[0].replace(/[-:]/g, '')+'Z';
       elastic.search({
           index: 'testdata',
           type: 'parameter',
           body: {
             "query": {
                "bool": {
                 "filter": [
                   { "match":  { "vin": vin }},
                   { "range": { "DateTime": { "gte": "20150101T000000Z", "lte": stopElasticFormat }}}
                 ]
                }
             }
           }
       }).then(function(resp) {
         console.log("going inside putToDB")
         putToDB.insertquery(vin,resp,start,stop)
         .then(res=>{
           callback(false,res)
         })
         .catch(err=>{
           callback(true,false)
         })
       })
}
})
}
module.exports = {
  getFromElastic
}
