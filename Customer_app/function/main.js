var express = require('express');
var bodyParser=require('body-parser');
var expressValidator = require('express-validator');
const hbs = require('hbs');
var customer_add = require('./customer_add');
//var User = require('../models/User');
var log_data = require('./log_data');
//var databaseQueries=require('./databaseQueries.js')
var checkuser=require('./checkuser')
var signin = require('./log_user')
//var device_add = require('./device_add')
const db = require('./sequelize')
var signup = require('./signup')
var cmd_device = require('./function/cmd_device')
//var sequelize= new Sequelize('postgres://postgres@localhost:5433/truring');
var app=express();
hbs.registerHelper('ifEquals', function(arg1, arg2, options) {
  return (arg1 == arg2) ? options.fn(this) : options.inverse(this);
});
app.use(expressValidator());
app.use(bodyParser.urlencoded({extended:false}))
app.use(express.static(__dirname + '/views'));
app.use(bodyParser.json());
var cookieParser = require('cookie-parser');
app.use(cookieParser());

app.set('view engine',hbs);



app.route('/logout')
.get((req,res)=>{
  res.clearCookie('user')
  res.redirect('/login')
})


app.route('/login')
.get(checkuser,(req,res)=>{
  console.log('hello')
  res.render('signin.hbs')
})
.post(signin,(req,res)=>{
  console.log(req.body)
})


app.route('/dashboard')
.get(checkuser,(req,res)=>{
  res.redirect('/listdevices');
});

app.route('/register')
.get(checkuser,(req,res)=>{
  res.render('register.hbs')
})

app.route('/c_add')
.post(customer_add,(req, res, next)=>{
	//console.log(req.body);
	var selectQuery = {
		text : 'select sr_no,status from devices where sr_no = $1',
		values : [req.body.sr_no]
	}
	db.query(selectQuery)
  	.then(result => {

  		console.log(result.rows);
  		res.render('listdevices.hbs',{device_info : result.rows});

	})
	.catch(e => console.error(e.stack));



 })

app.route('/listdevices')
.get(checkuser,(req,res)=>{
    var query= {
      text : 'select * from devices where status is not null'
    }
    db.query(query)
    .then(result =>{
      console.log(result.rows)
      res.render('listdevices.hbs',{device_info : result.rows})
    })
    .catch(e =>{
      console.error(e)
    });
});

app.route('/logdata*')
.get(checkuser,(req,res)=>{
  var query = {
    text : 'select * from devices left join log_table on devices.did = log_table.device_id where sr_no= $1 order by start_tmp desc;',
    values : [req.query.sr_no],
  }
  db.query(query)
  .then(result =>{
    console.log(result.rows[0])
    res.render('logdata.hbs',{device_info : result.rows[0]})
  }).catch(e=>console.error(e.stack))
})
.post((req,res)=>{
  res.render('logdata.hbs',{device_info : req.body})
})


//Route for create users page
app.route('/signup')
.get((req,res)=>{
  console.log("hit")
	res.render('signup.hbs')  //prepare registration page
})
.post(signup,(req,res)=>{

});


//Route for view user
app.route('/deviceinfo*')
.get(checkuser,(req,res)=>{
  console.log(req.query)
	var query={
    text : 'select * from devices where sr_no = $1',
    values : [req.query.device_id],
  }
  db.query(query)
  .then(result=>{
    console.log(result.rows[0]);
    res.render('register1.hbs',{device_info : result.rows[0]})
  });
})

app.route('/command')
.get((req,res)=>{

})
.post(log_data,(req,res)=>{
  if(req.body.msg=='start'){
    start_log(req.body,function(){
      console.log('callbacks here')
      cmd_device(req.body)
    })
  }
  if(req.body.msg=='stop'){
    stop_log(req.body,function(){
      console.log('callback here bro')
      cmd_device(req.body)
    })
  }
  if(req.body.vin_no){
    console.log("vehicle number")
    //store_vehicle(req.body)
  }
})


//Route for users users page
app.route('/downloadlog')
.get((req,res)=>{
})
.post((req,res)=>{
  var query={
    text : 'select * from can_log where device_id = $1',
    values : [sr_no],
  }    //get user's data
  db.query(query)
  .then(result=>{
    if(result.rows){

    }
    else{
      res.send("no download logs")
    }
  })
  })

  app.get('*',(req,res)=>{
    res.redirect('/login');
  })

//Route for delete users



app.listen(3000)
