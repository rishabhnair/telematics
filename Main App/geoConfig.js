var db = require('./sequelize.js')

var geoConfig = (data,pid,callback)=>{
  console.log({data,pid})
  var query = {
    text : 'Update subunit_info set geodist = $1 where subunit_name in (select subunit_name from subunit_db where project_id = $2)',
    values : [data.geodist,pid]
  }
  db.query(query)
  .then(resp=>{
    callback(false,true)
  })
  .catch(err=>{
    callback(true,false)
  })
}

module.exports = geoConfig;
