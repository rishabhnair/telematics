//FileName : databaseQueries.js
//Author : Venus Gupta
//CreationDate: 27/3/2018
//Description: made for database queries


//------------ Adding required files and modules ------------------
const Sequelize = require('Sequelize');
var User = require('../models/User');


//------------ Instantiated module objects ------------------
var sequelize= new Sequelize('postgres://postgres@localhost:5433/truring');


//---------------------------------Database queries------------------------------------
//prepare registration page
var getRegistrationData=(res)=>{
	sequelize.query('Select rm.role_name, rm.role_id from "role" as rm',{type:sequelize.QueryTypes.SELECT})
.then(role=>{
sequelize.query('Select usr.name from "users" as usr',{type:sequelize.QueryTypes.SELECT})
.then(reporting_manager=>{
    res.render('register.hbs',{
    	role:role,
    	reporting_manager:reporting_manager
          })
	     })
	    })
}

//get user's data
var getUserData=(req,res)=>{
	var id = req.query.id; // $_GET["id"]

      User.findAll({
		attributes:['name','email','username','password','role','reporting_manager','back_office','phone','designation','department'],
		where:{id:id}
	}).then(user=>{
sequelize.query('Select role_name from "role" as rm where role_id='+user[0].role+';',{type:sequelize.QueryTypes.SELECT})
.then(role_name=>{
sequelize.query('Select rm.role_name, rm.role_id from "role" as rm where not role_id='+user[0].role+';',{type:sequelize.QueryTypes.SELECT})
.then(rm=>{

sequelize.query(`Select distinct rm.reporting_manager from "users" as rm where not reporting_manager='${user[0].reporting_manager}';`,{type:sequelize.QueryTypes.SELECT})
.then(reporting_manager=>{
  sequelize.query(`Select bo.back_office from "back_office" as bo where back_office_id='${user[0].back_office}';`,{type:sequelize.QueryTypes.SELECT})
.then(back_office=>{
sequelize.query(`Select bo.back_office,bo.back_office_id from "back_office" as bo where not back_office_id='${user[0].back_office}';`,{type:sequelize.QueryTypes.SELECT})
.then(bck_office=>{
	res.render('editUser.hbs',{
			name:user[0].name,
			email:user[0].email,
			username:user[0].username,
			password:user[0].password,
			role:user[0].role,
			reporting_manager:user[0].reporting_manager,
			role_name:role_name[0].role_name,
			roles:rm,
			rm:reporting_manager,
			back_office:back_office[0].back_office,
			back_office_id:back_office[0].back_office_id,
			bck_office:bck_office,
			phone:user[0].phone	,
			designation:user[0].designation,
			department:user[0].department
		})
	console.log(JSON.stringify(back_office[0].back_office+'back_officeeeeeeeeeeee'))
})
})



})
	})
})
	})
}


//Fetch user information
var deleteUserData=(req,res)=>{
	var id = req.query.id; // $_GET["id"]

      User.findAll({
		attributes:['name','email','username','password','role','reporting_manager','back_office','phone'],
		where:{id:id}
	})
}

//Update Function
var UpdateUser=(req,res)=>{

 let name = req.body.name;
   let username = req.body.username;
   let email = req.body.email;
   let password = req.body.password;
   let repassword = req.body.repassword;
   let reporting_manager = req.body.reporting_manager;
   let back_office = req.body.back_office;
   let designation = req.body.designation;
   let department = req.body.department;

	var selector = {
  where: { username: username }
};
	User.update({
     name:name,
     email:req.body.email,
	 password:req.body.password,
	 role:req.body.role,
	 reporting_manager : req.body.reporting_manager,
     back_office: req.body.back_office,
     phone:req.body.phone ,
     department:department,
     designation:designation
  },selector)
  .then(response=>{
      res.send('Edited')
  })
  .catch(error=>{

		res.send(error.errors[0].message)      })

}

//List of all users
var getAllUsers=(res)=>{
	User.findAll({
		attributes:['id','name','email','username','role','reporting_manager','back_office','phone','designation','department']
	}).then(user=>{
		res.render('viewUser.hbs',{
			user:user
		})
	})
}

//Create user function
var createUser=(req,res)=>{
   let name = req.body.name;
   let username = req.body.username;
   let email = req.body.email;
   let password = req.body.password;
   let repassword = req.body.repassword;
   let reporting_manager = req.body.reporting_manager;
   let designation = req.body.designation;
   let department = req.body.department;
   let back_office = req.body.back_office;

	 User.create({
		name:req.body.name,
		username:req.body.username,
		email:req.body.email,
		password:req.body.password,
		role:req.body.role,
		reporting_manager : req.body.reporting_manager,
        back_office: req.body.back_office,
        phone:req.body.phone  ,
        designation:designation,
        department:department
	}).then(response=>{
		res.send('User created')
	}).catch(err=>{
		res.send(err.errors[0].message)
	})

}

//Delete User function
var deleteUser=(req,res)=>{
	var id = req.query.id; // $_GET["id"]

	User.destroy({
      where:{id:id}
	}).then(response=>{
		res.send('Deleted completely')
	})
}

//export all functions
module.exports={
	getRegistrationData,
	getUserData,
	deleteUserData,
	UpdateUser,
	getAllUsers,
	createUser,
	deleteUser
}
