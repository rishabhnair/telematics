var db = require('../sequelize.js')

var singleMetadata = (json,callback)=>{
  console.log
  var query = {
    text: 'insert into "sftpMetadata" ("ECU","Available_Version","File_Path","checksum") values ($1, $2, $3, $4)on CONFLICT on CONSTRAINT pkey DO UPDATE  set "File_Path" = $3,"checksum"= $4',
    values: [json.ECU, json.Available_Version, json.File_Path, json.Checksum]
  }
  db.query(query)
    .then(result => {
      console.log(result);
      console.log("data inserted Successfully");
      callback(false,true)
    }
    )
    .catch(function (err) {
      console.log(err)
      callback(true,false)
    })
}

module.exports = singleMetadata;
