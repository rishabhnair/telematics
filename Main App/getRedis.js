var redis = require('redis')
var client = redis.createClient();
var getRedis = (key)=>{
return new Promise(function(resolve,reject){
  client.get(key,function(err,reply){
    if(err){
      reject(err)
    }
    else if(reply=='null'){
      reject('Invalid or Expired Key')
    }
    else {
      resolve(reply)
    }
  })
})
}

module.exports = getRedis;
