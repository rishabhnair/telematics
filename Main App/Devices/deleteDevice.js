// var getRedis = require('../getRedis.js')
var redis = require('redis')
var client = redis.createClient();
var db = require('../sequelize.js')
var deletelogs = require('./deletelogs.js')
var deleteDevice = (data,callback)=>{
  var srno = data.srno;
  client.get(data.token, function(err, reply){
    var userInfo = JSON.parse(reply)
    var query = {
      text : 'SELECT id,project_id from devices WHERE srno = $1 ',
      values : [srno]
    }
    db.query(query)
    .then(result=>{
      if(result.rows[0].project_id==userInfo.project_id){
        var logDelQuery = {
          text : 'DELETE from devices where srno = $1',
          values : [srno]
        }
        db.query(logDelQuery)
        .then(result=>{
          callback(false,'Deleted Successfully')
          // deletelogs(srno,userInfo.id)
        })
        .catch(err=>{
          callback('Error Ocurred on DELETE',false)
        })
      }
      else{
        callback('Permission Denied',false)
      }
    })
    .catch(err=>{
      callback('Error Ocurred',false)
    })
  })
}
module.exports = deleteDevice;
