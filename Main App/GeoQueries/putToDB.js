var NodeGeocoder = require('node-geocoder');
const db = require('../sequelize');

var options = {
  provider: 'google',
  apiKey: 'AIzaSyAgddpoPTOECk3ECGHtWy68JhkNTiLqZfk',
};

var geocoder = NodeGeocoder(options);

var geocode = (path)=>{
  return new Promise(function(resolve,reject){
    geocoder.reverse(path, function(err, res) {
      if(err){
        console.log(err)
      }
      else{
        // console.log(res[0].formattedAddress)
        resolve(res[0].formattedAddress)
      }
    })
  })
}

var insertquery = async (vin,resp,start,stop)=>{
  var path =[];
  var start_time = start
  var stop_time = stop
  var start_location;
  var stop_location;
  await resp.hits.hits.forEach(function(hit){
  path.push(hit._source.location)
})
start_location = await geocode(path[0])
// .catch(err=>{})
stop_location = await geocode(path[path.length-1])
console.log(start_location)
console.log(stop_location)
console.log(path)
var query={
 text : "insert into trip_details(vin,start_time,stop_time,path,start_location,stop_location) values($1,$2,$3,$4,$5,$6)",
 values : [vin,start_time,stop_time,JSON.stringify(path),start_location,stop_location]
}
db.query(query)
.then(result=>{
  console.log(result)
  return result
})
.catch(err=>{
  console.log(err)
  throw err
})
}

module.exports = {
  insertquery
}
