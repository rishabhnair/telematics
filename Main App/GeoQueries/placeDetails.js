var https = require('https');

var fetchDetails = function(place_id,callback){
  // console.log(place_id)
  var msg = [];
  for(let i=0; i<5; i++){
    let j=i

    var url = 'https://maps.googleapis.com/maps/api/place/details/json?placeid='+place_id[j]+'&fields=name,formatted_address,formatted_phone_number,geometry&key=AIzaSyAgddpoPTOECk3ECGHtWy68JhkNTiLqZfk'
    // console.log(url);
  https.get(url, function(response) {
    var body ='';
    response.on('data', function(chunk) {
      body += chunk;
    });
    response.on('end', function() {
      var details = {}
      // console.log(body)
      var places = JSON.parse(body);
      var locations = places.result;
      details['name']= locations.name;
      details['address']= locations.formatted_address;
      details['phone_no']= locations.formatted_phone_number;
      details['location']= locations.geometry.location;
      msg.push(details)
      if(msg.length==5)
        callback(msg);
    })

  }).on('error', function(err) {
    callback(err.message)
  });
  }
}

// fetchAddress([{place_id: 'ChIJ75bALLUZDTkR41hH3SahPKM'}, {place_id: 'ChIJJYzf3cwZDTkRO9DUVSZiZsk'}])
module.exports= {
  fetchDetails
}
