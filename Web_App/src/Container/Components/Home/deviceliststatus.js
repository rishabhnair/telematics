import React, { Component } from 'react';
import {Col, Row, Button, Modal, ModalBody, Label, Alert} from 'reactstrap'
import { Scrollbars } from 'react-custom-scrollbars';
import axios from 'axios' ;
import '../../Style/deviceliststatus.css'
import '../../Style/animationandtrasition.css'
import '../../Style/homemodel.css'

class DeviceListStatus extends Component{
    constructor(props){
        super(props);
        this.state = {
            filterDeviceStatus:'',
            selectedvalue:'srno',
            confirm:false,
            selecteddevice:'',
            selecteddevicesrno:'',
            deviceList:'',
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.toggleConfirm = this.toggleConfirm.bind(this);
        this.updateDeviceList = this.updateDeviceList.bind(this);
    }

    axiosFunc = () => {
        axios.defaults.headers.common['auth'] = sessionStorage.getItem('DontTouchMe');
        axios.get('http://8.9.36.13/factory/deviceinfo')
        .then(res => {
            if(res.status === 200){
                let deviceReports = [];
                    for(let i=0; i<res.data.devices.length; i++){
                            deviceReports.push({srno:res.data.devices[i].srno ? res.data.devices[i].srno : 'NA', vin:res.data.devices[i].vin ? res.data.devices[i].vin : 'NA', status:res.data.devices[i].status ? res.data.devices[i].status : 'NA'})
                    }
                this.setState({deviceList: deviceReports});
            }
            else
                console.log("Error in response")
        })
        .catch( er => {
                if(er.response){
                    if(er.response.status === 401)
                        console.log("Unauthorized or Invalid credential")
                    if(er.response.status === 404){
                        console.log("Not Found")
                    }
                }
                else{
                    console.log("Error in response");
                }
        }); 
    };
    
    componentDidMount() {
        this.axiosFunc();
        this.interval = setInterval(this.axiosFunc, 5000);
    }
    
    handleInputChange(e) {
        this.setState({filterDeviceStatus:e.target.value});
    }

    handleChange(e){
            this.setState({selectedvalue:e.target.value})
    }

    toggleConfirm(e, index, srno){
        this.setState({
            confirm: !this.state.confirm, 
            selecteddevice: index,
            selecteddevicesrno: srno
        })
    }

    updateDeviceList(e){
        let data = this.state.deviceList;
        data.splice(this.state.selecteddevice, 1);
        
        let srno ={
            srno  : this.state.selecteddevicesrno
        }

        axios.defaults.headers.common['auth'] = sessionStorage.getItem('DontTouchMe');
        axios.post('http://8.9.36.13/factory/deleteDevice', srno).then(res => {        
            if(res.status === 200)
                console.log("Successfully deleted");
            else     
                console.log("response code other than 200");
        }).catch( er => {
                if(er.response){
                    if(er.response.status === 401){
                        console.log("Unauthorized or Invalid credential");
                    }
                    if(er.response.status === 404){
                        console.log("Not Found")
                    }
                }
                else{
                    console.log("Error in response");
                }
        });
        this.setState({
            deviceList : data,
            confirm:false
        })
    }

    componentWillUnmount(){
        clearInterval(this.interval);
    }


    render(){
        return(
            <div>
                {/* <div className='modeldivupdatedevice'> <b>DEVICE STATUS</b> </div> */}
                <Modal size='sm' isOpen={this.state.confirm} centered={true} fade={false} toggle={this.toggleConfirm}>
                    <ModalBody className='updatedevice'>
                        <div style={{textAlign:'center', color:'white'}}>Are you sure to do this ?</div>
                        <div style={{textAlign:'center', marginTop:'15px'}} >
                            <Button className='hvr-border-fade-orange' style={{backgroundColor:'transparent', color:'#ff9800', border:'1px solid #ff9800', fontSize:'15px', margin:'15px'}} onClick={this.updateDeviceList}>Delete</Button>
                            <Button className='hvr-border-fade-orange' style={{backgroundColor:'transparent', color:'#ff9800', border:'1px solid #ff9800', fontSize:'15px', margin:'15px' }} onClick={this.toggleConfirm} >Cancel </Button>
                        </div>
                    </ModalBody>
                </Modal>

                <Row style={{marginTop:'12px'}} >

                    <Col xs='1' sm='1' md='1' lg='1'></Col>

                    <Col xs='12' sm='12' md='5' lg='5'>    
                            <Label className="onandfilter">Filter</Label>
                                <input type="text" name="filterDeviceStatus" id="filterDeviceStatus" value={this.state.filterDeviceStatus} onChange={this.handleInputChange} />                                   
                    </Col>

                    <Col xs='12' sm='12' md='4' lg='4' className='onsmalldevice'>        
                        <Label className="onandfilter"> On </Label>          
                        <select className='dropdownonhover' onChange={(e)=>{this.handleChange(e, 'selected')}}  style={{color:'#ff9800 ', width:'60%', height:'100%', background:'#232228', border:'1px solid #ff9800'}} >
                            <option  value='srno'>Device ID</option>
                            <option  value='status'>Status</option>
                            <option  value='vin'>VIN</option>
                        </select>
                    </Col>
                </Row>
                
                <div>
                <Scrollbars
                    onScroll={this.handleScroll}
                    onScrollFrame={this.handleScrollFrame}
                    onScrollStart={this.handleScrollStart}
                    onScrollStop={this.handleScrollStop}
                    onUpdate={this.handleUpdate}
                    renderView={this.renderView}
                    renderTrackHorizontal={this.renderTrackHorizontal}
                    renderTrackVertical={this.renderTrackVertical}
                    renderThumbHorizontal={this.renderThumbHorizontal}
                    autoHide
                    autoHideTimeout={1000}
                    autoHideDuration={200}
                    autoHeight
                    autoHeightMin={0}
                    autoHeightMax={350}
                    thumbMinSize={30}
                    universal={true}
                    renderThumbVertical={props => < div {...props} className="thumb-vertical track-vertical"/>}
                    //renderTrackVertical={props => < div {...props} className="track-vertical"/>}
                    >   
                    <Alert className='checkwidth' fade={false} style={{fontSize:'15px', backgroundColor:'#ff9800', borderColor:'#ff9800', borderRadius:'10px', marginTop:'15px'}}>
                        <Row className="rowdeviceStatus" style={{textAlign:'center'}}>
                            <Col xs='3' sm='3' md='3' lg='3'><b>Device ID</b></Col>
                            <Col xs='2' sm='2' md='2' lg='2'><b>VIN</b></Col>
                            <Col xs='2' sm='2' md='2' lg='2'><b>Active</b></Col>
                            <Col xs='3' sm='3' md='3' lg='3'><b>Status</b></Col>
                            <Col xs='2' sm='2' md='2' lg='2'><b>Delete</b></Col>
                        </Row>
                    </Alert>
                </Scrollbars>
                </div>

                <div>
                    <Scrollbars
                    onScroll={this.handleScroll}
                    onScrollFrame={this.handleScrollFrame}
                    onScrollStart={this.handleScrollStart}
                    onScrollStop={this.handleScrollStop}
                    onUpdate={this.handleUpdate}
                    renderView={this.renderView}
                    renderTrackHorizontal={this.renderTrackHorizontal}
                    renderTrackVertical={this.renderTrackVertical}
                    renderThumbHorizontal={this.renderThumbHorizontal}
                    autoHide
                    autoHideTimeout={1000}
                    autoHideDuration={200}
                    autoHeight
                    autoHeightMin={0}
                    autoHeightMax={350}
                    thumbMinSize={30}
                    universal={true}
                    renderThumbVertical={props => < div {...props} className="thumb-vertical track-vertical"/>}
                    //renderTrackVertical={props => < div {...props} className="track-vertical"/>}
                    >   
                        
                        {this.state.deviceList ? 
                            this.state.deviceList.filter( (item, index) => item[this.state.selectedvalue].toLowerCase().includes(this.state.filterDeviceStatus.toLowerCase()) ).map( (details, ind) =>  
                                <Alert className='checkwidth' key={ind} fade={false} style={{fontSize:'13px', backgroundColor:'#676A86', borderColor:'#676A86', borderRadius:'10px', color:'white'}}>  
                                    <Row className="rowdeviceStatusdata" style={{textAlign:'center'}}>
                                        <Col  xs='3' sm='3' md='3' lg='3'> {details.srno ? details.srno : 'Not Applicable'} </Col>
                                        <Col  xs='2' sm='2' md='2' lg='2'> {details.vin ? details.vin : 'Not Applicable'}  </Col>
                                        <Col  xs='2' sm='2' md='2' lg='2' style={{color: details.status === 'New' ? 'white' : 'white' }}> {details.status ? <div>{details.status === 'New' ? <i className="fa fa-times" aria-hidden="true"></i> : <i className="fa fa-check" aria-hidden="true"></i>}</div> : 'Not Applicable'} </Col>
                                        <Col  xs='3' sm='3' md='3' lg='3'> {details.status ? details.status : 'Not Applicable'} </Col>
                                        <Col  xs='2' sm='2' md='2' lg='2'> <Button className='hvr-border-fade' onClick={(e) => {this.toggleConfirm(e, ind, details.srno)} } style={{marginTop:'0px' ,color:'white', fontSize:'15px', borderRadius:'5px', borderColor:'transparent', background:'transparent'}}> <i className="fa fa-trash" aria-hidden="true"></i> </Button> </Col>
                                    </Row>
                                </Alert> 

                            )
                            :
                            'Please wait...'
                        }
                        
                    </Scrollbars>
                </div>                
                
                <div style={{textAlign:'center', marginTop:'10px'}}> 
                    <Button className='hvr-border-fade-orange' onClick={this.props.toggledevicelist} style={{marginTop:'5px', color:'#ff9800', fontSize:'15px', borderRadius:'5px', borderColor:'#ff9800', background:'transparent'}} outline >Close</Button>
                </div>
            </div>
        );
        
    }
}
export default DeviceListStatus;