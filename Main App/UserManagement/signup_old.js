var db = require('../sequelize.js')
var nodemailer = require('nodemailer')
var redis = require('redis')
var client = redis.createClient();
var gen = require('../tokenGen/gen.js')


var signup = (req,res,next) =>{
  console.log(req.body)
  var name = req.body.name || req.body.username
  var user = req.body.username
  // var pass = Buffer.from(req.body.password).toString('base64')
  // var company = req.body.company
  var email = ((req.body.username).split('@'))[1]
  var query = {
    text : 'select * from company where email = $2',
    values : [email]
  }
  db.query(query)
  .then(result=>{
    if(result.rowCount==0){
      // throw Error('Invalid Email')
      var insertQuery = {
        text : 'INSERT INTO users(name, username, password, timestamp, project_id) values($1, $2, $3, $4, $5, $6)',
        values  : [name,user,pass,'now',3]
      }
    }
    else {
      var insertQuery = {
        text : 'INSERT INTO users(name, username, password, company_name, timestamp, project_id) values($1, $2, $3, $4, $5, $6)',
        values  : [name,user,pass,company,'now',result.rows[0].project_id]
      }
      db.query(insertQuery)
      .then(success=>{
        var nodemailer = require('nodemailer')
        var transporter = nodemailer.createTransport({
          service: 'gmail',
          auth: {
            user: 'rishabhnair313@gmail.com',
            pass: gen.pass()
          }
        });

        var mailOptions = {
          from: 'rishabhnair313@gmail.com',
          to: user,
          subject: 'Authenticate',
          text: 'Click this link :'
        };

        gen.createTok(user,function(data){
          client.set(data,'OK')
          //client.expire('auth',300)
          mailOptions.text = 'Click this link to activate : http://localhost:3002/signup?token='+data;
          transporter.sendMail(mailOptions, function(error, info){
            if (error) {
              console.log(error);
              res.status(200).json({error: 'Email couldn\'t be sent'})
            } else {
              console.log('Email sent: ' + info.response);
              res.status(200).json({success: 'acivation mail has been sent'})
            }
          });
        })
      })
      .catch(err=>{
        console.log(err.message)
        res.status(500).json({error: 'an error has occured'})
      })
    }
  })
  .catch(err=>{
    console.log(err)
    res.status(500).json({error: err.message})
  })
}

module.exports = signup;
