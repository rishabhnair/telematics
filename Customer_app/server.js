var express = require ('express');
var bodyParser = require('body-parser');
var fs = require('fs');
var mqtt = require('mqtt')
const pg = require('pg')
var client = require('./sequelize')

const port = process.env.PORT|| 3000;

//var hbs = require('hbs');
//const ejs = require('ejs');
const customer_add = require('./customer_add.js');
var app = express();
app.use(express.static(__dirname + '/'));

app.get('/', (req, res) => {
	res.sendFile(__dirname + '/main.html');
 });

//app.set('view engine','ejs');
app.use(bodyParser.urlencoded({ extended: false }));

app.use(bodyParser.json());



app.route('/c_add')
.post(customer_add,(req, res, next)=>{
	//console.log(req.body);
	var selectQuery = {
		text : 'select sr_no,status from devices where sr_no = $1',
		values : [req.body.user_login]
	}
	client.query(selectQuery)
  	.then(result => {

  		//console.log(result.rows);
  		res.render('customer_devices.hbs',{Sr_Num : result.rows[0].sr_no, status : result.rows[0].status},'/register.html');

	})
	.catch(e => console.error(e.stack));

	res.sendFile(__dirname + '/register1.html') ;

 })

 app.route('/command')
 .get((req,res)=>{

 })
 .post(log_data,(req,res)=>{
	 if(req.body.msg=='start'){
		 start_log(req.body,function(){
			 console.log('callbacks here')
			 cmd_device(req.body)
		 })
	 }
	 if(req.body.msg=='stop'){
		 stop_log(req.body,function(){
			 console.log('callback here bro')
			 cmd_device(req.body)
		 })
	 }
	 if(req.body.vin_no){
		 console.log("vehicle number")
		 store_vehicle(req.body)
	 }
 })


app.listen(port, () =>{
	console.log(`server is up on ${port}`);
});
