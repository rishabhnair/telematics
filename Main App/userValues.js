var redis = require('redis')
var client = redis.createClient()
var db = require('./sequelize.js')
var userValues = (token,callback)=>{
  client.get(token,function(err,reply){
    if(err){
      callback(true,false)
    }
    else if(reply=='null' || reply == null){
      callback(true,false)
    }
    else {
      var info = JSON.parse(reply)
      // console.log({info})
      try {
        if(info.project_id==3){
          var query = {

            text : 'SELECT srno from devices where public_user = $1',
            values : [info.id]
          }
        }
        else {
          var query = {
            text : 'SELECT srno from devices where project_id = $1',
            values : [info.project_id]
          }
        }


      db.query(query)
      .then(res=>{
        // console.log(res.rows)
        var count = 0;
        var srnoArr = []
        if(res.rows.length>0){
          res.rows.forEach(function(val, i){
            count ++;
            srnoArr.push(val.srno)
            if(count == res.rows.length){
              info['srno'] = srnoArr;
              callback(false,info)
            }
          })
        }
        else {
          callback(false,info)
        }
      })
      .catch(err=>{
        callback(false,info)
      })
    } catch (e) {
      // console.log(e)
      callback(true,false)
    }
      // callback(false,)
    }
  })
}


module.exports = userValues;
