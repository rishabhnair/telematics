var mqtt = require('mqtt');
  var db = require('../sequelize.js')

function mqttListen(SrNo) {
  var client = mqtt.connect('tcp://149.28.63.75:1883');
  client.subscribe(`pub/${SrNo}`)
  client.on('message', function (topic, message) {
    console.log(message)
    var srno = topic.split('/');
    SrNo= srno[1];
    console.log(message.toString())
// console.log(message);
    // console.log(JSON.stringify(message.Status))
    if(JSON.parse(message.toString()).Status == 'Updated'){
      var query= {
        text: `Update "Report" set "Status"= 'Updated' where ("SrNo"= $1)`,
        values: [SrNo]
      }
      db.query(query)
      .then(result => {
        client.end()
        console.log("Device updated successfully and completely")
      })
      .catch(function(err){
        client.end()
        console.log('error in query')
        console.log(err);
      })
    }
  })
}
module.exports = mqttListen;
