import React, { Component } from 'react';
import {Row, Col, Button, ModalBody, Modal, Form, FormGroup} from 'reactstrap';
import Header from '../HeaderComponent';
import DeviceList from './devicelist';
import Switchbutton from './switchbutton';
import DashboardG from './dashboardG';
import DashboardT from './dashboardT';
import Chatroom from '../ChatBoard/Chatroom';
import DownloadLogs from './downloadlogs';
import axios from 'axios';
import '../../Style/home.css';
import '../../Style/animationandtrasition.css'
import '../../Style/login.css'
import '../../Style/header.css'
var base64 = require('base-64');
var utf8 = require('utf8');

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedState:'',
            selectedCity:'',
            selectedSubcity:'',
            Dashboard:'Tabular View',
            icon: 'fa fa-table',
            devicelistfilter:'',

            isDisabled: true,

            userdata:'',
            popoverOpen: false,
            //default_srno: sessionStorage.getItem('srno'),
            default_srno: base64.decode( utf8.decode( sessionStorage.getItem('srno') ) ),
            
            //default_srno: this.props.location.state.srno,
            
            deviceDetails:'',
            deviceListData:'',
            downloadlogs: false,
            switchatFooter:false,

            geofence: false,
            subunit: base64.decode( utf8.decode( sessionStorage.getItem('$$$') ) ),
            geodistance:'',

            //error handling
            successfully:'',
            invaliddetails:'',

            filteronCityStateSununit:'',
            checkforCityStateSubunit:''
        };
            this.handleChange = this.handleChange.bind(this);
            this.onselectStateCitySubcity = this.onselectStateCitySubcity.bind(this);
            this.onDeviceSelect = this.onDeviceSelect.bind(this);
            this.changeDashboardView = this.changeDashboardView.bind(this);
            this.signOut = this.signOut.bind(this);
            this.switchValue = this.switchValue.bind(this);
            this.toggle = this.toggle.bind(this);
            this.viewonmap = this.viewonmap.bind(this);
            this.toggleDownloadlogs = this.toggleDownloadlogs.bind(this);
            this.createGeoFence = this.createGeoFence.bind(this);
            this.submitGeofence = this.submitGeofence.bind(this);
    }

    signOut(e){
        sessionStorage.setItem('DontTouchMe', '');
        sessionStorage.setItem('srno', '');
        sessionStorage.setItem('kuchhbhi', '');
        sessionStorage.setItem('noti', '');
        sessionStorage.setItem('role', '');
        sessionStorage.setItem('$$$', '');
        sessionStorage.setItem('$$$1', '');
        sessionStorage.setItem('$$$2', '');
    }

    toggle() {
        this.setState({
          popoverOpen: !this.state.popoverOpen
        });
      }

    componentDidMount(){
    }

    componentWillReceiveProps(nextProps){
        if(nextProps !== this.props){
            this.props = nextProps
        }
    }

    onselectStateCitySubcity(e){
        //console.log(e.value, e.check);
        this.setState({filteronCityStateSununit: e.value, checkforCityStateSubunit: e.check})
    }

    handleChange(e){
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    changeDashboardView(){
        this.setState({Dashboard: this.state.Dashboard === 'Dashboard View' ? 'Tabular View' : 'Dashboard View', icon: this.state.icon ==='fa fa-tachometer' ? 'fa fa-table' :'fa fa-tachometer'})
    }

    onDeviceSelect(index, device){
        this.setState({default_srno: device.srno})
        // setInterval( () => {
        //     this.componentDidMount()
        // }, 1000) 
    }

    switchValue(switchValue){
        let msg = {
            msg: switchValue ? 'start' : 'stop',
            srno: this.state.default_srno
        }
        this.setState({switchatFooter:switchValue})

        axios.defaults.headers.common['auth'] = sessionStorage.getItem('DontTouchMe');
        axios.post('http://8.9.36.13/factory/command', msg).then(res => {  
            if(res.status === 200){
                console.log("Successfully start/stop");
            }
        }).catch( er => {
            if(er.response){
                if(er.response.status === 401){
                    console.log("Unauthorized or Invalid credential")
                }
                if(er.response.status === 404){
                    console.log("Not Found")
                }
            }
            else{
                console.log("Error in response");
            }
        });
    }

    toggleDownloadlogs(){
        this.setState({
            downloadlogs: !this.state.downloadlogs
        })
    }

    createGeoFence(){
        this.setState({
            geofence: !this.state.geofence,
            successfully:'',
            invaliddetails:''
        })
    }

    submitGeofence(e){
        e.preventDefault();

        this.setState({invaliddetails:'', successfully:''})

        if(Number(this.state.geodistance <5)){
            this.setState({invaliddetails:'Please enter minimum Geo-distance 5 Km'})
        }
        else{
                let geofence = {
                    geodist: Number(this.state.geodistance>=5 ? this.state.geodistance : 5)
                }
                axios.defaults.headers.common['auth'] = sessionStorage.getItem('DontTouchMe');
                axios.post('http://8.9.36.13/factory/geoconfig', geofence).then(res => {  
                    if(res.status === 200){
                        console.log("Successfully created geofence")
                        this.setState({successfully:'Successfully configured'})
                    }
                    else{
                        console.log("Response status code other than 200");
                    }
                }).catch( er => {
                    console.log(er.response)
                    if(er.response){
                        if(er.response.status === 401){
                            this.setState({invaliddetails:'Error, try again !'})
                        }
                        if(er.response.status === 404){
                            console.log("Not Found")
                        }
                    }
                    else{
                        console.log("Error in response");
                    }
                });
        }
    }

    viewonmap(e){
        this.props.history.push('/map')
    }

    componentWillUnmount(){
        //clearInterval(this.interval);
    }

    render(){
        let url = 'https://fft.truringsoftek.co.in/#/tplogin?token='+sessionStorage.getItem('DontTouchMe');  

        return(
            <div id='mainehome' className='container-fluid' style={{backgroundColor: '#232228', height:'90%'}}>

                <Modal size='lg' isOpen={this.state.downloadlogs} fade={false}  toggle={this.toggleDownloadlogs} onClosed={this.state.closeAll ? this.toggle : undefined}>
                    <ModalBody className='updatedevice'>
                        <DownloadLogs  srno={this.state.default_srno} toggleDownloadlogs={this.toggleDownloadlogs}  />
                    </ModalBody>
                </Modal>

                <Modal size='md' isOpen={this.state.geofence} fade={false}  toggle={this.createGeoFence} onClosed={this.state.closeAll ? this.toggle : undefined}>
                    <ModalBody className='updatedevice'>
                            <div> <b>Configure Geo-fence</b></div>

                            <Form onSubmit={this.submitGeofence} style={{marginTop:'25px', color:'#ff9800', fontSize:'15px'}}>              
                                <FormGroup>
                                        <input  type="text" value={this.state.subunit} disabled />   
                                </FormGroup>

                                <FormGroup style={{marginLeft:'22px'}}>
                                        <input type="number" name="geodistance" id="geodistance" placeholder="Min Geo-distance 5 Km" value={this.state.geodistance} onChange={this.handleChange} required />Km
                                </FormGroup>

                                {this.state.successfully ? <span style={{color:'green'}}>{this.state.successfully} </span> : '' }
                                {this.state.invaliddetails ? <span style={{color:'#ff0000'}}>{this.state.invaliddetails} </span> : '' }
                                <br/>
                                <Button className="hvr-border-fade-orange" type="submit" style={{color:'#ff9800', marginTop:'20px', fontSize:'15px', borderRadius:'5px', borderColor:'#ff9800', backgroundColor:'transparent'}}> Submit </Button>
                            </Form>
                        
                            <div style={{borderTop:"1px solid #616161", textAlign:'center', margin:'15px'}}> 
                                <Button className='hvr-border-fade-orange' onClick={this.createGeoFence} style={{marginTop:'15px', color:'#ff9800', fontSize:'15px', borderRadius:'5px', borderColor:'#ff9800', background:'transparent'}} outline >Close</Button>
                            </div>
                    </ModalBody>
                </Modal>

                <Header signout={this.signOut} checkforadmin={sessionStorage.getItem('role')} onselectStateCitySubcity={this.onselectStateCitySubcity}/>

                <Row className='mainrowcontainer'>
                    <Col xs='12' sm="12" md="3" lg="2" className='devicelist'>
                        <div className='searchondevice'>
                            <input style={{width:'100%'}} type='text' name='devicelistfilter' placeholder='Search here' value={this.state.devicelistfilter} onChange={this.handleChange} />
                        </div>
                        <DeviceList filteronCityStateSununit={this.state.filteronCityStateSununit} checkforCityStateSubunit={this.state.checkforCityStateSubunit} filtervalue={this.state.devicelistfilter} ondeviceselect={this.onDeviceSelect} /> 
                    </Col>

                    <Col xs='12' sm="12" md="9" lg="10" className='mainsecondcol'>
                        {this.state.Dashboard === 'Dashboard View' ?
                            <DashboardT srno={this.state.default_srno} />
                            :
                            <DashboardG srno={this.state.default_srno} />
                        }
                        {/* <Col xs='12'><br/><br/></Col> */}
                        <div>
                        <Row className='fourthrow'>
                            {/* <Col xs='1'>
                            </Col> */}

                            <Col xs='3' sm="1" md="1" lg="1" style={{marginTop:'20px', textAlign:'right'}}>
                                <Switchbutton switchvalue={this.switchValue} />
                            </Col>

                            <Col xs='3' sm="1" md="1" lg="1" style={{marginTop:'17px', textAlign:'left'}}>
                                Logging
                            </Col>
                            
                            <Col xs='6' sm="5" md="4" lg="2" className='borderrightBottom'>
                                <Button onClick={this.toggleDownloadlogs} disabled={this.state.switchatFooter} className='hvr-border-fade-orange' style={{fontSize:'13px' ,margin:'12px' ,color:'#ff9800', borderColor:'#ff9800', backgroundColor:'transparent'}} ><i className="fa fa-download" aria-hidden="true"></i>  Download Logs</Button>
                            </Col>
                            
                            <Col xs='6' sm="4" md="6" lg="2" className='borderrightBottom'>
                                <a href={url}  rel="noopener noreferrer" target="_blank"><Button className='hvr-border-fade-orange' style={{fontSize:'13px', margin:'12px', color:'#ff9800', borderColor:'#ff9800', backgroundColor:'transparent'}} ><i className="fa fa-map-marker" aria-hidden="true"></i>  View On Map</Button></a>
                            </Col>

                            <Col xs='6' sm="4" md="4" lg="2" className='borderrightBottom'>
                                <Button onClick={this.createGeoFence} className='hvr-border-fade-orange' style={{fontSize:'13px', margin:'12px', color:'#ff9800', borderColor:'#ff9800', backgroundColor:'transparent'}} ><i className="fa fa-map-marker" aria-hidden="true"></i>   Geo-fence</Button>
                            </Col>

                            <Col xs='6' sm="4" md="4" lg="2" className='borderrightBottom'>
                                <Button onClick={this.changeDashboardView} className='hvr-border-fade-orange' style={{fontSize:'13px', margin:'12px', color:'#ff9800', borderColor:'#ff9800', backgroundColor:'transparent'}}  > <i className={this.state.icon} aria-hidden="true"></i>    {this.state.Dashboard}</Button>
                            </Col>

                            <Col xs='6' sm="4" md="4" lg="2">
                                <Chatroom srno={this.state.default_srno} />
                            </Col>
                        </Row>
                        </div>
                    </Col>
                </Row> 
            </div>
        )
    }
}
export default Home;
