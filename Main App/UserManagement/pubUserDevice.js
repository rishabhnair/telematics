var db = require('../sequelize.js')

var pubUserDevice = (user_id,srno,callback)=>{
  console.log({user_id,srno})
  var query = {
    text : 'UPDATE devices set public_user = $1 where srno = $2',
    values : [user_id,srno]
  }
  db.query(query)
  .then(resp=>{
    console.log({resp})
    callback(false,true)
  })
  .catch(err=>{
    console.log({err})
    callback(true,false)
  })
}

module.exports = pubUserDevice
