const db = require('../sequelize');

var getdetails = function(vin,callback){
  var query={
    text : 'select start_time,stop_time,start_location,stop_location from trip_details where vin = $1',
    values : [vin]
  }
  db.query(query)
  .then(resp=>{
    callback(false,resp.rows)
 }, function(err) {
     console.trace(err.message);
     callback(err.message,false)
 })
 .catch(err=>{
   callback('err',false)
 })
}

module.exports = {
  getdetails
}
