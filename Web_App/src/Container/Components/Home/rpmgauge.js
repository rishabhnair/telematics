import React, {PureComponent} from 'react'
import CircularProgressbar from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';
import '../../Style/gaugecustom.css'

class Rpmgauge extends PureComponent{
    constructor(props){
        super(props);
        this.state ={
            percentage:''
        }
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.value !== this.props.value){
            this.setState({percentage: nextProps.value})
            //console.log("vvvvv", nextProps.value);
        }  
    }

    render(){
        const percentage = this.state.percentage ? this.state.percentage : 0;
        let strokecolor = '';

        if(this.state.percentage){
            if(this.state.percentage < 25 && this.state.percentage > 0){
                strokecolor = '#3399FF'
            }
            else if(this.state.percentage >= 25 && this.state.percentage < 50){
                strokecolor = '#00CC00'
            }
            else if(this.state.percentage >=50 && this.state.percentage < 75){
                strokecolor = '#ff9800'
            }
            else{
                strokecolor = '#ff0000'
            }
        }
      
        return(
            <div style={{ width: '50%', margin:'auto' }}>
                <CircularProgressbar 
                    strokeWidth={10}
                    styles={{
                            path: { stroke: strokecolor },
                            text: { fill: 'white', fontSize: '15px' },
                            trail: {stroke: 'transparent' },
                            background:{fill: 'transparent' }
                    }}
                    percentage={percentage}
                    // text={`${percentage}RPM`}
                    text={`${percentage}`}
                />
            </div>
        )
    }
}
export default Rpmgauge;