var db = require('../sequelize.js')
var client = require('./fftDB.js')

var mgrsignup = (data,callback)=>{
  var user_name = data.subunit_name;
  var full_name = data.subunit_name;
  var role_id = 2;
  var project_id = 2;
  var back_office_id = data.project_id;
  var query = {
    text : 'INSERT into user_master(user_name,full_name,role_id,project_id,back_office_id,modified_on,reporting_manager_id) VALUES($1,$2,$3,$4,$5,$6,(select user_id from user_master where project_id = $7 and reporting_manager_id is null))',
    values : [user_name,full_name,role_id,project_id,back_office_id,'now',project_id]
  }
  client.query(query)
  .then(resp=>{
    console.log('Successfull')
    callback(false,true)
  })
  .catch(err=>{
    console.log({err})
    callback(true,false)
  })
}

module.exports = mgrsignup;
