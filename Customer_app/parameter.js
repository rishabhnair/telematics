var elasticsearch = require('elasticsearch');
var PostFunc = require('./httpPost.js')
var client = new elasticsearch.Client({
   hosts: [ 'http://45.63.16.214:9200']
});
// client.ping({
//      requestTimeout: 30000,
//  }, function(error) {
//      if (error) {
//          console.error('elasticsearch cluster is down!');
//      } else {
//          console.log('Everything is ok');
//      }
//  });
client.search({
    index: 'testdata',
    type: 'parameter',
    body: {
        query: {
            match: {
                "vin": 'TELINK4G0000001'
            }
        }
    }
}).then(function(resp) {
    console.log("--- Response ---");
    console.log(resp);
    console.log("--- Hits ---");
    resp.hits.hits.forEach(function(hit){
      msg= hit._source.state;
      console.log(msg);
      PostFunc(msg)
    })
}, function(err) {
    console.trace(err.message);
});
