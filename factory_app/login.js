var express = require('express');
var bodyParser=require('body-parser');
var expressValidator = require('express-validator');
const hbs = require('hbs');
//var User = require('../models/User');
//const Sequelize = require('Sequelize');
//var databaseQueries=require('./databaseQueries.js')
var checkuser=require('./checkuser')
var signin = require('./log_user')
var device_add = require('./device_add')
const db = require('./sequelize')
var signup = require('./signup')
//var sequelize= new Sequelize('postgres://postgres@localhost:5433/truring');
var app=express();

app.use(expressValidator());
app.use(bodyParser.urlencoded({extended:false}))
app.use(express.static(__dirname + '/views'));
app.use(bodyParser.json());
var cookieParser = require('cookie-parser');
app.use(cookieParser());

app.set('view engine',hbs);

app.route('/home')
.get((req,res)=>{
  console.log(req.cookies)
  res.render('main.hbs')
})

app.route('/logout')
.get((req,res)=>{
  res.clearCookie('user')
  res.redirect('/login')
})



app.route('/login')
.get(checkuser,(req,res)=>{
  console.log('hello')
  res.render('login.hbs')
})
.post(signin,(req,res)=>{
  console.log(req.body)
})


app.route('/dashboard')
.get(checkuser,(req,res)=>{
  res.render('register.hbs',{device_hash:"Null"});
});

app.route('/listdevices')
.get(checkuser,(req,res)=>{
    var query= {
      text : 'select * from devices'
    }
    db.query(query)
    .then(result =>{
      console.log(result.rows)
      res.render('listdevices.hbs',{device_info : result.rows})
    })
    .catch(e => console.error(e));
});

app.route('/factory_add')
.post(device_add,(req, res, next)=>{
	console.log(req.body);
  console.log('hello')
	var selectQuery = {
		text : 'select * from devices where sr_no = $1',
		values : [req.body.sr_no]
	}
	db.query(selectQuery)
  	.then(result => {

  		//console.log(result.rows[0].device_id);
  		res.render('register.hbs',{device_info: result.rows})
	})
	.catch(e => console.error(e.stack));

	//res.redirect('/register.html')
	//res.send('hello')

 })


//Route for create users page
app.route('/register')
.get(checkuser,(req,res)=>{
	databaseQueries.getRegistrationData(res);  //prepare registration page
})
.post(checkuser,(req,res)=>{
   		functions.checkBody(req);
   var errors = req.validationErrors();

  if(errors){
res.send(errors) ;
console.log(errors)
  }
 databaseQueries.createUser(req,res);  //Create user function
});

//Route for view user
app.route('/deviceinfo*')
.get((req,res)=>{
  console.log(req.query)
	var query={
    text : 'select * from devices where sr_no = $1',
    values : [req.query.sr_no],
  }
  db.query(query)
  .then(result=>{
    console.log("device_info")
    console.log(result.rows[0]);
    res.render('register.hbs',{device_info : result.rows})
  });
})

//Route for users users page
app.route('/editUser')
.get((req,res)=>{
	databaseQueries.getUserData(req,res)    //get user's data
})
.post((req,res)=>{

   functions.checkBody(req);    //validation function
   var errors = req.validationErrors();

  if(errors){
res.send(errors) ;
console.log(errors)
  }

  databaseQueries.UpdateUser(req,res); //Update Function
  })

app.route('/signup')
  .get((req,res)=>{
    console.log("hit")
  	res.render('signup.hbs')  //prepare registration page
  })
  .post(signup,(req,res)=>{

  });

//Route for delete users



app.listen(3001)
