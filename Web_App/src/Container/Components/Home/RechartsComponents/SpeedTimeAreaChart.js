import React, {Component} from "react";
import { AreaChart, Area, XAxis, YAxis, Tooltip, ResponsiveContainer} from 'recharts';

class SpeedTimeAreaChart extends Component{
    constructor(props){
        super(props);
        this.state ={
          left:0,
          right:0
        }
    }

    render(){
      var speed = this.props.value;
      var TIME =  this.props.TIME;

      if(this.props.rpmgraphdata.length <5){
        if(speed){
          this.props.rpmgraphdata.push( {Speed: speed, Time: TIME} )
        }
            
      }
      else{
        if(speed){
          this.props.rpmgraphdata.shift();
          this.props.rpmgraphdata.push( {Speed: speed, Time: TIME} )
        }
      }
      
      let data = this.props.rpmgraphdata;
        return(
          <div style={{fontSize:'12px', fontWeight:'bold', textAlign:'center'}}>
            <div style={{color:'#ea80fc', marginTop:'5px', fontSize:'15px', fontWeight:'normal'}}>Speed   vs   Time</div>
            <ResponsiveContainer height={150}>
              <AreaChart data={data} margin={{ top: 5, right: 40, left: 0, bottom: 0 }}>
            
                <XAxis stroke='white' dataKey='Time'>
                    {/* <Label fill='#ea80fc' value="'Speed'   vs   'Time'" offset={-8} position="insideBottom" /> */}
                </XAxis>

                <YAxis stroke='white' dataKey='Speed' domain={[0, 'dataMax+30']} />
              
                <Tooltip/>

                <Area type='monotone' dataKey='Speed' stroke='#ff9800' fill='#ff9800' isAnimationActive={true} strokeWidth={2} activeDot={{r: 5}} />
              </AreaChart>
            </ResponsiveContainer>  
          </div>   
        );
    }
}
export default SpeedTimeAreaChart;