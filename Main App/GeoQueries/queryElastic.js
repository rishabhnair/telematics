const db = require('../sequelize');
const elastic = require('./elasticize');
// const redis = require('./redisize');
// var data = Date.now()
// console.log(data.getDate())
//var date = data.toString().split('.')
// console.log(data)

var getFromElastic = function(vin,size,callback){
         elastic.search({
             index: 'testdata',
             type: 'parameter',
             body: {
               "query": {
                  "match": {
                    "vin":vin
                  }
               },
                "sort": [
                 {
                   "DateTime": {
                   "order": "desc"
                   }
                 }
               ],
               "size": 1
             }
         }).then(function(resp) {
             var msg = [];
             // console.log("--- Response ---");
             // console.log(resp);
             // console.log("--- Hits ---");
             resp.hits.hits.forEach(function(hit){
               var p = {}
               p['vin'] = hit._source.vin
               date = hit._source.DateTime.replace(/[TZ]/g,'')
              // p['time']= new Date(date.substring(0,4),date.substring(4,6),date.substring(6,8),date.substring(8,10),date.substring(10,12),date.substring(12,14)).toString()
               p['time']= new Date(Date.now())
			   p['location'] = hit._source.location
               for(var i=1; i<=10; i++){
                 p[`p${i}`] = hit._source[`p${i} Data`].replace(/[a-zA-Z%-]/g,'')*1
               }
               msg.push(p)
             })
             // console.log(msg);
             callback(false,msg)
             // res.send(msg);
         }, function(err) {
             console.trace(err.message);
             callback(err.message,false)
         })
         .catch(err=>{
           callback('err',false)
         })
       }
module.exports = {
  getFromElastic
}
