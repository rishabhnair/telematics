var db = require('../sequelize.js')

var fetchCanLog = (srno,callback)=>{
  var query = {
    text : 'SELECT * from can_log where vin in (select vin from devices where srno = $1) order by log_start_tmp desc',
    values : [srno]
  }
  db.query(query)
  .then(res=>{
    if(res.rows.length>0){
      callback(false, res.rows)
    }
    else {
      callback('Empty List',false)
    }
  })
  .catch(err=>{
    callback("Error in Query",false)
  })
}

module.exports = fetchCanLog;
