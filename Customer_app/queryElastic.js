const db = require('./sequelize');
const elastic = require('./elasticize');
const redis = require('./redisize');
// var data = Date.now()
// console.log(data.getDate())
//var date = data.toString().split('.')
// console.log(data)
//
var getFromElastic = function(vin,size){
         elastic.search({
             index: 'testdata',
             type: 'parameter',
             body: {
               "query": {
                  "match": {
                    "vin":vin
                  }
               },
                "sort": [
                 {
                   "DateTime": {
                   "order": "desc"
                   }
                 }
               ],
               "size": size
             }
         }).then(function(resp) {
             var msg = [];
             console.log("--- Response ---");
             console.log(resp);
             console.log("--- Hits ---");
             resp.hits.hits.forEach(function(hit){
               var p = {}
               p['vin'] = hit._source.vin
               p['time'] = hit._source.DateTime
               p['location'] = hit._source.location
               for(var i=1; i<=10; i++){
                 p[`p${i}`] = hit._source[`p${i} Data`]
               }
               msg.push(p)
             })
             console.log(msg);
             // res.send(msg);
         }, function(err) {
             console.trace(err.message);
         });
       }
module.exports = {
  getFromElastic
}
