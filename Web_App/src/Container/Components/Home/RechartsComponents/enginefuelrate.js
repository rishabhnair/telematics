import React, {Component} from "react";
import {XAxis, YAxis, Tooltip, CartesianGrid, ResponsiveContainer, Line, LineChart } from 'recharts';

class Enginefuelrate extends Component {
  constructor(props) {
      super(props);
      this.state = {

      }
    }
    
  componentWillReceiveProps(nextProps){
      if(nextProps !== this.props){
          this.props = nextProps
      }
  }

    render() {
        var rpm = this.props.value;
        //var speed = this.props.value1;
        var TIME =  this.props.TIME;
  
        if(this.props.rpmgraphdata.length < 5){
          if(rpm){
            this.props.rpmgraphdata.push( {'Engine fuel rate': rpm, Time: TIME} )
          }     
        }
        else{
          if(rpm){
            this.props.rpmgraphdata.shift();
            this.props.rpmgraphdata.push( {'Engine fuel rate': rpm, Time: TIME} )
          }
        }
        
        let data = this.props.rpmgraphdata; 
        return (
          <div style={{textAlign:'center', fontSize:'12px', fontWeight:'bold', marginBottom:'50px'}}> 
            <div style={{color:'#ea80fc', marginTop:'5px', fontSize:'15px', fontWeight:'normal'}}>Engine Fuel Rate vs Time</div>  
            <ResponsiveContainer height={200}>                
              <LineChart data={data} margin={{ top: 5, right: 30, left: 0, bottom: 0 }} >   
                  <XAxis stroke='white' dataKey='Time' fontSize='12px' >
                    {/* <Label fill='#ea80fc' value=" 'Engine Fuel Rate' vs 'Time' " offset={-10} position="insideBottom" /> */}
                  </XAxis> 
                  <YAxis stroke='white' dataKey='Engine fuel rate' fontSize='12px' domain={[0, 'dataMax+20']} />
                  <CartesianGrid strokeDasharray="2 2"/>
                  <Tooltip />                         
                  <Line type='monotone' strokeWidth={2} dataKey='Engine fuel rate' activeDot={{r: 5}} stroke='#ff9800'  animationEasing={'linear'} animationDuration={1000}/> 
              </LineChart> 
            </ResponsiveContainer>
          </div>
        );
    }
}
export default Enginefuelrate;
