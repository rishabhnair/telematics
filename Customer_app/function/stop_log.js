var mqtt = require('mqtt')
var db = require('../sequelize')
//var vin_hash = require('./vin_hash')
var stop_log = (data,callback) => {
  var now = new Date();
  var client  = mqtt.connect('tcp://149.28.63.75:1883');
  var query = {
    text : 'select * from (select * from log_table join  devices on log_table.device_id = devices.did) as foo where foo.sr_no = $1 order by foo.start_tmp desc limit 1;',
    values : [data.sr_no],
  }
  db.query(query)
  .then(result => {
    var query = {
      text : 'update can_log set log_stop_tmp = $1 where vin=$2 and log_stop_tmp is null',
      values : [now,result.rows[0].vin],
    }//conencted check
    db.query(query)
    .then(result1 =>{
      const query2 =
            {
              text: 'update devices set status = $1 where sr_no = $2',
              values: ['connected',result.rows[0].device_id],
            }
            db.query(query2)
            .then(result=>{
              //var topic = 'param/'+data.sr_no;
              var topic = 'sub/'+data.sr_no;
              client.on('connect',function(){
                client.publish(topic,':001442010301251E01261E0130ABCD');
              })
              //vin_hash(data.sr_no,0)
              callback();
            }).catch(e=> console.log(e));
    }).catch(e=> console.log(e));
  }).catch(e=> console.log(e));
}

module.exports = stop_log;
