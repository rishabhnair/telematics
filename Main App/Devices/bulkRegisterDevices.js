var registerDevice = require('./registerDevice.js')
var fs = require('fs')
var count = 0;
var bulkRegisterDevices =  (file,token,callback) => {
  var data = file.data.toString()
  var dataBlocks = data.split('\n')
  console.log(dataBlocks)
  loop(dataBlocks,token).then(res=>{
    console.log(res)
    callback(false,res)
  })
  .catch(err=>{
    callback(err,false)
  })
}

var loop = (dataBlocks,token) =>{

  return new Promise(function(resolve,reject){
    var count = 0;
    var reply = {}
    console.log(dataBlocks)
    dataBlocks.forEach(async function(val, i){
      console.log({val, i})
      var info = await {
        srno : val,
        token : token
      }
      registerDevice(info)
      .then(res=>{
        count++;
        console.log({res})
        reply[info.srno] = 'Registered'
        if(count==(dataBlocks.length)){
          resolve(reply)
          console.log({count})
        }
      })
      .catch(err=>{
        count++
        console.log(err)
        reply[info.srno] = err.error
        if(count==(dataBlocks.length)){
          resolve(reply)
          console.log({count})
        }
      })
    })
  })
}
module.exports = bulkRegisterDevices;
