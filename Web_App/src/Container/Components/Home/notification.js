import React, { Component } from 'react';
import {Alert ,Col, Row, Button, Label} from 'reactstrap'
import { Scrollbars } from 'react-custom-scrollbars';
import DateFormat from 'dateformat' ;
import '../../Style/homemodel.css'

class Notification extends Component{
    constructor(props){
        super(props);
        this.state = {
            filterDeviceStatus:'',
            selectedvalue:'srno',
            notificationlist:this.props.notificationlist
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
    }

    componentWillReceiveProps(nextProps){
        if(nextProps !== this.props){
            this.props = nextProps;
        }
    }

    handleInputChange(e) {
        this.setState({filterDeviceStatus:e.target.value});
    }

    handleChange(e){
        this.setState({selectedvalue:e.target.value})
    }

    render(){
        return(
            <div>
                <Row style={{marginTop:'12px'}} >
                    <Col xs='1' sm='1' md='1' lg='1'></Col>

                    <Col xs='12' sm='12' md='5' lg='5'>    
                            <Label className="onandfilter"> Filter </Label>
                            <input type="text" name="filterDeviceStatus" id="filterDeviceStatus" value={this.state.filterDeviceStatus} onChange={this.handleInputChange} />                                
                    </Col>

                    <Col xs='1' sm='1' md='0' lg='0'></Col>

                    <Col xs='12' sm='12' md='4' lg='4' className='onsmalldevice'>        
                        
                        <Label className="onandfilter"> On </Label>
                        <select onChange={(e)=>{this.handleChange(e, 'selected')}}  style={{color:'#ff9800 ', width:'60%', height:'100%', background:'#232228', border:'1px solid #ff9800'}} >
                            <option  value='srno'>Device ID</option>
                            <option  value='alertCode'>Alert Message</option>
                        </select>
                    </Col>
                </Row>
                
                <div>
                <Scrollbars
                    onScroll={this.handleScroll}
                    onScrollFrame={this.handleScrollFrame}
                    onScrollStart={this.handleScrollStart}
                    onScrollStop={this.handleScrollStop}
                    onUpdate={this.handleUpdate}
                    renderView={this.renderView}
                    renderTrackHorizontal={this.renderTrackHorizontal}
                    renderTrackVertical={this.renderTrackVertical}
                    renderThumbHorizontal={this.renderThumbHorizontal}
                    autoHide
                    autoHideTimeout={1000}
                    autoHideDuration={200}
                    autoHeight
                    autoHeightMin={0}
                    autoHeightMax={350}
                    thumbMinSize={30}
                    universal={true}
                    renderThumbVertical={props => < div {...props} className="thumb-vertical track-vertical"/>}
                    //renderTrackVertical={props => < div {...props} className="track-vertical"/>}
                    >   
                        <Alert className='checkwidth' fade={false} style={{fontSize:'15px', backgroundColor:'#ff9800', borderColor:'#ff9800', borderRadius:'10px', marginTop:'15px'}}>
                            <Row className="rowdeviceStatus" style={{textAlign:'center'}}>
                                <Col xs='6' sm='6' md='6' lg='6'><b>Device ID</b></Col>
                                <Col xs='6' sm='6' md='6' lg='6'><b>Alert Message</b></Col>
                            </Row>
                        </Alert>

                    </Scrollbars>
                </div>

                <div>
                    <Scrollbars
                    onScroll={this.handleScroll}
                    onScrollFrame={this.handleScrollFrame}
                    onScrollStart={this.handleScrollStart}
                    onScrollStop={this.handleScrollStop}
                    onUpdate={this.handleUpdate}
                    renderView={this.renderView}
                    renderTrackHorizontal={this.renderTrackHorizontal}
                    renderTrackVertical={this.renderTrackVertical}
                    renderThumbHorizontal={this.renderThumbHorizontal}
                    autoHide
                    autoHideTimeout={1000}
                    autoHideDuration={200}
                    autoHeight
                    autoHeightMin={0}
                    autoHeightMax={300}
                    thumbMinSize={30}
                    universal={true}
                    renderThumbVertical={props => < div {...props} className="thumb-vertical track-vertical"/>}
                    //renderTrackVertical={props => < div {...props} className="track-vertical"/>}
                    >   
                    {this.props.notificationlist ? 
                        this.props.notificationlist.filter((item,index) => item[this.state.selectedvalue].toLowerCase().includes(this.state.filterDeviceStatus.toLowerCase())).map( (details, i) =>  
                            <Alert className='checkwidth' key={i} fade={false} style={{fontSize:'13px' ,backgroundColor:'#676A86', borderColor:'#676A86', marginTop:"0px", borderRadius:'10px', color:'white'}}>
                                <Row className="rowdeviceStatusdata">
                                    <Col xs='6' sm='6' md='6' lg='6' style={{textAlign:'center'}}> {details.srno ? details.srno : 'Not Applicable'} </Col>
                                    <Col xs='6' sm='6' md='6' lg='6'> {details.alertCode ? details.alertCode : 'Not Applicable'}<br/>{details.time !=='NA' ? <div style={{fontSize:'10px', textAlign:'right'}}>{DateFormat(new Date( Number(details.time) ),"h:MM:ss dd-mm-yyyy")}</div> : <div style={{textAlign:'right' ,fontSize:'10px'}}>NA</div> } </Col>
                                </Row>
                            </Alert> 
                        )
                        :
                        'Please wait...'
                    }
                    </Scrollbars>
                </div>                
                
                <div style={{textAlign:'center', marginTop:'10px'}}> 
                    <Button className='hvr-border-fade-orange' onClick={this.props.notificationonclose} style={{marginTop:'5px', color:'#ff9800', fontSize:'15px', borderRadius:'5px', border:'1px solid #ff9800', background:'transparent'}} outline >Close</Button>
                </div>
            </div>
        );
    }
}
export default Notification;