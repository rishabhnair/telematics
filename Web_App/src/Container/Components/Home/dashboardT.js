import React, {Component} from 'react'
import {Row, Col} from 'reactstrap'
import '../../Style/home.css'
import '../../Style/dashboardT.css'
import SwitchForTable from './switchForTable'
import { Table } from 'reactstrap';
import axios from 'axios'
import DateFormat from 'dateformat' ;

class DashboardT extends Component{
    constructor(props){
        super(props);
        this.state = {
            srno: this.props.srno,
            deviceDetails:'',
            devicedetailsList: [],
            NA: 'NA',
            autoRefresh: true
        }
        this.switchValue = this.switchValue.bind(this);
    }

    axiosFunc = () => {
        let data = {
            vin : this.state.srno
        }
        axios.defaults.headers.common['auth'] = sessionStorage.getItem('DontTouchMe');
        axios.post('http://8.9.36.13/factory/timeseries', data).then(res => {
            if(res.status === 200){
                this.setState({deviceDetails: res.data.data[0]});
            }
            else
                console.log("Response code other than 200")
        }).catch( er => {
            if(er.response){
                if(er.response.status === 401)
                    console.log("Unauthorized or Invalid credential")
                if(er.response.status === 404){
                    console.log("Not Found")
                }
            }
            else{
                console.log("Error in response");
            }
        });
    };

    componentDidMount(){
       this.axiosFunc();
       this.interval = setInterval(this.axiosFunc, 5000);
    }

    componentWillReceiveProps(nextProps){
        if(nextProps !== this.props){
            this.props = nextProps
            this.setState({srno: nextProps.srno})
        }
    }

    switchValue(switchValue){
        this.setState({autoRefresh:switchValue})
    }

    componentWillUnmount(){
       clearInterval(this.interval);
    }

    render(){ 
        //console.log(this.state.devicedetailsList)
        if(this.state.devicedetailsList.length <=5){
            if(this.state.autoRefresh){
                if(this.state.deviceDetails)
                this.state.devicedetailsList.unshift(this.state.deviceDetails)
            }
        }
        else{
            if(this.state.deviceDetails){
                if(this.state.autoRefresh){
                    this.state.devicedetailsList.pop();
                    this.state.devicedetailsList.unshift(this.state.deviceDetails)
                }
            }
        }
        return(
            <div className='dashboardT'>
                <Row className='dashboardT_firstrow'>
                    <Col xs='12' sm='12' md='4' lg='4'>
                        <Row>
                            <Col xs='4' sm="4" md="4" lg="4" className="homeh">VIN</Col>
                            <Col xs='8' sm="8" md="8" lg="8" className="homeh1">{this.state.deviceDetails ? this.state.deviceDetails.vin ? this.state.deviceDetails.vin : 'Not Applicable' : 'Not Applicable'} </Col>
                        </Row>
                    </Col>

                    <Col xs='12' sm='12' md='4' lg='4'>
                        <Row>
                            <Col xs='4' sm="4" md="4" lg="4" className="homeh"> Fuel Type</Col>
                            <Col xs='8' sm="8" md="8" lg="8" className="homeh1">{this.state.deviceDetails ? this.state.deviceDetails.fueltype ? this.state.deviceDetails.fueltype : 'Not Applicable' : 'Not Applicable'} </Col>
                        </Row>
                    </Col>

                    <Col xs='12' sm='12' md='4' lg='4'>
                        <Row>
                            <Col xs='6' sm="6" md="6" lg="6" className="homeh">Auto Refresh</Col>
                            <Col xs='6' sm="6" md="6" lg="6" className="homeh1"> <SwitchForTable switchvalue={this.switchValue} /> </Col>
                        </Row>
                    </Col>
                </Row>

                <Table responsive>
                    <thead style={{color:'#ff9800'}}>
                        <tr>
                            <th> Timestamp </th>
                            <th> Speed </th>
                            <th> RPM </th>
                            <th> Fuel Pressure </th>
                            <th> Exhaust Pressure </th>
                            <th> Average RPM </th>
                            <th> Engine Load </th>
                            <th> Coolant Temperature </th>
                            <th> Intake Air Temperature </th>
                            <th> Engine Temperature </th>
                        </tr>
                    </thead>

                    {this.state.devicedetailsList.length !== 0 ? 
                        <tbody style={{color:'#bdbdbd'}}>
                            {this.state.devicedetailsList.map((obj, index) =>  
                                <tr key={index}>
                                    <td> {DateFormat(obj.time,"yyyy-mm-dd h:MM:ss") ? DateFormat(obj.time,"yyyy-mm-dd h:MM:ss") : this.state.NA }</td>
                                    <td> {obj.p2 ===0 || obj.p2 !==undefined ? obj.p2 : this.state.NA} </td>
                                    <td> {obj.p3 ===0 || obj.p3 !==undefined ? obj.p3 : this.state.NA} </td>
                                    <td> {obj.p5 ===0 || obj.p5 !==undefined ? obj.p5 : this.state.NA} </td>
                                    <td> {obj.p8 ===0 || obj.p8 !==undefined ? obj.p8 : this.state.NA} </td>
                                    <td> {obj.p9 ===0 || obj.p9 !==undefined ? obj.p9 : this.state.NA} </td>
                                    <td> {obj.p2 ===0 || obj.p2 !==undefined ? obj.p2 : this.state.NA} </td>
                                    <td> {obj.p3 ===0 || obj.p3 !==undefined ? obj.p3 : this.state.NA} </td>
                                    <td> {obj.p5 ===0 || obj.p5 !==undefined ? obj.p5 : this.state.NA} </td>
                                    <td> {obj.p8 ===0 || obj.p8 !==undefined ? obj.p8 : this.state.NA} </td>
                                </tr>
                            )}
                        </tbody>
                            :
                        <tbody>
                            <tr>
                                <td> Please wait... </td>
                            </tr>
                        </tbody>
                    }
                </Table>
            </div>
        )
    }
}
export default DashboardT;

// <Table responsive>
// <thead style={{color:'#ff9800'}}>
   
//         {this.props.devicedetails.map( (element, ind) => 
//         <tr key={ind}>  
//             {Object.keys(element).map( (val, ind) => 
//             {
//                 if(val !== 'location' && val !== 'vin'){
//                     return(
//                         <th key={ind}>{val}  </th>
//                     )
//                 }                      
//                 //console.log({val, ind}, element[val])                               
//             }
//             )}
//         </tr>
//         )}
// </thead>

// <tbody style={{color:'#bdbdbd'}}>
//     {this.props.devicedetailsList.map( (obj, index) =>  
//     obj.length !==0 ?
//     <tr key={index}>
//         {/* <td> {obj.time} </td> */}
//         {Object.keys(obj).map( (val, ind) => 
//             {
//                 if(val !== 'location' && val !== 'vin'){
//                     return(
//                         <th key={ind}>{ obj[val]}  </th>
//                     )
//                 }                      
//                 //console.log({val, ind}, element[val])                               
//             }
//             )}
//     </tr>
//     :
//     ''
//     ) }
// </tbody>
// </Table>