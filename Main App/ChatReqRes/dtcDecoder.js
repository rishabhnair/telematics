var hexToBinary = require('hex-to-binary');

var dtcDecoder = (msg,callback)=>{
  // callback()
	callback(false,msg)
	return;

	var test = msg.replace(/[\s0-9a-fA-F]/g,'')
	msg = msg.replace(/\s/g,'')
	if(test.length>0){
		callback("error",false)
	}
	else{

	var fc = {
		'00' : 'P',
		'01' : 'C',
		'10' : 'B',
		'11' : 'U'
	}
	var sc = {
		'00' : '0',
		'01' : '1',
		'10' : '2',
		'11' : '3'
	}
	var output = []
	var index = 0;
	var x = 6;

	try {
		while(x<msg.length){
			var first = hexToBinary(msg.slice(x,x+1))
			var input1 = fc[first.slice(0,2)]
			var input2 = sc[first.slice(2,4)]
			var input3 = msg[x+1]
			var input4 = msg.slice(x+2,x+4)
			output[index] = (input1+input2+input3+input4)
			index = index + 1
			x = x+8
			}
	} catch (e) {
		callback(e,false)
	} finally {

	}

	}
	if(x>=msg.length){
		callback(false,output)
	}

}

module.exports = dtcDecoder
