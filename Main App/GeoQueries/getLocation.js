const db = require('../sequelize');
const elastic = require('./elasticize');
// const redis = require('./redisize');
var client = require('./fftDB.js')

var getFromElastic = function(callback){
            elastic.search({
                index: 'testdata',
                type: 'parameter',
                body: {
                  "query": {
                     "bool": {
                      "filter": [
                        { "range": { "DateTime": { "gte": "20150101T000000Z", "lte": "now/s" }}}
                      ]
                     }
                  },
                  "sort": [
                   {
                     "DateTime": {
                     "order": "desc"
                     }
                   }
                 ],
                 "size": 250
                }
            }).then(function(resp) {
             var msg = [];
             var visited= new Set();
             // console.log("--- Response ---");
             // console.log(resp);
             // console.log("--- Hits ---");
             resp.hits.hits.forEach(function(hit){
               if(visited.has(hit._source.vin)){
                 // console.log(hit._source.vin)
               }
               else {
                 visited.add(hit._source.vin)
                 msg.push({
                   'time':hit._source.DateTime,
                   'location':hit._source.location,
                   'vin': hit._source.vin
                 })
               }
             })
             // console.log(msg)
             for(let i=0; i<msg.length; i++){
               var now = new Date()
               // console.log(`msg length: ${msg.length}`)
               let data = msg[i]

               // console.log(`vin details: ${data.vin}`)
               // console.log(`time:${data.time}`)
               var query={
                 text : 'update devices set lat = $1, lon = $2, time = $3 where srno= $4',
                 values : [data.location.lat,data.location.lon,'now',data.vin]
                 // text : 'select * from devices where srno = $1',
                 // values : [data.vin]
               }
               db.query(query)
               .then(result=>{
                 // console.log(data.vin)
                 // console.log(result.rows)
                 var now = new Date().toUTCString()
                 // console.log('DATE TIME')
                 // console.log(now)
                 // console.log(msg)
                 var fftQuery = {
                   text : 'INSERT INTO route_location_tracking (latitude, longitude, user_id, mobile_time) VALUES($1,$2,(select user_id from user_master where full_name = $3),$4) ON CONFLICT(user_id) do UPDATE set latitude=$1,longitude=$2,mobile_time=$4',
                   values : [data.location.lat,data.location.lon,data.vin,'now']
                 }
                 client.query(fftQuery)
                 .then(resultNew=>{
                   // console.log('successfully saved to fft')
                   // console.log(resultNew)
                 })
                 .catch(err=>{
                   console.log({err})
                 })
                 // console.log('DB updated');
               })
               .catch(err=>{
                 console.log({err})
                 callback('err',false)
               })
         } callback(false,true)
       }, function(err) {
           console.trace(err.message);
           callback(err.message,false)
       })
     }


module.exports = getFromElastic;
