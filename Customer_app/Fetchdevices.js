const db = require('./sequelize');
const elastic = require('./elasticize');
const redis = require('./redisize');

var getFromElastic = function(vin,size){
         elastic.search({
             index: 'testdata',
             type: 'parameter',
             body: {
               "query": {
                 "bool" : {
                   "must" : {
                     "match_all" : {}
                   },
                   "filter" : {
                     "geo_distance" : {
                       "distance" : "200km",
                       "location" : {
                         "lat" : 28,
                         "lon" : 77
                       }
                     }
                   }
                 }
               }
             }
         }).then(function(resp) {
             var msg = [];
             console.log("--- Response ---");
             console.log(resp);
             console.log("--- Hits ---");
             resp.hits.hits.forEach(function(hit){
               var p = {}
               p['vin'] = hit._source.vin
               msg.push(p)
             })
             console.log(msg);
             // res.send(msg);
         }, function(err) {
             console.trace(err.message);
         });
       }
module.exports = {
  getFromElastic
}
