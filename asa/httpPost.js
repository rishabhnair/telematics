var http = require("http");

var express = require('express');

var app = express();

// app.listen(3000)

//var alert = require('./getAlerts.js');

var PostFunc = (msg)=>{

  var options = {

  hostname: 'localhost',

  port: 5002,

  path: '/alerts',

  method: 'POST',

  headers: {

      'Content-Type': 'application/json',

  }

};

var req = http.request(options, function(res) {

  console.log('Status: ' + res.statusCode);

  console.log('Headers: ' + JSON.stringify(res.headers));

  res.setEncoding('utf8');

  res.on('data', function (body) {

    console.log('Body: ' + body);

  });

});

req.on('error', function(e) {

  console.log('problem with request: ' + e.message);

});

 

req.write(JSON.stringify(msg));

req.end();

}

module.exports = PostFunc;