var db = require('./sequelize.js');
var dateTime = require('node-datetime');

var updatedevice = require('./UpdateDevice.js')
var checkjson = function(){
  var json = {
    'Username': 'Hardik',
    'SrNo': 'TELINK4G0000001',
    'ECU': 'DCU',
    'UpdateToVersion': 'Version3'
  }
  var query = {
    text: `select "ECU", "Available_Version" from "sftpMetadata" where("ECU"= $1 AND "Available_Version"= $2)`,
    values: [json.ECU,json.UpdateToVersion],
  }
  db.query(query)
  .then(result => {
    console.log(result.rows)
    if(result.rows.length!=0){
      console.log('Values are correct, Updating device');
      var dt = dateTime.create();
      var formatted = dt.format('Y-m-d H:M:S');
      var query1 = {
        text: `insert into "Report" ("DateTime","Username","SrNo", "ECU","UpdateToVersion","Status") values ($1, $2, $3, $4, $5, $6)`,
        values: [formatted,json.Username,json.SrNo,json.ECU,json.UpdateToVersion,"Updating"]
      }
      db.query(query1)
      .then(result =>{
        updatedevice.sendsftpPath(json,formatted);
        res.send("Uploaded Successfully").end();
      })
      .catch(function(err){
        console.log('error in query')
        console.log(err);
      })
    }
    else{
      console.log('invalid values')
    }
  })
  .catch(function(err){
    console.log('error in query')
    console.log(err);
  })
}
// checkjson()
module.exports = {
  checkjson
}
