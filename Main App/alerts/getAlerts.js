var mqtt = require('mqtt')
var db = require('../sequelize.js')
var HashMap = require('hashmap')
var map = new HashMap();
var redis = require('redis')
var red = redis.createClient()
var userValues = require('../userValues.js')
var mailer = require('./nodemailer.js')

// var PostFunc = require('./httpPost.js')
var alertjson = function() {
  var client  = mqtt.connect('tcp://149.28.63.75:1883');
  client.subscribe('alerts/#');
   client.on('message', function (topic, message) {

    // console.log("messageeeee "+message);
    var user = 'rishabh.nair@truringsoftek.com'
     var srno = topic.split('/');
     if(message.includes(' ')){
       message = message.toString()
       var msgt = message+'|'+Date.now()
       red.hset(srno[1],message,msgt)
       if(message.toLowerCase().includes('critical')){
         var ms = 'Critical : '+message;
         mailer(user,ms,function(err,reply){
           // console.log({err})
           if(err){
             // console.log({err})
           }
           else {
             // console.log(reply)
           }
         })
       }
     }
     else{
       var query = {
         text : 'SELECT "AlertMessage" FROM alerts WHERE "AlertCode" = $1',
         values : [message]
       }
       db.query(query)
       .then(result=>{
         // console.log( result.rows[0]);

           var alert_msg = result.rows[0].AlertMessage+'|'+Date.now()
           message = message.toString()
           var dt = [];
           red.hset(srno[1],message,alert_msg)
           client.end()
           // PostFunc(msg)
           // console.log(alert_msg);
       })
       .catch(e=>{
         // console.log(e)
         client.end()
       })
       // PostFunc(message)
     }
   });
  }

  var fetchAlerts =  (token,callback)=>{
    userValues(token,function(err,reply){
      var count = 0;
      // console.log(reply)
      if(err){
        // console.log({err})
        callback(true,false)
      }
      else {
          if(reply.project_id!=3){
            var result = [];
            reply.srno.forEach(function(val, i){
              count ++;
              red.hgetall(val,function(err,keyval){
                if(err){
                  // console.log({err})
                }
                else {
                  for(key in keyval){
                    var code = keyval[key].split('|')
                    var alerts = { "srno" : val, "alertCode": code[0], "time" : code[1] };
                    // console.log({alerts})
                    result.push(alerts)
                  }
                  if(count == reply.srno.length){
                    setTimeout(function () {

                      callback(false,result)
                    }, 100);
                  }

                }
              })
              // red.llen(val,function(err,len){
              //   if(len>0){
              //     red.lrange(val,0,-1,function(err,alerts){
              //       alerts.forEach(function(msg, j){
              //         // count ++;
              //         var max = reply.srno.length
              //         var alert = { "srno" : val, "alertCode": msg };
              //         result.push(alert)
              //         // console.log({count,j})
              //         if(count ==  reply.srno.length && j == alerts.length - 1){
              //           // console.log({result})
              //           callback(false,result)
              //
              //         }
              //       })
              //     })
              //   }
              // })
            })
          }
          else {
            var result = [];
            var public_user = JSON.parse(reply).user_id
            var query = {
              text : 'SELECT srno from devices where public_user=$1',
              values : [public_user]
            }
            db.query(query)
            .then(res=>{
              res.rows.forEach(function(val, i){
                count ++;

                if(count == reply.srno.length){
                  callback(false,result)
                }
              })
              // console.log(res.rows)
            })
            .catch(err=>{
              callback(err,false)
            })
          }
      }

  });
}
// fetchAlerts()
// alertjson();

module.exports = { alertjson, fetchAlerts};
