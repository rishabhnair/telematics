var db = require('../sequelize.js')

var  getlocation = function(srno,callback){
    var query={
      text : 'SELECT lat,lon FROM devices WHERE srno = $1',
      values: [srno]
    }
     db.query(query)
     .then(resp=>{
      callback(false,resp.rows)
    }, function(err) {
      callback(err.message,false)
      // console.log(err)
    })
    .catch(err=>{
      callback('err',false)
      // console.log(err)
  })
}


module.exports = {
  getlocation
}
