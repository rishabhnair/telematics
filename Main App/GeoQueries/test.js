const db = require('./sequelize');
const elastic = require('./elasticize');
const redis = require('./redisize');
// var data = Date.now()
// console.log(data.getDate())
//var date = data.toString().split('.')
// console.log(data)

var getFromElastic = function(vin){
  var start= new Date (Date.now());
  // redis.hget('tripy',vin,function(error,result){
  //    if(error){
  //      console.log('redis error:')
  //      console.log(error);
  //    }
  //      else {
  //      var start = new Date (JSON.parse(result).start_time);
  //      console.log(start)
  //      console.log(stop)
  //      var query={
  //        text : "insert into trip_details(vin,start_time,stop_time) values($1,$2,$3)",
  //        values : [vin,start,stop]
  //      }
  //      db.query(query)
  //      .then(result=>{
  //        console.log('DB updated');
         // var start = start.replace(/[-.:]/g, '');
         start = start.toISOString().split('.')[0].replace(/[-:]/g, '')+'Z';
         console.log(start)
         elastic.search({
             index: 'testdata',
             type: 'parameter',
             body: {
                 query: {
                         range: {
                           DateTime: {
                             "gte": "20150101T000000Z",
                             "lte": start
                             // "lte": "now",
                             // "time_zone": "+01:00"
                           }
                     }
                     // match: {
                     //   "vin":"TELINK4G0000001"
                     // }
                 }
             }
         }).then(function(resp) {
             var msg = [];
             console.log("--- Response ---");
             console.log(resp);
             console.log("--- Hits ---");
             resp.hits.hits.forEach(function(hit){
               msg.push({
                        'time':hit._source.DateTime,
                        'location':hit._source.location,
                        'vin': hit._source.vin
                      })
             })
             console.log(msg);
             // res.send(msg);
         }, function(err) {
             console.trace(err.message);
         });
   //     })
   //   }
   // })

}
getFromElastic('')

module.exports = {
  getFromElastic
}
