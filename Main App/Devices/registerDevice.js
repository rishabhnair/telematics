var fs = require('fs');
var mqtt = require('mqtt')
var db = require('../sequelize.js')
var getRedis = require('../getRedis.js')


var registerDevice = async (info)=>{
  console.log({info})
  var x = await getRedis(info.token)
  .catch(err=>{ throw {error : 'Invalid User'} })
  var userInfo = JSON.parse(x)
  console.log('CHECK THIS MATE: CHECK THIS CHECK THIS CHECK THIS CHECK THIS CHECK THIS')
  console.log(userInfo)
  var query = {
    text: 'SELECT COUNT (*) from devices where srno = $1',
  	values: [info.srno]
	}
  var countDevice =  await db.query(query)
  .catch(err=>{ throw {error : 'Unrecognized Serial No.'} })
  console.log({countDevice})
  if(countDevice.rows[0].count>0){
    var statusQuery = {
      text: 'select status from devices where srno = $1',
      values: [info.srno]
    }
    var statusDevice = await db.query(statusQuery)
    .catch(err=>{ throw {error : 'Invalid User'} })
    console.log(statusDevice.rows[0].status)
    if(statusDevice.rows[0].status == null){
      const text2 =
      {
        text: 'Update devices SET status = $1,project_id = $2,updated_by = $3,subunit_name = $4 where srno = $5',
        values: ['New',userInfo.project_id,userInfo.id,userInfo.subunit_name,info.srno]
      }
      db.query(text2)
      .then(res=>{
        console.log('device redistered')
        return res;
      })
      .catch(err=>{
        throw {error : 'Unable to Update Device Info' }
      })

    }
    else {
      throw {error : 'Device already Registered !'}
    }
  }
  else {
    throw {error : 'Unrecognized Device'}
  }



 };
module.exports = registerDevice;
