const cassandra = require('cassandra-driver');
var fs = require('fs');
const client = new cassandra.Client({ contactPoints: ['108.61.89.215'], keyspace: 'kspace' });
var batch_file = function(data,callback){
console.log(data)
var vin = data.srno;
var startTime = new Date(data.startTime);
var stopTime = new Date(data.stopTime);
console.log('-----------')
console.log({stopTime,startTime})
console.log('----------')
var startTimeString = (""+startTime).split('GMT');
var rep = startTimeString[0].trim().replace(":","");
console.log(rep)
var filename = vin+"_"+rep.replace(":","")+".asc";
filename = filename.replace(/\s/g,'');
console.log(filename)
// const query = "SELECT * FROM candata WHERE srno = ? AND time >= ? AND time <= ? ALLOW FILTERING";
const query = `SELECT * FROM candata WHERE srno = ? AND time >= ? AND time <= ? ALLOW FILTERING`;
client.execute(query,[vin,startTime,stopTime], function (err, result) {
  if (err) {
    console.log('Select Sample Data Failed: ' + JSON.stringify(err));
    console.log('Select Sample Data Failed: ' + err.message);
  }
  else {
    console.log(result)
    var path = './files/'+filename;
    fs.exists(path,function(exists){
      if(exists){
        console.log('File Exists')
        callback(false,filename)
      }
      else {
        var count = 0;
        var len =0;
        console.log('Selected Sample Data: ' + JSON.stringify(result));
        for (i = 0; i < result.rows.length; i++) {
          var newresult= result.rows[i]['payload'].slice(0,-2).split(/\[(.+)/)[1].split('[')
        for (var j= 0; j<newresult.length; j++){
          count++;
          len = result.rows.length*newresult.length;
          fs.appendFile('./files/'+filename, '[' +newresult[j]+'\n',function(err) {
            if(err) {
              console.log(err);
              callback(true,false)
            }
          })
          }
        }
        if(count == len)
        callback(false,filename)
      }
    })

  }
})
}
     module.exports = batch_file;
