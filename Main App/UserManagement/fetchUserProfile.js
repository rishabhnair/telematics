var db = require('../sequelize.js')
var redis = require('redis')
var client = redis.createClient();

var fetchUserProfile = (data,callback)=>{
  var finalResult = {}
  // console.log(data)
  client.get(data,function(err, reply){
    if(err){
      callback(err,false)
    }
    else {
      var userInfo = JSON.parse(reply)
      // console.log({userInfo})
      var query = {
        text : 'SELECT * from users inner join user_profile on users.user_id = user_profile.user_id where username = $1',
        values : [userInfo.username]
      }
      db.query(query)
      .then(result=>{
        // console.log(result.rows)
        if(result.rows.length>0){
          finalResult['user'] = result.rows[0];
          var vehicleQuery = {
            text : 'SELECT * from devices where public_user = $1',
            values : [result.rows[0].user_id]
          }
          db.query(vehicleQuery)
          .then(newResult=>{
            finalResult['vehicle'] = newResult.rows[0];
            callback(false, finalResult)
          })
          .catch(err=>{
            callback(false, finalResult)
          })
        }
        else {
          callback('User not found', false)
        }
      })
      .catch(err=>{
        callback(err, false)
      })
    }
  })
}

module.exports = fetchUserProfile;
