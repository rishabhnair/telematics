import React, { Component } from "react";
import Switch from "react-switch";

class SwitchForTable extends Component {
  constructor() {
    super();
    this.state = { checked: true };
    this.handleChange = this.handleChange.bind(this);
  }
 
  handleChange(checked) {
    this.setState({ checked });
    this.props.switchvalue(checked);
  }
 
  render() {
    return (
        <Switch 
            checked={this.state.checked}
            onChange={this.handleChange}
            onColor="#4caf50"
            offColor='#e0e0e0'
            onHandleColor="#fafafa"
            //offHandleColor='#e0e0e0'
            handleDiameter={16}
            uncheckedIcon={false}
            checkedIcon={false}
            boxShadow="white"
            activeBoxShadow="white"
            height={10}
            width={30}
            className="react-switch"
            id="material-switch"
        />
    );
  }
}

export default SwitchForTable;