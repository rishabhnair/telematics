const db = require('../sequelize');
const elastic = require('./elasticize');

var getFromElastic = function(vin,geolat,geolon,geodist,callback){
  console.log({vin})
  elastic.search({
      index: 'testdata',
      type: 'parameter',
      body: {
        "query": {
          "bool" : {
            "must" : {
              "match_all" : {}
            },
            "filter" : {
              "geo_distance" : {
                "distance" : geodist + "km",
                "location" : {
                  "lat" : geolat,
                  "lon" : geolon
                }
              }
            },
            "filter": [
              { "range": { "DateTime": { "gte": "20150101T000000Z", "lte": "now/s" }}}
            ]
          }
        }
      }
  }).then(function(resp) {
             var inside =[];
             var outside =[]
             resp.hits.hits.forEach(function(hit){
               inside.push(hit._source.vin)
             })
             uniqueArray = inside.filter(function(elem, pos) {
             return inside.indexOf(elem) == pos;
             })
             // console.log(inside);
             // console.log(uniqueArray)
             for(var i = 0; i<vin.length; i++){
               var flag=0;
               for(var j=0; j<uniqueArray.length; j++){
                 if(vin[i]==uniqueArray[j]){
                   flag=1
                 }
               }
               if(flag==0){
                 outside.push(vin[i])
               }
             }
             console.log('outside vins:')
             console.log(outside)
             callback(false,outside)
         }, function(err) {
             console.trace(err.message);
         })
         .catch(err=>{
           callback('err',false)
           console.log(err)
         })
       }

// getFromElastic(["TELINK4G0000001","TELINK4G0000002"], 24, 77, 200)

module.exports =   getFromElastic
