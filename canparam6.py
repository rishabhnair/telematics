# coding: utf-8
import sys
import psycopg2
import re
import redis
import pandas as pd
from pyspark import SparkContext
from pyspark import SparkConf
from pyspark.streaming import StreamingContext
from pyspark.streaming.kafka import KafkaUtils
from pyspark.sql import SparkSession
from pyspark.sql import Row
from pyspark.sql import functions
from uuid import uuid4
import uuid
import time_uuid
import datetime
import requests


if __name__ == "__main__":
    
	i=1
	spark = SparkSession.builder.appName("CassandraIntegration").getOrCreate()
	ssc = StreamingContext(spark.sparkContext,1)
	try:
		kvsparam = KafkaUtils.createDirectStream(ssc,["panda"],{"bootstrap.servers": "149.28.63.75:9092,45.63.16.214:9092,45.63.16.214:9093"})
		#print(kvs)
		dfparam=kvsparam.map(lambda x: transform(x[1]))
		#df= kvs.map(lambda x: x[0])
		print(type(kvsparam))


		#sp = kvsparam.map(lambda x,y: x.split(','))
		#print(sp)
		#kvs.map(lambda x: transform(str(x)))
		dfparam.foreachRDD(lambda x: writeToElastic(x,i))

		print(type(dfparam))
		#dfparam.pprint()
	except:
		print("Invalid value")
	try:
		kvscan = KafkaUtils.createDirectStream(ssc,["candy"],{"bootstrap.servers": "149.28.63.75:9092,45.63.16.214:9092,45.63.16.214:9093"})
		print(type(kvscan))
		dfcan= kvscan.map(lambda x: Row(srno=fetchvin(x[0].encode('utf-8').strip()), payload=x[1].encode('utf-8').strip(), time= datetime.datetime.now()))
		#dfcan= kvscan.map(lambda x: Row(srno=fetchvin(x[0].encode('utf-8').strip()), payload=x[1].encode('utf-8').strip(), time= datetime.datetime.now().encode('ISO-8859-1').strip()))
		#print("Invalid value")
		dfcan.foreachRDD(lambda x: writeToCassandra(x,i))
	except:
		print("Invalid value")
		
		
		
	def fetchvin(topic):
		vin = topic.split("/")
		vin = vin[1]
		vin = vin[:-1]
		return vin
	
	def transform(ips):
		#ips = input("string: ") #input string
		ips = re.split("|/",ips) #split input string
		fst_s = ips[0]
		rest_s = ips[1:len(ips)]
		def parse_vin(v):
			lst = []
			lst.append(v[0:15])
			lst.append(v[15:24])
			lst.append(v[24:33])
			return lst
		def date_time_convert(dt): #converts datetime to kibana datetime format
			dt_s = ""
			for ii in range (len(dt)):
				dt_s+=dt
			dt1 = ""
			dt2 = ""
			dt3 = ""
			for m in range(0,6):
				dt1 = dt1 + dt_s[m]
			for n in range(4,6):
				dt2 = dt2 + dt1[n]
			for n in range(2,4):
				dt2 = dt2 + dt1[n]
			for n in range(0,2):
				dt2 = dt2 + dt1[n]
			for p in range(6,12):
				dt3 = dt3 + dt_s[p]
			dt_s = "20" + dt2 + "T" + dt3 + "Z"
			return (dt_s)
		def parse_first(s): #parses the first string of splt_ips
			lst = []
			lst2 = []
			lst_vin = []
			lst.append(s[1:3])
			if s[3]!="D":              #check(1) packet type is D
				return 1
			else:
				lst.append(s[3])
				lst.append(s[4]) #Number of parameters in hexa
				lst2.append(s[5:38])
				vin_lat_long = ""
				for ii in range(len(lst2)):
					vin_lat_long+=lst2[ii]
				lst+=parse_vin(vin_lat_long)
				lst.append(date_time_convert(s[38:50]))
				lst.append(s[50])
				lst.append(s[51:(int(s[50],16)+51)]) 
				return lst       #list one
		def parse_rest(s):   #parses the remaning part of splt_ips
			lst = []
			empty_str = ""
			for ii in range(len(s)-1):
				empty_str = s[ii]
				lst.append(empty_str[0])
				empty_str = empty_str[1:]
				lst.append(empty_str)
				empty_str = ""       
			empty_str += s[len(s)-1]
			lst.append(empty_str[0:2])
			return(lst)           #list two
		def checks(a,b):    #checks all the reqirements and prints list
			empty_str = ""
			if parse_first(fst_s)==1:
				return(["501"])
			else:
				for ii in range(len(a+b)):
					empty_str += (a+b)[ii]
				if ((len(b[0:(len(b)-1)])/2)+1)!=(int(a[2],16)):
					return(["501"])
				elif (len(empty_str)+5)!=int(a[0],16):
					return(["501"])
				else:
					return(a+b)
		l=checks(parse_first(fst_s), parse_rest(rest_s))
		if l[0]=="501":
			dict1={"vin":l[0]}
		else:	
			dict1 = {"frame length" : int(l[0],16) ,
			"Packet Type": l[1],
			"No of parameters": int(l[2],16),
			"vin":l[3],
			"location": {"lat":float(l[4]), "lon":float(l[5])},
			"DateTime": l[6],
			"p1 Length": int(l[7],16),
			"p1 Data": l[8],
			"p2 Length": int(l[9],16),
			"p2 Data": l[10] ,
			"p3 Length": int(l[11],16),
			"p3 Data": l[12],
			"p4 Length": int(l[13],16),
			"p4 Data": l[14],
			"p5 Length": int(l[15],16),
			"p5 Data":  l[16],
			"p6 Length": int(l[17],16),
			"p6 Data": l[18],
			"p7 Length": int(l[19],16),
			"p7 Data": l[20],
			"p8 Length": int(l[21],16),
			"p8 Data":  l[22],
			"p9 Length": int(l[23],16),
			"p9 Data":  l[24],
			"p10 Length": int(l[25],16),
			"p10 Data":  l[26],
			"checksum": int(l[27],16)}
		return(dict1)

	def writeToCassandra(rdd,j):
		if not rdd.isEmpty():
			
			df = spark.createDataFrame(rdd)
			#df.show()
			print(type(df))
			##dfWithTime= df.withColumn("time", datetime.datetime.now())
			##dfWithTime.show()
			##print(type(dfWithTime))
			df.write.format("org.apache.spark.sql.cassandra").options(table="candata", keyspace= "kspace").save(mode="append")
			j+=1
			print(i)
		return rdd
		

	
	def writeToElastic(rdd,j):
		if not rdd.isEmpty():
			
			df = spark.createDataFrame(rdd)
			df.show()
			
			print(type(df))
			
			#VIN= str(client_id)
			#now = datetime.datetime.now()
			#date= str(timestamp)
			#df1=df.coalesce('client_id')
			#df1[['payload']].write.partitionBy('client_id').format("text").mode("append").save("%s_%s.asc",(VIN,date))
			#df.write.format("text").mode("append").save("output20.asc")
			
			
			df=df.repartition(50)
			mvv_list = df.select('vin').collect()
			mvvrow = mvv_list[0].vin
			vin= str(mvvrow)
			print(vin)
			dictsrno = {'vin':vin}
			if vin != '501':
				df.write.format("org.elasticsearch.spark.sql").option("es.port","9200").option("es.nodes","45.63.16.214").mode("append").save("testdata/parameter")
			
			j+=1
			print(i)
		return rdd
	
	ssc.start()
	ssc.awaitTermination()

