import React, {PureComponent} from 'react'
import ReactSpeedometer from "react-d3-speedometer";

class Speedgauge extends PureComponent{
    constructor(props){
        super(props);
        this.state = {
            speed : 0
        }
    }

    //currentValueText = {' Speed - ${value} (Km/h)'}
    componentWillReceiveProps(nextProps){
        if(nextProps.speed !== this.props.speed){
            this.setState({speed: nextProps.speed})
        }  
    }

    render(){
        return(
            <div>
                <ReactSpeedometer
                    forceRender={true}
                    height={110}
                    width={170}
                    minValue={0}
                    maxValue={180}
                    value={this.props.speed}
                    currentValueText = {this.state.speed+'  Km/h'}
                    needleColor="#ff9800"
                    startColor="#039be5"
                    segments={6}
                    endColor="red"
                    needleTransitionDuration={10}
                    needleTransition="easeElastic"
                    textColor = 'white'
                    ringWidth = {45}
                />
            </div>
        );
    }
}
export default Speedgauge;